#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

mkdir $ARTIFACT_ROOT/plots

python $ARTIFACT_ROOT/scripts/internal/plot/generation/collect_results.py
if [[ -n "$ENABLE_PYTHON_CONSTRAINT_SSC" && "$ENABLE_PYTHON_CONSTRAINT_SSC" -eq 1 ]]; then
  gnuplot $ARTIFACT_ROOT/scripts/internal/plot/generation/CONV/gnuplot_generation_ssc
  gnuplot $ARTIFACT_ROOT/scripts/internal/plot/generation/GEMM/gnuplot_generation_ssc
  gnuplot $ARTIFACT_ROOT/scripts/internal/plot/generation/CCSDT/gnuplot_generation_ssc
  gnuplot $ARTIFACT_ROOT/scripts/internal/plot/generation/PRL/gnuplot_generation_ssc
else
  gnuplot $ARTIFACT_ROOT/scripts/internal/plot/generation/CONV/gnuplot_generation
  gnuplot $ARTIFACT_ROOT/scripts/internal/plot/generation/GEMM/gnuplot_generation
  gnuplot $ARTIFACT_ROOT/scripts/internal/plot/generation/CCSDT/gnuplot_generation
  gnuplot $ARTIFACT_ROOT/scripts/internal/plot/generation/PRL/gnuplot_generation
fi

python $ARTIFACT_ROOT/scripts/internal/plot/storage/collect_results.py
gnuplot $ARTIFACT_ROOT/scripts/internal/plot/storage/CONV/gnuplot_storage
gnuplot $ARTIFACT_ROOT/scripts/internal/plot/storage/GEMM/gnuplot_storage
gnuplot $ARTIFACT_ROOT/scripts/internal/plot/storage/CCSDT/gnuplot_storage
gnuplot $ARTIFACT_ROOT/scripts/internal/plot/storage/PRL/gnuplot_storage

python $ARTIFACT_ROOT/scripts/internal/plot/exploration/collect_results.py
gnuplot $ARTIFACT_ROOT/scripts/internal/plot/exploration/CONV/gnuplot_exploration
gnuplot $ARTIFACT_ROOT/scripts/internal/plot/exploration/GEMM/gnuplot_exploration
gnuplot $ARTIFACT_ROOT/scripts/internal/plot/exploration/CCSDT/gnuplot_exploration
gnuplot $ARTIFACT_ROOT/scripts/internal/plot/exploration/PRL/gnuplot_exploration


python $ARTIFACT_ROOT/scripts/internal/plot/overall/collect_results.py > $ARTIFACT_ROOT/plots/overall.txt


python $ARTIFACT_ROOT/scripts/internal/plot/siamese/collect_results.py > $ARTIFACT_ROOT/plots/siamese.txt


echo ""
echo ""
echo ""
echo "Done plotting results"
echo "Plots can be found in folder \"plots\""
echo ""