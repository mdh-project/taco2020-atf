#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
mkdir $BUILD_FOLDER &> /dev/null || { rm -rf $BUILD_FOLDER; mkdir $BUILD_FOLDER; }
{
  # fix permissions in case files have been uploaded using sftp
  chmod u+x $ARTIFACT_ROOT/evaluation/overall/COGENT/*.sh
  chmod u+x $ARTIFACT_ROOT/extern/mkl-dnn/scripts/*.sh
  chmod u+x $ARTIFACT_ROOT/evaluation/siamese/caffe*/data/mnist/get_mnist.sh
  chmod u+x $ARTIFACT_ROOT/evaluation/siamese/caffe*/examples/siamese/*.sh
  find $ARTIFACT_ROOT/scripts -type f -iname "*.sh" -exec chmod u+x {} \;

  # download MKL-DNN bundled MKL
  if [[ -n "$ENABLE_MKL_DNN" && "$ENABLE_MKL_DNN" -eq 1 ]]; then
    cd $ARTIFACT_ROOT/extern/mkl-dnn/scripts &&
    ./prepare_mkl.sh
  fi &&

  # initialize CMake project
  cd $BUILD_FOLDER &&
  cmake -DCMAKE_BUILD_TYPE="Release" -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/cmake/Modules -DSAMPLES=ON ${@:1} .. &&
  make -j `nproc` &&

  cp -r $ARTIFACT_ROOT/extern/CLTune/samples $BUILD_FOLDER/extern &&

  if [[ -n "$ENABLE_EKR" && "$ENABLE_EKR" -eq 1 ]]; then
    # EKR Record Linkage
    cp -r $ARTIFACT_ROOT/evaluation/overall/EKR $BUILD_FOLDER/evaluation/overall
    ln -s $ARTIFACT_ROOT/evaluation/data $BUILD_FOLDER/evaluation/overall/EKR/data
  fi &&

  if [[ -n "$ENABLE_COGENT" && "$ENABLE_COGENT" -eq 1 ]]; then
    # COGENT
    cp -r $ARTIFACT_ROOT/evaluation/overall/COGENT $BUILD_FOLDER/evaluation/overall
  fi &&

  if [[ -n "$ENABLE_TENSOR_COMPREHENSIONS" && "$ENABLE_TENSOR_COMPREHENSIONS" -eq 1 ]]; then
    # Tensor Comprehensions
    cp -r $ARTIFACT_ROOT/evaluation/overall/TensorComprehensions $BUILD_FOLDER/evaluation/overall
  fi &&

  printf "\n\nArtifact installation successful!\n"
} || {
  printf "\n\nArtifact installation failed!\n"
  exit 1
}