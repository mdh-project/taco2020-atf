#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

if [[ -n "$ENABLE_CAFFE_GPU" && "$ENABLE_CAFFE_GPU" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/siamese_gpu/caffe/run_one.sh
fi && \
$ARTIFACT_ROOT/scripts/internal/siamese_gpu/ATF/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/siamese_gpu/CLBlast/run_all.sh && \
if [[ -n "$ENABLE_CUBLAS" && "$ENABLE_CUBLAS" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/siamese_gpu/cuBLAS/run_all.sh
fi