#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

platform_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."siamese"."GPU OpenCL platform id"'` && \
device_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."siamese"."GPU OpenCL device id"'` && \
input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."siamese"."input sizes"['"$1"']'` && \
minutes=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."siamese"."tuning time (minutes)"'` && \
cd $BUILD_FOLDER/extern/CLBlast && \
rm -rf `echo $input_size | jq -r '[.[] | tostring] | join("x")'`
mkdir `echo $input_size | jq -r '[.[] | tostring] | join("x")'` && \
cd `echo $input_size | jq -r '[.[] | tostring] | join("x")'` && \
mkdir -p $ARTIFACT_ROOT/results/siamese/gpu/$platform_id/$device_id/CLBlast && \
../clblast_tuner_xgemm_direct -platform $platform_id -device $device_id -m `echo $input_size | jq -r '.[0]'` -n `echo $input_size | jq -r '.[1]'` -k `echo $input_size | jq -r '.[2]'` -alpha 1 -beta 0 -fraction 1 |& tee "$ARTIFACT_ROOT/results/siamese/gpu/$platform_id/$device_id/CLBlast/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log" && \
grep -aoP "\* Found best result \K[0-9\.]+" "$ARTIFACT_ROOT/results/siamese/gpu/$platform_id/$device_id/CLBlast/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log" | tail -n1 > "$ARTIFACT_ROOT/results/siamese/gpu/$platform_id/$device_id/CLBlast/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_runtime" && \
grep -aoP "generation time: \K[0-9]+" "$ARTIFACT_ROOT/results/siamese/gpu/$platform_id/$device_id/CLBlast/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log" | tail -n1 > "$ARTIFACT_ROOT/results/siamese/gpu/$platform_id/$device_id/CLBlast/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_generation_time" && \
grep -aoP "[0-9]+ configuration\(s\)" "$ARTIFACT_ROOT/results/siamese/gpu/$platform_id/$device_id/CLBlast/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log" | tail -n1 | grep -oP "[0-9]+" > "$ARTIFACT_ROOT/results/siamese/gpu/$platform_id/$device_id/CLBlast/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_sp_size"