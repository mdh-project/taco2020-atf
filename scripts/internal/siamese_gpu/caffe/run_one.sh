#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT/evaluation/siamese/caffe_gpu && \
mkdir -p $ARTIFACT_ROOT/results/siamese/gpu/caffe && \
$ARTIFACT_ROOT/evaluation/siamese/caffe_gpu/data/mnist/get_mnist.sh && \
$ARTIFACT_ROOT/evaluation/siamese/caffe_gpu/examples/siamese/create_mnist_siamese.sh
runtime="$(date +%s%N)"
$ARTIFACT_ROOT/evaluation/siamese/caffe_gpu/examples/siamese/train_mnist_siamese.sh |& tee "$ARTIFACT_ROOT/results/siamese/gpu/caffe/output.log"
runtime="$(($(date +%s%N)-runtime))"
runtime=$((runtime/1000000))
echo $runtime > "$ARTIFACT_ROOT/results/siamese/gpu/caffe/runtime"