#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."GEMM"."input size"'` && \
cd $BUILD_FOLDER/evaluation/overall/MKL && \
mkdir -p $ARTIFACT_ROOT/results/overall/cpu/MKL && \
./overall_mkl_gemm --input-size `echo $input_size | jq -r '[.[] | tostring] | join(" ")'` |& tee "$ARTIFACT_ROOT/results/overall/cpu/MKL/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log"