#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

num_exploration_runs=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."num. runs"'` && \
if [ $num_exploration_runs -gt 0 ]; then
  $ARTIFACT_ROOT/scripts/internal/overall_cpu/CLBlast/run_one.sh || exit 1
fi