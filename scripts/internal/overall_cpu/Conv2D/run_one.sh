#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

platform_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."CPU OpenCL platform id"'` && \
device_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."CPU OpenCL device id"'` && \
cd $BUILD_FOLDER/extern/CLTune && \
mkdir -p $ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/Conv2D && \
./sample_conv $platform_id $device_id 3 |& tee "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/Conv2D/conv_4096x4096.log" && \
grep -aoP "conv; \K[0-9\.]+" "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/Conv2D/conv_4096x4096.log" | tail -n1 > "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/Conv2D/conv_4096x4096_runtime" && \
grep -aoP "generation time: \K[0-9]+" "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/Conv2D/conv_4096x4096.log" | tail -n1 > "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/Conv2D/conv_4096x4096_generation_time" && \
grep -aoP " out of \K[0-9]+" "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/Conv2D/conv_4096x4096.log" | tail -n1 > "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/Conv2D/conv_4096x4096_sp_size"