#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

platform_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."CPU OpenCL platform id"'` && \
device_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."CPU OpenCL device id"'` && \
input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."PRL"."input size"'` && \
minutes=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."tuning time (minutes)"'` && \
cd $BUILD_FOLDER/evaluation/overall/OpenTuner && \
mkdir -p $ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/OpenTuner && \
(FILES_SUFFIX="_$1"; timeout -s 9 "$((minutes+OVERALL_TIMEOUT_EXTRA))m" ./overall_opentuner_prl --mode tune --platform-id $platform_id --device-id $device_id --input-size `echo $input_size | jq -r '[.[] | tostring] | join(" ")'` --tuning-time `echo "scale=4; $minutes/60" | bc` && ./overall_opentuner_prl --mode bench --platform-id $platform_id --device-id $device_id --input-size `echo $input_size | jq -r '[.[] | tostring] | join(" ")'`) |& tee "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/OpenTuner/prl_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log"
grep -aoP "number of evaluated configs: \K[0-9]+" "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/OpenTuner/prl_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log" > "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/OpenTuner/prl_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1_num_invalid"
grep -aoP "number of valid configs: \K[0-9]+" "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/OpenTuner/prl_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log" > "$ARTIFACT_ROOT/results/overall/cpu/$platform_id/$device_id/OpenTuner/prl_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1_num_valid"