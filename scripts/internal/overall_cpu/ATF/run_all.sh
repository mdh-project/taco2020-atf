#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

$ARTIFACT_ROOT/scripts/internal/overall_cpu/ATF/CONV/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/ATF/GEMM/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/ATF/CCSDT/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/ATF/PRL/run_all.sh