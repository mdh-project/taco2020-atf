#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

num_exploration_runs=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."num. runs"'` && \
if [ $num_exploration_runs -gt 0 ]; then
  for i in `seq 0 $((num_exploration_runs - 1))`; do
    $ARTIFACT_ROOT/scripts/internal/overall_cpu/CLTune_pruned/GEMM/run_one.sh $i || exit 1
  done
fi