#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

$ARTIFACT_ROOT/scripts/internal/overall_cpu/ATF/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/ATF_former/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/CLBlast/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/CLTune/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/CLTune_pruned/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/Conv2D/run_all.sh && \
if [[ -n "$ENABLE_EKR" && "$ENABLE_EKR" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_cpu/EKR/run_all.sh
fi && \
if [[ -n "$ENABLE_MKL" && "$ENABLE_MKL" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_cpu/MKL/run_all.sh
fi && \
if [[ -n "$ENABLE_MKL_DNN" && "$ENABLE_MKL_DNN" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_cpu/MKL-DNN/run_all.sh
fi && \
$ARTIFACT_ROOT/scripts/internal/overall_cpu/OpenTuner/run_all.sh