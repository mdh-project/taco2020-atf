#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."PRL"."input size"'` && \
threads=`nproc --all`
threads=`echo "$threads*2" | bc`
cd $BUILD_FOLDER/evaluation/overall/EKR && \
mkdir -p $ARTIFACT_ROOT/results/overall/cpu/EKR && \
java -Xms512m -Xmx14g -jar record-linkage-java.jar `echo $input_size | jq -r '[.[] | tostring] | join(" ")'`  $threads |& tee "$ARTIFACT_ROOT/results/overall/cpu/EKR/prl_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log" && \
grep -aoP "[0-9]+" "$ARTIFACT_ROOT/results/overall/cpu/EKR/prl_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log" > "$ARTIFACT_ROOT/results/overall/cpu/EKR/prl_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_runtime"