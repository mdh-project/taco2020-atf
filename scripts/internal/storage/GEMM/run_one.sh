#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

platform_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."storage"."OpenCL platform id"'` && \
device_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."storage"."OpenCL device id"'` && \
input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."storage"."input sizes (square)"['"$1"']'` && \
cd $BUILD_FOLDER/evaluation/storage && \
mkdir -p $ARTIFACT_ROOT/results/storage/$platform_id/$device_id/MD && \
mkdir -p $ARTIFACT_ROOT/results/storage/$platform_id/$device_id/1D && \
./storage_gemm $platform_id $device_id $input_size $input_size $input_size |& tee "$ARTIFACT_ROOT/results/storage/$platform_id/$device_id/gemm_${input_size}x${input_size}x${input_size}.log"