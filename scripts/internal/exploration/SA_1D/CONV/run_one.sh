#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

platform_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."exploration"."OpenCL platform id"'` && \
device_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."exploration"."OpenCL device id"'` && \
input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."exploration"."CONV"."input size"'` && \
minutes=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."exploration"."tuning time (minutes)"'` && \
cd $BUILD_FOLDER/evaluation/exploration && \
mkdir -p $ARTIFACT_ROOT/results/exploration/$platform_id/$device_id/SA_1D && \
./exploration_sa_1d_conv $platform_id $device_id $minutes $1 `echo $input_size | jq -r '[.[] | tostring] | join(" ")'` |& tee "$ARTIFACT_ROOT/results/exploration/$platform_id/$device_id/SA_1D/conv_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log"