#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

$ARTIFACT_ROOT/scripts/internal/exploration/SA_1D/CONV/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/exploration/SA_1D/GEMM/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/exploration/SA_1D/CCSDT/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/exploration/SA_1D/PRL/run_all.sh