#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

$ARTIFACT_ROOT/scripts/internal/generation/ATF/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/generation/ATF_former/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/generation/CLTune/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/generation/python-constraint/run_all.sh && \
if [[ -n "$ENABLE_PYTHON_CONSTRAINT_SSC" && "$ENABLE_PYTHON_CONSTRAINT_SSC" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/generation/python-constraint-ssc/run_all.sh
fi