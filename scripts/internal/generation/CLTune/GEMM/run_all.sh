#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

num_generation_inputs=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."generation"."input sizes (square)"|length'` && \
if [ $num_generation_inputs -gt 0 ]; then
  for i in `seq 0 $((num_generation_inputs - 1))`; do
    $ARTIFACT_ROOT/scripts/internal/generation/CLTune/GEMM/run_one.sh $i || exit 1
  done
fi