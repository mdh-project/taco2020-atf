#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

platform_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."generation"."OpenCL platform id"'` && \
device_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."generation"."OpenCL device id"'` && \
input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."generation"."input sizes (square)"['"$1"']'` && \
timeout_hours=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."generation"."timeout (hours)"'` && \
cd $ARTIFACT_ROOT/evaluation/generation/python-constraint-ssc && \
mkdir -p $ARTIFACT_ROOT/results/generation/$platform_id/$device_id/python-constraint-ssc && \
timeout -s 9 "${timeout_hours}h" python ccsdt.py $input_size $input_size $input_size $input_size $input_size $input_size $input_size |& tee "$ARTIFACT_ROOT/results/generation/$platform_id/$device_id/python-constraint-ssc/ccsdt_${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}.log"
if [ ${PIPESTATUS[0]} == 0 ]; then
  cp "$ARTIFACT_ROOT/results/generation/$platform_id/$device_id/python-constraint-ssc/ccsdt_${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}.log" "$ARTIFACT_ROOT/results/generation/$platform_id/$device_id/python-constraint-ssc/ccsdt_${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}"
else
  printf "timeout" > "$ARTIFACT_ROOT/results/generation/$platform_id/$device_id/python-constraint-ssc/ccsdt_${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}"
fi