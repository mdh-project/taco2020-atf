#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

platform_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."generation"."OpenCL platform id"'` && \
device_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."generation"."OpenCL device id"'` && \
input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."generation"."input sizes (square)"['"$1"']'` && \
cd $BUILD_FOLDER/evaluation/generation/ATF && \
mkdir -p $ARTIFACT_ROOT/results/generation/$platform_id/$device_id/ATF && \
./generation_ccsdt $platform_id $device_id $input_size $input_size $input_size $input_size $input_size $input_size $input_size |& tee "$ARTIFACT_ROOT/results/generation/$platform_id/$device_id/ATF/ccsdt_${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}x${input_size}.log"