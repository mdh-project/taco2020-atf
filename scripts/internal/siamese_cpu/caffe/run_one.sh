#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT/evaluation/siamese/caffe_cpu && \
mkdir -p $ARTIFACT_ROOT/results/siamese/cpu/caffe && \
$ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/data/mnist/get_mnist.sh && \
$ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/examples/siamese/create_mnist_siamese.sh
runtime="$(date +%s%N)"
$ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/examples/siamese/train_mnist_siamese.sh |& tee "$ARTIFACT_ROOT/results/siamese/cpu/caffe/output.log"
runtime="$(($(date +%s%N)-runtime))"
runtime=$((runtime/1000000))
echo $runtime > "$ARTIFACT_ROOT/results/siamese/cpu/caffe/runtime"