#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."siamese"."input sizes"['"$1"']'` && \
cd $BUILD_FOLDER/evaluation/siamese/ATLAS && \
mkdir -p $ARTIFACT_ROOT/results/siamese/cpu/ATLAS && \
./siamese_atlas_gemm --input-size `echo $input_size | jq -r '[.[] | tostring] | join(" ")'` |& tee "$ARTIFACT_ROOT/results/siamese/cpu/ATLAS/gemm_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log"