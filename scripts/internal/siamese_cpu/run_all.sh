#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

if [[ -n "$ENABLE_CAFFE_CPU" && "$ENABLE_CAFFE_CPU" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/siamese_cpu/caffe/run_one.sh
fi && \
$ARTIFACT_ROOT/scripts/internal/siamese_cpu/ATF/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/siamese_cpu/CLBlast/run_all.sh && \
if [[ -n "$ENABLE_ATLAS" && "$ENABLE_ATLAS" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/siamese_cpu/ATLAS/run_all.sh
fi