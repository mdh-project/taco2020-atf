from __future__ import print_function
import os
import json
import math

if os.environ.get('ARTIFACT_ROOT') == None or os.environ['ARTIFACT_ROOT'] == '':
  print('Please set the environement variable ARTIFACT_ROOT to the root directory of the artifact before executing this script!')
  exit(1)

experiments_json = '{}/{}'.format(os.environ['ARTIFACT_ROOT'], os.environ['EXPERIMENTS_FILE'])
input_sizes = dict()
with open(experiments_json, 'r') as json_file:
  json_data = json.load(json_file)
  devices_to_run_on = [x.lower() for x in json_data['overall']['run experiments on']]
  platform_id = {
    'cpu': json_data['overall']['CPU OpenCL platform id'],
    'gpu': json_data['overall']['GPU OpenCL platform id']
  }
  device_id = {
    'cpu': json_data['overall']['CPU OpenCL device id'],
    'gpu': json_data['overall']['GPU OpenCL device id']
  }
  input_sizes['conv'] = [str(x) for x in json_data['overall']['CONV']['input size']]
  input_sizes['gemm'] = [str(x) for x in json_data['overall']['GEMM']['input size']]
  input_sizes['ccsdt'] = [str(x) for x in json_data['overall']['CCSD(T)']['input size']]
  input_sizes['prl'] = [str(x) for x in json_data['overall']['PRL']['input size']]
  num_runs = json_data['overall']['num. runs']
  tuning_time = json_data['overall']['tuning time (minutes)']

class span:
  def __init__(self, columns, text):
    self.columns = columns
    self.text = text

tables = [
  [[ '', '', '', 'SP\nGen.Time', 'SP\nSize', 'Valid\nConfigs.', 'Invalid\nConfigs.', 'Runtime\non CPU', '', 'SP\nGen.Time', 'SP\nSize', 'Valid\nConfigs.', 'Invalid\nConfigs.', 'Runtime\non GPU']],

  [
    ['',  'ATF',                  '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['C', 'OpenTuner',            '', ' -- ', '', '', '',                               '',   '',   ' -- ', '', '', '',                               ''],
    ['O', 'CLTune',               '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['N', 'CLTune (pruned)',      '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['V', 'ATF (former)',         '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['',  'MKL-DNN / cuDNN',      '', span(4, 'auto-tuning not supported'), '', '', '', '',   '',   span(4, 'auto-tuning not supported'), '', '', '', ''],
    ['',  'Conv2D',               '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               '']
  ],

  [
    ['',  'ATF',                  '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['G', 'OpenTuner',            '', ' -- ', '', '', '',                               '',   '',   ' -- ', '', '', '',                               ''],
    ['E', 'CLTune',               '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['M', 'CLTune (pruned)',      '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['M', 'ATF (former)',         '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['',  'MKL / cuBLAS',         '', span(4, 'auto-tuning not supported'), '', '', '', '',   '',   span(4, 'auto-tuning not supported'), '', '', '', ''],
    ['',  'CLBlast',              '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               '']
  ],

  [
    ['',  'ATF',                  '', '',     '', '', '',                               '',   '',   '',     '', '',  '',                              ''],
    ['C', 'OpenTuner',            '', ' -- ', '', '', '',                               '',   '',   ' -- ', '', '',  '',                              ''],
    ['C', 'CLTune',               '', '',     '', '', '',                               '',   '',   '',     '', '',  '',                              ''],
    ['S', 'CLTune (pruned)',      '', '',     '', '', '',                               '',   '',   '',     '', '',  '',                              ''],
    ['D',  'ATF (former)',        '', '',     '', '', '',                               '',   '',   '',     '', '',  '',                              ''],
    ['T', 'TensorComprehensions', '', span(5, 'cpu not supported'), '', '', '',         '',   '',   ' -- ', '?', '', '',                              ''],
    ['',  'COGENT',               '', span(5, 'cpu not supported'), '', '', '',         '',   '',   span(4, 'auto-tuning not supported'), '', '', '', '']
  ],

  [
    ['',  'ATF',                  '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['P', 'OpenTuner',            '', ' -- ', '', '', '',                               '',   '',   ' -- ', '', '', '',                               ''],
    ['R', 'CLTune',               '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['L', 'CLTune (pruned)',      '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['',  'ATF (former)',         '', '',     '', '', '',                               '',   '',   '', '',     '', '',                               ''],
    ['',  'EKR',                  '', span(4, 'auto-tuning not supported'), '', '', '', '',   '',   span(5, 'gpu not supported'), '', '', '',         '']
  ]
]

### collect results from log files
file_pattern = '{0}/results/overall/{{0}}/{{1}}/{{2}}/{{3}}/{{4}}_{{5}}_{{6}}{{7}}'.format(os.environ['ARTIFACT_ROOT'])
mkl_dnn_file_pattern = '{0}/results/overall/{{0}}/MKL-DNN/{{1}}_{{2}}{{3}}'.format(os.environ['ARTIFACT_ROOT'])
cudnn_file_pattern = '{0}/results/overall/{{0}}/cuDNN/{{1}}_{{2}}{{3}}'.format(os.environ['ARTIFACT_ROOT'])
mkl_file_pattern = '{0}/results/overall/{{0}}/MKL/{{1}}_{{2}}{{3}}'.format(os.environ['ARTIFACT_ROOT'])
cublas_file_pattern = '{0}/results/overall/{{0}}/cuBLAS/{{1}}_{{2}}{{3}}'.format(os.environ['ARTIFACT_ROOT'])
conv2d_file_pattern = '{0}/results/overall/{{0}}/{{1}}/{{2}}/Conv2D/{{3}}_{{4}}{{5}}'.format(os.environ['ARTIFACT_ROOT'])
clblast_file_pattern = '{0}/results/overall/{{0}}/{{1}}/{{2}}/CLBlast/{{3}}_{{4}}{{5}}'.format(os.environ['ARTIFACT_ROOT'])
ekr_file_pattern = '{0}/results/overall/{{0}}/EKR/{{1}}_{{2}}{{3}}'.format(os.environ['ARTIFACT_ROOT'])
tensor_comprehensions_file_pattern = '{0}/results/overall/{{0}}/TensorComprehensions/{{1}}_{{2}}_{{3}}{{4}}'.format(os.environ['ARTIFACT_ROOT'])
cogent_file_pattern = '{0}/results/overall/{{0}}/COGENT/{{1}}_{{2}}{{3}}'.format(os.environ['ARTIFACT_ROOT'])

## CPU
for device in devices_to_run_on:
  table_index = 1
  for app in ['conv', 'gemm', 'ccsdt', 'prl']:
    # get runtimes
    unit_values = []
    # ATF
    atf_min_runtime = None
    atf_min_runtime_index = 0
    for run in range(0, num_runs):
      runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'ATF', app, 'x'.join(input_sizes[app]), run, '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          runtime = float(runtime_file.read())
          if atf_min_runtime is None or runtime < atf_min_runtime:
            atf_min_runtime = runtime
            atf_min_runtime_index = run
    if atf_min_runtime is not None:
      unit_values.append(atf_min_runtime)

    # OpenTuner
    opentuner_min_runtime = None
    opentuner_min_runtime_index = 0
    for run in range(0, num_runs):
      runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'OpenTuner', app, 'x'.join(input_sizes[app]), run, '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          runtime = float(runtime_file.read())
          if opentuner_min_runtime is None or runtime < opentuner_min_runtime:
            opentuner_min_runtime = runtime
            opentuner_min_runtime_index = run
    if opentuner_min_runtime is not None:
      unit_values.append(opentuner_min_runtime)

    # CLTune
    cltune_min_runtime = None
    cltune_min_runtime_index = 0
    for run in range(0, num_runs):
      runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'CLTune', app, 'x'.join(input_sizes[app]), run, '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          runtime = float(runtime_file.read())
          if cltune_min_runtime is None or runtime < cltune_min_runtime:
            cltune_min_runtime = runtime
            cltune_min_runtime_index = run
    if cltune_min_runtime is not None:
      unit_values.append(cltune_min_runtime)

    # CLTune (pruned)
    cltune_pruned_min_runtime = None
    cltune_pruned_min_runtime_index = 0
    for run in range(0, num_runs):
      runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'CLTune_pruned', app, 'x'.join(input_sizes[app]), run, '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          runtime = float(runtime_file.read())
          if cltune_pruned_min_runtime is None or runtime < cltune_pruned_min_runtime:
            cltune_pruned_min_runtime = runtime
            cltune_pruned_min_runtime_index = run
    if cltune_pruned_min_runtime is not None:
      unit_values.append(cltune_pruned_min_runtime)

    # ATF (former)
    atf_former_min_runtime = None
    atf_former_min_runtime_index = 0
    for run in range(0, num_runs):
      runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'ATF_former', app, 'x'.join(input_sizes[app]), run, '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          runtime = float(runtime_file.read())
          if atf_former_min_runtime is None or runtime < atf_former_min_runtime:
            atf_former_min_runtime = runtime
            atf_former_min_runtime_index = run
    if atf_former_min_runtime is not None:
      unit_values.append(atf_former_min_runtime)

    if app == 'conv':
      if device == 'cpu':
        # MKL-DNN
        mkl_dnn_runtime = None
        runtime_file_name = mkl_dnn_file_pattern.format(device, app, 'x'.join(input_sizes[app]), '_runtime')
        if os.path.exists(runtime_file_name):
          with open(runtime_file_name, 'r') as runtime_file:
            mkl_dnn_runtime = float(runtime_file.read())
            unit_values.append(mkl_dnn_runtime)

      if device == 'gpu':
        # cuDNN
        cudnn_runtime = None
        runtime_file_name = cudnn_file_pattern.format(device, app, 'x'.join(input_sizes[app]), '_runtime')
        if os.path.exists(runtime_file_name):
          with open(runtime_file_name, 'r') as runtime_file:
            cudnn_runtime = float(runtime_file.read())
            unit_values.append(cudnn_runtime)

      # Conv2D
      conv2d_runtime = None
      runtime_file_name = conv2d_file_pattern.format(device, platform_id[device], device_id[device], app, 'x'.join(input_sizes[app]), '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          conv2d_runtime = float(runtime_file.read())
          unit_values.append(conv2d_runtime)

    if app == 'gemm':
      if device == 'cpu':
        # MKL
        mkl_runtime = None
        runtime_file_name = mkl_file_pattern.format(device, app, 'x'.join(input_sizes[app]), '_runtime')
        if os.path.exists(runtime_file_name):
          with open(runtime_file_name, 'r') as runtime_file:
            mkl_runtime = float(runtime_file.read())
            unit_values.append(mkl_runtime)

      if device == 'gpu':
        # cuBLAS
        cublas_runtime = None
        runtime_file_name = cublas_file_pattern.format(device, app, 'x'.join(input_sizes[app]), '_runtime')
        if os.path.exists(runtime_file_name):
          with open(runtime_file_name, 'r') as runtime_file:
            cublas_runtime = float(runtime_file.read())
            unit_values.append(cublas_runtime)

      # CLBlast
      clblast_runtime = None
      runtime_file_name = clblast_file_pattern.format(device, platform_id[device], device_id[device], app, 'x'.join(input_sizes[app]), '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          clblast_runtime = float(runtime_file.read())
          unit_values.append(clblast_runtime)

    if device == 'cpu' and app == 'prl':
      # EKR
      ekr_runtime = None
      runtime_file_name = ekr_file_pattern.format(device, app, 'x'.join(input_sizes[app]), '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          ekr_runtime = float(runtime_file.read())
          unit_values.append(ekr_runtime)

    if device == 'gpu' and app == 'ccsdt':
      # TensorComprehensions
      tensor_comprehensions_min_runtime = None
      tensor_comprehensions_min_runtime_index = 0
      for run in range(0, num_runs):
        runtime_file_name = tensor_comprehensions_file_pattern.format(device, app, 'x'.join(input_sizes[app]), run, '_runtime')
        if os.path.exists(runtime_file_name):
          with open(runtime_file_name, 'r') as runtime_file:
            runtime = float(runtime_file.read())
            if tensor_comprehensions_min_runtime is None or runtime < tensor_comprehensions_min_runtime:
              tensor_comprehensions_min_runtime = runtime
              tensor_comprehensions_min_runtime_index = run
      if tensor_comprehensions_min_runtime is not None:
        unit_values.append(tensor_comprehensions_min_runtime)

      # COGENT
      cogent_runtime = None
      runtime_file_name = cogent_file_pattern.format(device, app, 'x'.join(input_sizes[app]), '_runtime')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          cogent_runtime = float(runtime_file.read())
          unit_values.append(cogent_runtime)

    # determine unit
    unit = 'ms' if len(unit_values) == 0 else 'us'
    for val in unit_values:
      if val >= 0.5:
        unit = 'ms'
        break

    tables[table_index][0][3 + (0 if device == 'cpu' else 6) + 4] = ('FAILED' if atf_min_runtime is None else '{:.2f} {}'.format(atf_min_runtime if unit == 'ms' else (atf_min_runtime * 1000), unit))
    tables[table_index][1][3 + (0 if device == 'cpu' else 6) + 4] = ('FAILED' if opentuner_min_runtime is None else '{:.2f} {}'.format(opentuner_min_runtime if unit == 'ms' else (opentuner_min_runtime * 1000), unit))
    tables[table_index][2][3 + (0 if device == 'cpu' else 6) + 4] = ('FAILED' if cltune_min_runtime is None else '{:.2f} {}'.format(cltune_min_runtime if unit == 'ms' else (cltune_min_runtime * 1000), unit))
    tables[table_index][3][3 + (0 if device == 'cpu' else 6) + 4] = ('FAILED' if cltune_pruned_min_runtime is None else '{:.2f} {}'.format(cltune_pruned_min_runtime if unit == 'ms' else (cltune_pruned_min_runtime * 1000), unit))
    tables[table_index][4][3 + (0 if device == 'cpu' else 6) + 4] = ('FAILED' if atf_former_min_runtime is None else '{:.2f} {}'.format(atf_former_min_runtime if unit == 'ms' else (atf_former_min_runtime * 1000), unit))
    if app == 'conv':
      if device == 'cpu':
        tables[table_index][5][3 + (0 if device == 'cpu' else 6) + 4] = ('' if mkl_dnn_runtime is None else '{:.2f} {}'.format(mkl_dnn_runtime if unit == 'ms' else (mkl_dnn_runtime * 1000), unit))
      if device == 'gpu':
        tables[table_index][5][3 + (0 if device == 'cpu' else 6) + 4] = ('' if cudnn_runtime is None else '{:.2f} {}'.format(cudnn_runtime if unit == 'ms' else (cudnn_runtime * 1000), unit))
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 4] = ('' if conv2d_runtime is None else '{:.2f} {}'.format(conv2d_runtime if unit == 'ms' else (conv2d_runtime * 1000), unit))
    if app == 'gemm':
      if device == 'cpu':
        tables[table_index][5][3 + (0 if device == 'cpu' else 6) + 4] = ('' if mkl_runtime is None else '{:.2f} {}'.format(mkl_runtime if unit == 'ms' else (mkl_runtime * 1000), unit))
      if device == 'gpu':
        tables[table_index][5][3 + (0 if device == 'cpu' else 6) + 4] = ('' if cublas_runtime is None else '{:.2f} {}'.format(cublas_runtime if unit == 'ms' else (cublas_runtime * 1000), unit))
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 4] = ('' if clblast_runtime is None else '{:.2f} {}'.format(clblast_runtime if unit == 'ms' else (clblast_runtime * 1000), unit))
    if device == 'cpu' and app == 'prl':
      tables[table_index][5][3 + (0 if device == 'cpu' else 6) + 4] = ('' if ekr_runtime is None else '{:.2f} {}'.format(ekr_runtime if unit == 'ms' else (ekr_runtime * 1000), unit))
    if device == 'gpu' and app == 'ccsdt':
      tables[table_index][5][3 + (0 if device == 'cpu' else 6) + 4] = ('' if tensor_comprehensions_min_runtime is None else '{:.2f} {}'.format(tensor_comprehensions_min_runtime if unit == 'ms' else (tensor_comprehensions_min_runtime * 1000), unit))
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 4] = ('' if cogent_runtime is None else '{:.2f} {}'.format(cogent_runtime if unit == 'ms' else (cogent_runtime * 1000), unit))


    # get generation time
    # ATF
    atf_generation_runtime = '>{:.1f} h'.format(tuning_time / 60.0)
    if atf_min_runtime is not None:
      generation_runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'ATF', app, 'x'.join(input_sizes[app]), atf_min_runtime_index, '_generation_time')
      if os.path.exists(generation_runtime_file_name):
        with open(generation_runtime_file_name, 'r') as runtime_file:
          try:
            atf_generation_runtime = int(runtime_file.read())
            if atf_generation_runtime > 1000 * 60 * 60:
              atf_generation_runtime = '{:.1f} h'.format(atf_generation_runtime / (1000.0 * 60 * 60))
            elif atf_generation_runtime > 1000 * 60:
              atf_generation_runtime = '{:.1f} min'.format(atf_generation_runtime / (1000.0 * 60))
            elif atf_generation_runtime > 1000:
              atf_generation_runtime = '{:.1f} s'.format(atf_generation_runtime / (1000.0))
            else:
              atf_generation_runtime = '{} ms'.format(atf_generation_runtime)
          except ValueError:
            pass

    # CLTune
    cltune_generation_runtime = '>{:.1f} h'.format(tuning_time / 60.0)
    if cltune_min_runtime is not None:
      generation_runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'CLTune', app, 'x'.join(input_sizes[app]), cltune_min_runtime_index, '_generation_time')
      if os.path.exists(generation_runtime_file_name):
        with open(generation_runtime_file_name, 'r') as runtime_file:
          try:
            cltune_generation_runtime = int(runtime_file.read())
            if cltune_generation_runtime > 1000 * 60 * 60:
              cltune_generation_runtime = '{:.1f} h'.format(cltune_generation_runtime / (1000.0 * 60 * 60))
            elif cltune_generation_runtime > 1000 * 60:
              cltune_generation_runtime = '{:.1f} min'.format(cltune_generation_runtime / (1000.0 * 60))
            elif cltune_generation_runtime > 1000:
              cltune_generation_runtime = '{:.1f} s'.format(cltune_generation_runtime / (1000.0))
            else:
              cltune_generation_runtime = '{} ms'.format(cltune_generation_runtime)
          except ValueError:
            pass

    # CLTuned (pruned)
    cltune_pruned_generation_runtime = '>{:.1f} h'.format(tuning_time / 60.0)
    if cltune_pruned_min_runtime is not None:
      generation_runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'CLTune_pruned', app, 'x'.join(input_sizes[app]), cltune_pruned_min_runtime_index, '_generation_time')
      if os.path.exists(generation_runtime_file_name):
        with open(generation_runtime_file_name, 'r') as runtime_file:
          try:
            cltune_pruned_generation_runtime = int(runtime_file.read())
            if cltune_pruned_generation_runtime > 1000 * 60 * 60:
              cltune_pruned_generation_runtime = '{:.1f} h'.format(cltune_pruned_generation_runtime / (1000.0 * 60 * 60))
            elif cltune_pruned_generation_runtime > 1000 * 60:
              cltune_pruned_generation_runtime = '{:.1f} min'.format(cltune_pruned_generation_runtime / (1000.0 * 60))
            elif cltune_pruned_generation_runtime > 1000:
              cltune_pruned_generation_runtime = '{:.1f} s'.format(cltune_pruned_generation_runtime / (1000.0))
            else:
              cltune_pruned_generation_runtime = '{} ms'.format(cltune_pruned_generation_runtime)
          except ValueError:
            pass

    # ATF (former)
    atf_former_generation_runtime = '>{:.1f} h'.format(tuning_time / 60.0)
    if atf_former_min_runtime is not None:
      generation_runtime_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'ATF_former', app, 'x'.join(input_sizes[app]), atf_former_min_runtime_index, '_generation_time')
      if os.path.exists(generation_runtime_file_name):
        with open(generation_runtime_file_name, 'r') as runtime_file:
          try:
            atf_former_generation_runtime = int(runtime_file.read())
            if atf_former_generation_runtime > 1000 * 60 * 60:
              atf_former_generation_runtime = '{:.1f} h'.format(atf_former_generation_runtime / (1000.0 * 60 * 60))
            elif atf_former_generation_runtime > 1000 * 60:
              atf_former_generation_runtime = '{:.1f} min'.format(atf_former_generation_runtime / (1000.0 * 60))
            elif atf_former_generation_runtime > 1000:
              atf_former_generation_runtime = '{:.1f} s'.format(atf_former_generation_runtime / (1000.0))
            else:
              atf_former_generation_runtime = '{} ms'.format(atf_former_generation_runtime)
          except ValueError:
            pass

    # Conv2D
    if app == 'conv':
      conv2d_generation_runtime = '>{:.1f} h'.format(tuning_time / 60.0)
      if conv2d_runtime is not None:
        runtime_file_name = conv2d_file_pattern.format(device, platform_id[device], device_id[device], app, 'x'.join(input_sizes[app]), '_generation_time')
        if os.path.exists(runtime_file_name):
          with open(runtime_file_name, 'r') as runtime_file:
            try:
              conv2d_generation_runtime = int(runtime_file.read())
              if conv2d_generation_runtime > 1000 * 60 * 60:
                conv2d_generation_runtime = '{:.1f} h'.format(conv2d_generation_runtime / (1000.0 * 60 * 60))
              elif conv2d_generation_runtime > 1000 * 60:
                conv2d_generation_runtime = '{:.1f} min'.format(conv2d_generation_runtime / (1000.0 * 60))
              elif conv2d_generation_runtime > 1000:
                conv2d_generation_runtime = '{:.1f} s'.format(conv2d_generation_runtime / (1000.0))
              else:
                conv2d_generation_runtime = '{} ms'.format(conv2d_generation_runtime)
            except ValueError:
              pass

    # CLBlast
    if app == 'gemm':
      clblast_generation_runtime = '>{:.1f} h'.format(tuning_time / 60.0)
      if clblast_runtime is not None:
        runtime_file_name = clblast_file_pattern.format(device, platform_id[device], device_id[device], app, 'x'.join(input_sizes[app]), '_generation_time')
        if os.path.exists(runtime_file_name):
          with open(runtime_file_name, 'r') as runtime_file:
            try:
              clblast_generation_runtime = int(runtime_file.read())
              if clblast_generation_runtime > 1000 * 60 * 60:
                clblast_generation_runtime = '{:.1f} h'.format(clblast_generation_runtime / (1000.0 * 60 * 60))
              elif clblast_generation_runtime > 1000 * 60:
                clblast_generation_runtime = '{:.1f} min'.format(clblast_generation_runtime / (1000.0 * 60))
              elif clblast_generation_runtime > 1000:
                clblast_generation_runtime = '{:.1f} s'.format(clblast_generation_runtime / (1000.0))
              else:
                clblast_generation_runtime = '{} ms'.format(clblast_generation_runtime)
            except ValueError:
              pass

    tables[table_index][0][3 + (0 if device == 'cpu' else 6) + 0] = atf_generation_runtime
    tables[table_index][2][3 + (0 if device == 'cpu' else 6) + 0] = cltune_generation_runtime
    tables[table_index][3][3 + (0 if device == 'cpu' else 6) + 0] = cltune_pruned_generation_runtime
    tables[table_index][4][3 + (0 if device == 'cpu' else 6) + 0] = atf_former_generation_runtime
    if app == 'conv':
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 0] = conv2d_generation_runtime
    if app == 'gemm':
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 0] = clblast_generation_runtime


    # get search space size
    # constrained
    constrained_sp_size = ''
    if app == 'conv':
      constrained_sp_size = '1.69E+08'
    elif app == 'gemm':
      constrained_sp_size = '2.51E+08'
    elif app == 'ccsdt':
      constrained_sp_size = '8.81E+18'
    elif app == 'prl':
      constrained_sp_size = '2.31E+07'

    # unconstrained
    unconstrained_sp_size = ''
    if app == 'conv':
      unconstrained_sp_size = '1.13E+31'
    elif app == 'gemm':
      unconstrained_sp_size = '1.02E+25'
    elif app == 'ccsdt':
      unconstrained_sp_size = '5.99E+43'
    elif app == 'prl':
      unconstrained_sp_size = '1.74E+26'

    # CLTuned (pruned)
    cltune_pruned_sp_size = ''
    if app == 'conv':
      cltune_pruned_sp_size = '48'
    elif app == 'gemm':
      cltune_pruned_sp_size = '3,024'
    elif app == 'ccsdt':
      cltune_pruned_sp_size = '2.32E+09'
    elif app == 'prl':
      cltune_pruned_sp_size = '1.75E+05'

    # Conv2D
    if app == 'conv':
      conv2d_sp_size = ''
      runtime_file_name = conv2d_file_pattern.format(device, platform_id[device], device_id[device], app, 'x'.join(input_sizes[app]), '_sp_size')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          try:
            conv2d_sp_size = int(runtime_file.read())
            if conv2d_sp_size >= 10000:
              conv2d_sp_size = '{:.2E}'.format(conv2d_sp_size)
            else:
              conv2d_sp_size = '{:,d}'.format(conv2d_sp_size)
          except ValueError:
            pass

    # CLBlast
    if app == 'gemm':
      clblast_sp_size = ''
      runtime_file_name = clblast_file_pattern.format(device, platform_id[device], device_id[device], app, 'x'.join(input_sizes[app]), '_sp_size')
      if os.path.exists(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
          try:
            clblast_sp_size = int(runtime_file.read())
            if clblast_sp_size >= 10000:
              clblast_sp_size = '{:.2E}'.format(clblast_sp_size)
            else:
              clblast_sp_size = '{:,d}'.format(clblast_sp_size)
          except ValueError:
            pass

    tables[table_index][0][3 + (0 if device == 'cpu' else 6) + 1] = constrained_sp_size
    tables[table_index][1][3 + (0 if device == 'cpu' else 6) + 1] = unconstrained_sp_size
    tables[table_index][2][3 + (0 if device == 'cpu' else 6) + 1] = constrained_sp_size
    tables[table_index][3][3 + (0 if device == 'cpu' else 6) + 1] = cltune_pruned_sp_size
    tables[table_index][4][3 + (0 if device == 'cpu' else 6) + 1] = constrained_sp_size
    if app == 'conv':
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 1] = conv2d_sp_size
    if app == 'gemm':
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 1] = clblast_sp_size


    # get num valid configurations
    # ATF
    atf_num_valid = '0'
    if atf_min_runtime is not None:
      sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'ATF', app, 'x'.join(input_sizes[app]), atf_min_runtime_index, '_num_valid')
      if os.path.exists(sp_size_file_name):
        with open(sp_size_file_name, 'r') as runtime_file:
          try:
            atf_num_valid = int(runtime_file.read())
            atf_num_valid = '{:,d}'.format(atf_num_valid)
          except ValueError:
            pass

    # OpenTuner
    opentuner_num_valid = '0'
    if opentuner_min_runtime is not None:
      sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'OpenTuner', app, 'x'.join(input_sizes[app]), opentuner_min_runtime_index, '_num_valid')
      if os.path.exists(sp_size_file_name):
        with open(sp_size_file_name, 'r') as runtime_file:
          try:
            opentuner_num_valid = int(runtime_file.read())
            opentuner_num_valid = '{:,d}'.format(opentuner_num_valid)
          except ValueError:
            pass

    # CLTune
    cltune_num_valid = '0'
    if cltune_min_runtime is not None:
      sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'CLTune', app, 'x'.join(input_sizes[app]), cltune_min_runtime_index, '_num_valid')
      if os.path.exists(sp_size_file_name):
        with open(sp_size_file_name, 'r') as runtime_file:
          try:
            cltune_num_valid = int(runtime_file.read())
            cltune_num_valid = '{:,d}'.format(cltune_num_valid)
          except ValueError:
            pass

    # CLTune (pruned)
    cltune_pruned_num_valid = '0'
    if cltune_pruned_min_runtime is not None:
      sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'CLTune_pruned', app, 'x'.join(input_sizes[app]), cltune_pruned_min_runtime_index, '_num_valid')
      if os.path.exists(sp_size_file_name):
        with open(sp_size_file_name, 'r') as runtime_file:
          try:
            cltune_pruned_num_valid = int(runtime_file.read())
            cltune_pruned_num_valid = '{:,d}'.format(cltune_pruned_num_valid)
          except ValueError:
            pass

    # ATF (former)
    atf_former_num_valid = '0'
    if atf_former_min_runtime is not None:
      sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'ATF_former', app, 'x'.join(input_sizes[app]), atf_former_min_runtime_index, '_num_valid')
      if os.path.exists(sp_size_file_name):
        with open(sp_size_file_name, 'r') as runtime_file:
          try:
            atf_former_num_valid = int(runtime_file.read())
            atf_former_num_valid = '{:,d}'.format(atf_former_num_valid)
          except ValueError:
            pass

    if device == 'gpu' and app == 'ccsdt':
      # TensorComprehensions
      tensor_comprehensions_num_valid = '0'
      if tensor_comprehensions_min_runtime is not None:
        sp_size_file_name = tensor_comprehensions_file_pattern.format(device, app, 'x'.join(input_sizes[app]), tensor_comprehensions_min_runtime_index, '_num_valid')
        if os.path.exists(sp_size_file_name):
          with open(sp_size_file_name, 'r') as runtime_file:
            try:
              tensor_comprehensions_num_valid = int(runtime_file.read())
              tensor_comprehensions_num_valid = '{:,d}'.format(tensor_comprehensions_num_valid)
            except ValueError:
              pass

    tables[table_index][0][3 + (0 if device == 'cpu' else 6) + 2] = atf_num_valid
    tables[table_index][1][3 + (0 if device == 'cpu' else 6) + 2] = opentuner_num_valid
    tables[table_index][2][3 + (0 if device == 'cpu' else 6) + 2] = cltune_num_valid
    tables[table_index][3][3 + (0 if device == 'cpu' else 6) + 2] = cltune_pruned_num_valid
    tables[table_index][4][3 + (0 if device == 'cpu' else 6) + 2] = atf_former_num_valid
    if app == 'conv':
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 2] = conv2d_sp_size
    if app == 'gemm':
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 2] = clblast_sp_size
    if device == 'gpu' and app == 'ccsdt':
      tables[table_index][5][3 + (0 if device == 'cpu' else 6) + 2] = tensor_comprehensions_num_valid


    # get num invalid configurations
    # ATF
    atf_num_invalid = '0'
    sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'ATF', app, 'x'.join(input_sizes[app]), atf_min_runtime_index, '_num_invalid')
    if os.path.exists(sp_size_file_name):
      with open(sp_size_file_name, 'r') as runtime_file:
        try:
          atf_num_invalid = int(runtime_file.read())
          atf_num_invalid = '{:,d}'.format(atf_num_invalid)
        except ValueError:
          pass

    # OpenTuner
    opentuner_num_invalid = '0'
    sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'OpenTuner', app, 'x'.join(input_sizes[app]), opentuner_min_runtime_index, '_num_invalid')
    if os.path.exists(sp_size_file_name):
      with open(sp_size_file_name, 'r') as runtime_file:
        try:
          opentuner_num_invalid = int(runtime_file.read())
          opentuner_num_invalid = '{:,d}'.format(opentuner_num_invalid)
        except ValueError:
          pass

    # CLTune
    cltune_num_invalid = '0'
    sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'CLTune', app, 'x'.join(input_sizes[app]), cltune_min_runtime_index, '_num_invalid')
    if os.path.exists(sp_size_file_name):
      with open(sp_size_file_name, 'r') as runtime_file:
        try:
          cltune_num_invalid = int(runtime_file.read())
          cltune_num_invalid = '{:,d}'.format(cltune_num_invalid)
        except ValueError:
          pass

    # CLTune (pruned)
    cltune_pruned_num_invalid = '0'
    sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'CLTune_pruned', app, 'x'.join(input_sizes[app]), cltune_pruned_min_runtime_index, '_num_invalid')
    if os.path.exists(sp_size_file_name):
      with open(sp_size_file_name, 'r') as runtime_file:
        try:
          cltune_pruned_num_invalid = int(runtime_file.read())
          cltune_pruned_num_invalid = '{:,d}'.format(cltune_pruned_num_invalid)
        except ValueError:
          pass

    # ATF (former)
    atf_former_num_invalid = '0'
    sp_size_file_name = file_pattern.format(device, platform_id[device], device_id[device], 'ATF_former', app, 'x'.join(input_sizes[app]), atf_former_min_runtime_index, '_num_invalid')
    if os.path.exists(sp_size_file_name):
      with open(sp_size_file_name, 'r') as runtime_file:
        try:
          atf_former_num_invalid = int(runtime_file.read())
          atf_former_num_invalid = '{:,d}'.format(atf_former_num_invalid)
        except ValueError:
          pass

    tables[table_index][0][3 + (0 if device == 'cpu' else 6) + 3] = atf_num_invalid
    tables[table_index][1][3 + (0 if device == 'cpu' else 6) + 3] = opentuner_num_invalid
    tables[table_index][2][3 + (0 if device == 'cpu' else 6) + 3] = cltune_num_invalid
    tables[table_index][3][3 + (0 if device == 'cpu' else 6) + 3] = cltune_pruned_num_invalid
    tables[table_index][4][3 + (0 if device == 'cpu' else 6) + 3] = atf_former_num_invalid
    if app == 'conv':
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 3] = '0'
    if app == 'gemm':
      tables[table_index][6][3 + (0 if device == 'cpu' else 6) + 3] = '0'
    if device == 'gpu' and app == 'ccsdt':
      tables[table_index][5][3 + (0 if device == 'cpu' else 6) + 3] = '0'

    table_index += 1



# print tables
width = [0] * len(tables[0][0])
height = []
for tbl in tables:
  for row in tbl:
    height.append(0)
global_row_index = 0
for tbl in tables:
  for row_index in range(0, len(tbl)):
    for col_index in range(0, len(tbl[row_index])):
      val = tbl[row_index][col_index]
      if isinstance(val, span):
        span_width = width[col_index]
        for i in range(1, val.columns):
          span_width += width[col_index + i] + 3
        if len(val.text) > span_width:
          to_add = len(val.text) - span_width - 3 * (val.columns - 1)

          coi = 0
          while to_add > 0:
            if min(width[col_index:col_index+val.columns - 1]) == max(width[col_index:col_index+val.columns - 1]):
              width[col_index + coi] += 1
              to_add -= 1
            elif width[col_index + coi] < min(width[col_index:col_index+val.columns - 1]):
              width[col_index + coi] += 1
              to_add -= 1
            coi = (coi + 1) % val.columns
      elif isinstance(val, str):
        tmp_height = 0
        for sub_str in val.split('\n'):
          width[col_index] = max(width[col_index], len(sub_str))
          tmp_height += 1
        height[global_row_index + row_index] = max(height[global_row_index + row_index], tmp_height)
      else:
        print('expecting str, got {} for value {}'.format(type(val), val))
        exit(1)
  global_row_index += len(tbl)

global_row_index = 0
header = True
for tbl in tables:
  print('+', end='')
  for w in width:
    if w == 0:
      print('  +', end='')
    else:
      print('-' * (w + 2) + '+', end='')
  print('')

  for row_index in range(0, len(tbl)):
    for h in range(0, height[global_row_index + row_index]):
      print('| ', end='')
      span_count = 0
      alignment = None
      for col_index in range(0, len(tbl[row_index])):
        if span_count > 0:
          span_count -= 1
          continue
        val = tbl[row_index][col_index]
        w = width[col_index]
        if isinstance(val, span):
          for i in range(1, val.columns):
            w += width[col_index + i] + 3
          span_count = val.columns - 1
          val = val.text
          alignment = 'c'
        elif isinstance(val, str):
          val = val.split('\n')
          if h < len(val):
            val = val[h]
          else:
            val = ''
          if val == ' -- ' or val == '?':
            alignment = 'c'
          else:
            alignment = 'r'
        else:
          print('expecting str, got {} for value {}'.format(type(val), val))
          exit(1)

        if header or alignment == 'c':
          to_add = w - len(val)
          val = ' ' * int(to_add / 2) + val
          val = val + ' ' * int((to_add + 1) / 2)
        elif col_index == 1 or alignment == 'l':
          val = val.ljust(w, ' ')
        else:
          val = val.rjust(w, ' ')
        print(val, end='')
        print(' | ', end='')
      print('')
  global_row_index += len(tbl)

  print('+', end='')
  for w in width:
    if w == 0:
      print('  +', end='')
    else:
      print('-' * (w + 2) + '+', end='')
  print('')

  header = False