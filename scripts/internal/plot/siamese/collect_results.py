from __future__ import print_function

import json
import os

def format_ms(ms):
  unit = 'ms'
  if ms > 1000:
    ms /= 1000
    unit = 's'
  if ms > 60:
    ms /= 60
    unit = 'min'
  if ms > 60:
    ms /= 60
    unit = 'h'
  return '%.1f %s' % (ms, unit)

def print_table(tables):
  width = [0] * len(tables[0][0])
  height = []
  for tbl in tables:
    for row in tbl:
      height.append(0)
  global_row_index = 0
  for tbl in tables:
    for row_index in range(0, len(tbl)):
      for col_index in range(0, len(tbl[row_index])):
        val = tbl[row_index][col_index]
        if isinstance(val, span):
          span_width = width[col_index]
          for i in range(1, val.columns):
            span_width += width[col_index + i] + 3
          if len(val.text) > span_width:
            to_add = len(val.text) - span_width - 3 * (val.columns - 1)

            coi = 0
            while to_add > 0:
              if min(width[col_index:col_index+val.columns - 1]) == max(width[col_index:col_index+val.columns - 1]):
                width[col_index + coi] += 1
                to_add -= 1
              elif width[col_index + coi] < min(width[col_index:col_index+val.columns - 1]):
                width[col_index + coi] += 1
                to_add -= 1
              coi = (coi + 1) % val.columns
        elif isinstance(val, str):
          tmp_height = 0
          for sub_str in val.split('\n'):
            width[col_index] = max(width[col_index], len(sub_str))
            tmp_height += 1
          height[global_row_index + row_index] = max(height[global_row_index + row_index], tmp_height)
        else:
          print('expecting str, got {} for value {}'.format(type(val), val))
          exit(1)
    global_row_index += len(tbl)

  global_row_index = 0
  header = True
  for tbl in tables:
    print('+', end='')
    for w in width:
      if w == 0:
        print('  +', end='')
      else:
        print('-' * (w + 2) + '+', end='')
    print('')

    for row_index in range(0, len(tbl)):
      for h in range(0, height[global_row_index + row_index]):
        print('| ', end='')
        span_count = 0
        alignment = None
        for col_index in range(0, len(tbl[row_index])):
          if span_count > 0:
            span_count -= 1
            continue
          val = tbl[row_index][col_index]
          w = width[col_index]
          if isinstance(val, span):
            for i in range(1, val.columns):
              w += width[col_index + i] + 3
            span_count = val.columns - 1
            val = val.text
            alignment = 'c'
          elif isinstance(val, str):
            val = val.split('\n')
            if h < len(val):
              val = val[h]
            else:
              val = ''
            if val == ' -- ' or val == '?':
              alignment = 'c'
            else:
              alignment = 'r'
          else:
            print('expecting str, got {} for value {}'.format(type(val), val))
            exit(1)

          if header or alignment == 'c':
            to_add = w - len(val)
            val = ' ' * int(to_add / 2) + val
            val = val + ' ' * int((to_add + 1) / 2)
          elif col_index == 1 or alignment == 'l':
            val = val.ljust(w, ' ')
          else:
            val = val.rjust(w, ' ')
          print(val, end='')
          print(' | ', end='')
        print('')
    global_row_index += len(tbl)

    print('+', end='')
    for w in width:
      if w == 0:
        print('  +', end='')
      else:
        print('-' * (w + 2) + '+', end='')
    print('')

    header = False

if os.environ.get('ARTIFACT_ROOT') == None or os.environ['ARTIFACT_ROOT'] == '':
  print('Please set the environement variable ARTIFACT_ROOT to the root directory of the artifact before executing this script!')
  exit(1)

experiments_json = '{}/{}'.format(os.environ['ARTIFACT_ROOT'], os.environ['EXPERIMENTS_FILE'])
with open(experiments_json, 'r') as json_file:
  json_data = json.load(json_file)
  devices_to_run_on = [x.lower() for x in json_data['siamese']['run experiments on']]
  platform_id = {
    'cpu': json_data['siamese']['CPU OpenCL platform id'],
    'gpu': json_data['siamese']['GPU OpenCL platform id']
  }
  device_id = {
    'cpu': json_data['siamese']['CPU OpenCL device id'],
    'gpu': json_data['siamese']['GPU OpenCL device id']
  }
  tuning_time = json_data['siamese']['tuning time (minutes)']

class span:
  def __init__(self, columns, text):
    self.columns = columns
    self.text = text

tables_overall = [
  [
    [       '', span(3, 'CPU'), '', '',    span(3, 'GPU'), '', ''    ],
    ['backend', 'ATLAS', 'CLBlast', 'ATF', 'cuBLAS', 'CLBlast', 'ATF']
  ],

  [
    ['runtime', '', '', '', '', '', ''],
    ['speedup', '', '', '', '', '', '']
  ]
]

tables_detail = [
  [
    [    '', '', '', '',                 '',  '',  span(5, 'CPU'), '', '', '', '',                                       '',   span(5, 'GPU'), '', '', '', ''                              ],
    [    '', span(3, 'Input Size'), '', '', 'num',    '',    'ATLAS', 'CLBlast',    'speedup',     'ATF',    'speedup',  '',   'cuBLAS', 'CLBlast',     'speedup',     'ATF',     'speedup'],
    [ 'No.', 'M', 'N', 'K',                 'calls',  '',  'runtime', 'runtime', 'over ATLAS', 'runtime', 'over ATLAS',  '',  'runtime', 'runtime', 'over cuBLAS', 'runtime', 'over cuBLAS']
  ],

  [
    [ '1.',  '50',  '64', '500', '8420128',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    [ '2.',  '20', '576',  '25', '8420128',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    [ '3.',  '20', '576',   '1', '8420128',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    [ '4.',  '50',  '64',   '1', '8420128',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    [ '5.', '500',  '64',  '50', '6400000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    [ '6.',  '50', '500',  '64', '6400000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    [ '7.',  '20',  '25', '576', '6400000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    [ '8.',  '64', '500', '800',  '100002',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    [ '9.',  '64',  '10', '500',  '100002',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['10.',  '64', '500',   '1',  '100002',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['11.',  '64',  '10',   '1',  '100002',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['12.',  '64',   '2',  '10',  '100002',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['13.',  '64',   '2',   '1',  '100002',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['14.', '500', '800',  '64',  '100000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['15.',  '64', '800', '500',  '100000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['16.',  '64', '500',  '10',  '100000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['17.',  '10', '500',  '64',  '100000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['18.',  '64',  '10',   '2',  '100000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['19.',   '2',  '10',  '64',  '100000',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['20.', '100', '500', '800',   '20200',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['21.', '100',  '10', '500',   '20200',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['22.', '100', '500',   '1',   '20200',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['23.', '100',  '10',   '1',   '20200',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['24.', '100',   '2',  '10',   '20200',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     ''],
    ['25.', '100',   '2',   '1',   '20200',  '',  '', '', '', ''    , '',  '',  '', '', '', '',     '']
  ]
]

### collect results from log files
atf_file_pattern = '{0}/results/siamese/{{0}}/{{1}}/{{2}}/ATF/gemm_{{3}}_runtime'.format(os.environ['ARTIFACT_ROOT'])
clblast_file_pattern = '{0}/results/siamese/{{0}}/{{1}}/{{2}}/CLBlast/gemm_{{3}}_runtime'.format(os.environ['ARTIFACT_ROOT'])
atlas_file_pattern = '{0}/results/siamese/cpu/ATLAS/gemm_{{0}}_runtime'.format(os.environ['ARTIFACT_ROOT'])
cublas_file_pattern = '{0}/results/siamese/gpu/cuBLAS/gemm_{{0}}_runtime'.format(os.environ['ARTIFACT_ROOT'])
caffe_file_pattern = '{0}/results/siamese/{{0}}/caffe/runtime'.format(os.environ['ARTIFACT_ROOT'])

# collect caffe results
caffe_cpu_base_runtime = None
caffe_gpu_base_runtime = None
caffe_runtime_file = caffe_file_pattern.format('cpu')
if os.path.exists(caffe_runtime_file):
  with open(caffe_runtime_file, 'r') as runtime_file:
    caffe_cpu_base_runtime = float(runtime_file.read())
caffe_runtime_file = caffe_file_pattern.format('gpu')
if os.path.exists(caffe_runtime_file):
  with open(caffe_runtime_file, 'r') as runtime_file:
    caffe_gpu_base_runtime = float(runtime_file.read())

# collect detailed results
atlas_total_runtime = 0
cublas_total_runtime = 0
atf_cpu_total_runtime = 0
atf_gpu_total_runtime = 0
clblast_cpu_total_runtime = 0
clblast_gpu_total_runtime = 0
device_table_index = 0
for device in devices_to_run_on:
  for row in range(0, len(tables_detail[1])):
    input_size = 'x'.join(tables_detail[1][row][1:4])
    num_calls  = int(tables_detail[1][row][4])

    # ATLAS
    atlas_runtime = None
    if device == 'cpu':
      atlas_runtime_file = atlas_file_pattern.format(input_size)
      if os.path.exists(atlas_runtime_file):
        with open(atlas_runtime_file, 'r') as runtime_file:
          atlas_runtime = float(runtime_file.read())
    if atlas_runtime is not None:
      caffe_cpu_base_runtime -= num_calls * atlas_runtime
      atlas_total_runtime += num_calls * atlas_runtime

    # cuBLAS
    cublas_runtime = None
    if device == 'gpu':
      cublas_runtime_file = cublas_file_pattern.format(input_size)
      if os.path.exists(cublas_runtime_file):
        with open(cublas_runtime_file, 'r') as runtime_file:
          cublas_runtime = float(runtime_file.read())
    if cublas_runtime is not None:
      caffe_gpu_base_runtime -= num_calls * cublas_runtime
      cublas_total_runtime += num_calls * cublas_runtime

    # ATF
    atf_runtime_file = atf_file_pattern.format(device, platform_id[device], device_id[device], input_size)
    atf_runtime = None
    if os.path.exists(atf_runtime_file):
      with open(atf_runtime_file, 'r') as runtime_file:
        atf_runtime = float(runtime_file.read())
    if atf_runtime is not None:
      if device == 'cpu':
        atf_cpu_total_runtime += num_calls * atf_runtime
      if device == 'gpu':
        atf_gpu_total_runtime += num_calls * atf_runtime

    # CLBlast
    clblast_runtime_file = clblast_file_pattern.format(device, platform_id[device], device_id[device], input_size)
    clblast_runtime = None
    if os.path.exists(clblast_runtime_file):
      with open(clblast_runtime_file, 'r') as runtime_file:
        clblast_runtime = float(runtime_file.read())
    if clblast_runtime is not None:
      if device == 'cpu':
        clblast_cpu_total_runtime += num_calls * clblast_runtime
      if device == 'gpu':
        clblast_gpu_total_runtime += num_calls * clblast_runtime

    # calculate speedups
    atf_speedup = None
    if atf_runtime is not None:
      if device == 'cpu' and atlas_runtime is not None:
        atf_speedup = atlas_runtime / atf_runtime
      if device == 'gpu' and cublas_runtime is not None:
        atf_speedup = cublas_runtime / atf_runtime
    clblast_speedup = None
    if clblast_runtime is not None:
      if device == 'cpu' and atlas_runtime is not None:
        clblast_speedup = atlas_runtime / clblast_runtime
      if device == 'gpu' and cublas_runtime is not None:
        clblast_speedup = cublas_runtime / clblast_runtime

    if atlas_runtime is not None:
      tables_detail[1][row][6+device_table_index*6+0] = '%.4f ms' % atlas_runtime
    if cublas_runtime is not None:
      tables_detail[1][row][6+device_table_index*6+0] = '%.4f ms' % cublas_runtime
    if clblast_runtime is not None:
      tables_detail[1][row][6+device_table_index*6+1] = '%.4f ms' % clblast_runtime
    if clblast_speedup is not None:
      tables_detail[1][row][6+device_table_index*6+2] = '%.2f' % clblast_speedup
    if atf_runtime is not None:
      tables_detail[1][row][6+device_table_index*6+3] = '%.4f ms' % atf_runtime
    if atf_speedup is not None:
      tables_detail[1][row][6+device_table_index*6+4] = '%.2f' % atf_speedup

  device_table_index += 1

# insert overall data into table
caffe_runtime_atlas = None
if caffe_cpu_base_runtime is not None and atlas_total_runtime > 0:
  caffe_runtime_atlas = caffe_cpu_base_runtime + atlas_total_runtime
  tables_overall[1][0][1] = format_ms(caffe_runtime_atlas)
  caffe_speedup_atlas = caffe_runtime_atlas / caffe_runtime_atlas
  tables_overall[1][1][1] = '%.1f' % caffe_speedup_atlas
caffe_runtime_cublas = None
if caffe_gpu_base_runtime is not None and cublas_total_runtime > 0:
  caffe_runtime_cublas = caffe_gpu_base_runtime + cublas_total_runtime
  tables_overall[1][0][4] = format_ms(caffe_runtime_cublas)
  caffe_speedup_cublas = caffe_runtime_cublas / caffe_runtime_cublas
  tables_overall[1][1][4] = '%.1f' % caffe_speedup_cublas
caffe_runtime_clblast_cpu = None
if caffe_cpu_base_runtime is not None and clblast_cpu_total_runtime > 0:
  caffe_runtime_clblast_cpu = caffe_cpu_base_runtime + clblast_cpu_total_runtime
  tables_overall[1][0][2] = format_ms(caffe_runtime_clblast_cpu)
  if caffe_runtime_atlas is not None:
    caffe_speedup_clblast_cpu = caffe_runtime_atlas / caffe_runtime_clblast_cpu
    tables_overall[1][1][2] = '%.1f' % caffe_speedup_clblast_cpu
caffe_runtime_clblast_gpu = None
if caffe_gpu_base_runtime is not None and clblast_gpu_total_runtime > 0:
  caffe_runtime_clblast_gpu = caffe_gpu_base_runtime + clblast_gpu_total_runtime
  tables_overall[1][0][5] = format_ms(caffe_runtime_clblast_gpu)
  if caffe_runtime_cublas is not None:
    caffe_speedup_clblast_gpu = caffe_runtime_cublas / caffe_runtime_clblast_gpu
    tables_overall[1][1][5] = '%.1f' % caffe_speedup_clblast_gpu
caffe_runtime_atf_cpu = None
if caffe_cpu_base_runtime is not None and atf_cpu_total_runtime > 0:
  caffe_runtime_atf_cpu = caffe_cpu_base_runtime + atf_cpu_total_runtime
  tables_overall[1][0][3] = format_ms(caffe_runtime_atf_cpu)
  if caffe_runtime_atlas is not None:
    caffe_speedup_atf_cpu = caffe_runtime_atlas / caffe_runtime_atf_cpu
    tables_overall[1][1][3] = '%.1f' % caffe_speedup_atf_cpu
caffe_runtime_atf_gpu = None
if caffe_gpu_base_runtime is not None and atf_gpu_total_runtime > 0:
  caffe_runtime_atf_gpu = caffe_gpu_base_runtime + atf_gpu_total_runtime
  tables_overall[1][0][6] = format_ms(caffe_runtime_atf_gpu)
  if caffe_runtime_cublas is not None:
    caffe_speedup_atf_gpu = caffe_runtime_cublas / caffe_runtime_atf_gpu
    tables_overall[1][1][6] = '%.1f' % caffe_speedup_atf_gpu


# print tables
print_table(tables_overall)
print()
print()
print_table(tables_detail)