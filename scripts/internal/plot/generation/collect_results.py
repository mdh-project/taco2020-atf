import math
import os
import csv
import json

if os.environ.get('ARTIFACT_ROOT') == None or os.environ['ARTIFACT_ROOT'] == '':
  print('Please set the environement variable ARTIFACT_ROOT to the root directory of the artifact before executing this script!')
  exit(1)

python_constraint_ssc = False
if os.environ.get('ENABLE_PYTHON_CONSTRAINT_SSC') is not None and os.environ['ENABLE_PYTHON_CONSTRAINT_SSC'] == '1':
  python_constraint_ssc = True

experiments_json = '{}/{}'.format(os.environ['ARTIFACT_ROOT'], os.environ['EXPERIMENTS_FILE'])
with open(experiments_json, 'r') as json_file:
  json_data = json.load(json_file)
  platform_id = json_data['generation']['OpenCL platform id']
  device_id = json_data['generation']['OpenCL device id']
  input_sizes = json_data['generation']['input sizes (square)']

log_file_pattern    = '{0}/results/generation/{1}/{2}/{{0}}/{{1}}_{{2}}.log'.format(os.environ['ARTIFACT_ROOT'], platform_id, device_id)
result_file_pattern = '{0}/results/generation/{1}/{2}/{{0}}/{{1}}_{{2}}'.format(os.environ['ARTIFACT_ROOT'], platform_id, device_id)
output_file_pattern = '{0}/results/generation/{{0}}_{{1}}_plot.csv'.format(os.environ['ARTIFACT_ROOT'])

frameworks = ['ATF', 'ATF_former', 'python-constraint', 'CLTune']
if python_constraint_ssc:
  frameworks.append('python-constraint-ssc')

applications = dict()
applications['conv'] = 2
applications['gemm'] = 3
applications['ccsdt'] = 7
applications['prl'] = 2
for app in applications.keys():
  for fw in frameworks:
    with open(output_file_pattern.format(app, fw), 'w') as output_file:
      csv_writer = csv.writer(output_file, delimiter=';')
      for input_size in input_sizes:
        result_file_name = result_file_pattern.format(fw, app, 'x'.join([str(input_size)] * applications[app]))
        if os.path.exists(result_file_name):
          with open(result_file_name, 'r') as result_file:
            result = result_file.read().lstrip().rstrip()
            try:
              result = math.log(int(result), 1900)
            except:
              try:
                result = math.log(float(result), 1900)
              except:
                result = 'timeout'
            csv_writer.writerow([input_size, result])
