import os
import csv
import json

if os.environ.get('ARTIFACT_ROOT') == None or os.environ['ARTIFACT_ROOT'] == '':
  print('Please set the environement variable ARTIFACT_ROOT to the root directory of the artifact before executing this script!')
  exit(1)

experiments_json = '{}/{}'.format(os.environ['ARTIFACT_ROOT'], os.environ['EXPERIMENTS_FILE'])
input_sizes = dict()
with open(experiments_json, 'r') as json_file:
  json_data = json.load(json_file)
  platform_id = json_data['exploration']['OpenCL platform id']
  device_id = json_data['exploration']['OpenCL device id']
  input_sizes['conv'] = json_data['exploration']['CONV']['input size']
  input_sizes['gemm'] = json_data['exploration']['GEMM']['input size']
  input_sizes['ccsdt'] = json_data['exploration']['CCSD(T)']['input size']
  input_sizes['prl'] = json_data['exploration']['PRL']['input size']
  num_runs = json_data['exploration']['num. runs']

log_file_pattern    = '{0}/results/exploration/{1}/{2}/{{0}}/{{1}}_{{2}}_{{3}}.log'.format(os.environ['ARTIFACT_ROOT'], platform_id, device_id)
result_file_pattern = '{0}/results/exploration/{1}/{2}/{{0}}/{{1}}_{{2}}_{{3}}'.format(os.environ['ARTIFACT_ROOT'], platform_id, device_id)
output_file_pattern = '{0}/results/exploration/{{0}}_plot.csv'.format(os.environ['ARTIFACT_ROOT'])

for app in input_sizes.keys():
  with open(output_file_pattern.format(app), 'w') as output_file:
    for run in range(0, num_runs):
      for fw in ['SA 1D', 'SA MD', 'AUC 1D', 'AUC MD']:
        result_file_name = result_file_pattern.format(fw.replace(' ', '_'), app, 'x'.join([str(x) for x in input_sizes[app]]), run)
        if os.path.exists(result_file_name):
          with open(result_file_name, 'r') as result_file:
            result = result_file.read().lstrip().rstrip()
            output_file.write('{};'.format(result))
        else:
          output_file.write(';')
      output_file.write('\n')