#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."CCSD(T)"."input size"'` && \
cd $BUILD_FOLDER/evaluation/overall/TensorComprehensions && \
mkdir -p $ARTIFACT_ROOT/results/overall/gpu/TensorComprehensions && \
(
  FILES_SUFFIX="_$1" && \
  START=`date +%s` && \
  while [ $(( $(date +%s) - `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."tuning time (minutes)"'` * 60 )) -lt $START ]; do
    python tune.py  abcdef gebc dfga `echo $input_size | jq -r '[.[] | tostring] | join(" ")'`
  done && \
  python bench.py abcdef gebc dfga `echo $input_size | jq -r '[.[] | tostring] | join(" ")'`
) |& tee "$ARTIFACT_ROOT/results/overall/gpu/TensorComprehensions/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log"
num_generations=$(grep -aoP "Generation: \K[0-9]+" "$ARTIFACT_ROOT/results/overall/gpu/TensorComprehensions/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log" | tail -n1)
echo $((num_generations * 100)) > "$ARTIFACT_ROOT/results/overall/gpu/TensorComprehensions/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1_num_valid"