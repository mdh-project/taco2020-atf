#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."CONV"."input size"'` && \
cd $BUILD_FOLDER/evaluation/overall/cuDNN && \
mkdir -p $ARTIFACT_ROOT/results/overall/gpu/cuDNN && \
./overall_cudnn_conv --device-id 0 --input-size `echo $input_size | jq -r '[.[] | tostring] | join(" ")'` |& tee "$ARTIFACT_ROOT/results/overall/gpu/cuDNN/conv_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log"