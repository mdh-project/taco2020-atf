#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

$ARTIFACT_ROOT/scripts/internal/overall_gpu/ATF/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/ATF_former/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/CLBlast/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/CLTune/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/CLTune_pruned/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/Conv2D/run_all.sh && \
if [[ -n "$ENABLE_COGENT" && "$ENABLE_COGENT" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_gpu/COGENT/run_all.sh
fi && \
if [[ -n "$ENABLE_CUBLAS" && "$ENABLE_CUBLAS" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_gpu/cuBLAS/run_all.sh
fi && \
if [[ -n "$ENABLE_CUDNN" && "$ENABLE_CUDNN" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_gpu/cuDNN/run_all.sh
fi && \
if [[ -n "$ENABLE_TENSOR_COMPREHENSIONS" && "$ENABLE_TENSOR_COMPREHENSIONS" -eq 1 ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_gpu/TensorComprehensions/run_all.sh
fi && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/OpenTuner/run_all.sh