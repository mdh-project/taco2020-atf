#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

$ARTIFACT_ROOT/scripts/internal/overall_gpu/ATF_former/CONV/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/ATF_former/GEMM/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/ATF_former/CCSDT/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/overall_gpu/ATF_former/PRL/run_all.sh