#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."CCSD(T)"."input size"'` && \
is="[[\"abcdef\", \"gebc\", \"dfga\"], $input_size]" && \
cd $BUILD_FOLDER/evaluation/overall/COGENT && \
mkdir -p $ARTIFACT_ROOT/results/overall/gpu/COGENT && \
(
  cp bench_pattern.sh bench.sh &&
  sed -i "s/INPUT_SIZE/`echo $is | jq -r '[.[1][] | tostring] | join(" ")'`/g" bench.sh &&
  cp input_strings/tccg/input_tccg_49_pattern.in input_strings/tccg/input_tccg_49.in
  sed -i "s/INPUT_DIMS/`echo $is | jq -r '[.[0][] | tostring] | join("-")'`/g" input_strings/tccg/input_tccg_49.in &&
  sed -i "s/INPUT_SIZE/`echo $is | jq -r '[.[1][] | tostring] | join(" ")'`/g" input_strings/tccg/input_tccg_49.in &&
  sed -i "s/DIMS_T3/`echo $is | jq -r '[.[0][0] | split("")[] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
  sed -i "s/DIMS_T2/`echo $is | jq -r '[.[0][1] | split("")[] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
  sed -i "s/DIMS_V2/`echo $is | jq -r '[.[0][2] | split("")[] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
  sed -i "s/DIMS_REDUCE/`echo $is | jq -r '[(def intersection(x;y):      ( (x|unique) + (y|unique) | sort) as $sorted      | reduce range(1; $sorted|length) as $i          ([]; if $sorted[$i] == $sorted[$i-1] then . + [$sorted[$i]] else . end) ; intersection(.[0][1]|split("");.[0][2]|split(""))-(.[0][0]|split("")))[] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
  sed -i "s/DIMS_SIZES_T3/`echo $is | jq -r '[([([.[0][] | split("")] | add | unique | sort), .[1]] | transpose | map( {(.[0]): .[1]}) | add) as $sizes | (.[0][0] | split(""))[] | ., $sizes[.] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
  sed -i "s/DIMS_SIZES_REDUCE/`echo $is | jq -r '[([([.[0][] | split("")] | add | unique | sort), .[1]] | transpose | map( {(.[0]): .[1]}) | add) as $sizes | (def intersection(x;y):      ( (x|unique) + (y|unique) | sort) as $sorted      | reduce range(1; $sorted|length) as $i          ([]; if $sorted[$i] == $sorted[$i-1] then . + [$sorted[$i]] else . end) ; intersection(.[0][1]|split("");.[0][2]|split(""))-(.[0][0]|split("")))[] | ., $sizes[.] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
  cp drivers/tccg-float/code_main_tccg_49_pattern.c drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/INPUT_DIMS/`echo $is | jq -r '[.[0][] | tostring] | join("-")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/DIMS_T2/`echo $is | jq -r '[.[0][1] | split("")[] | tostring] | join(",")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/DIMS_V2/`echo $is | jq -r '[.[0][2] | split("")[] | tostring] | join(",")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/DIMS_SIZES_T3/`echo $is | jq -r '[([([.[0][] | split("")] | add | unique | sort), .[1]] | transpose | map( {(.[0]): .[1]}) | add) as $sizes | (.[0][0] | split(""))[] | ., $sizes[.] | tostring] | join(",")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/DIMS_SIZES_REDUCE/`echo $is | jq -r '[([([.[0][] | split("")] | add | unique | sort), .[1]] | transpose | map( {(.[0]): .[1]}) | add) as $sizes | (def intersection(x;y):      ( (x|unique) + (y|unique) | sort) as $sorted      | reduce range(1; $sorted|length) as $i          ([]; if $sorted[$i] == $sorted[$i-1] then . + [$sorted[$i]] else . end) ; intersection(.[0][1]|split("");.[0][2]|split(""))-(.[0][0]|split("")))[] | ., $sizes[.] | tostring] | join(",")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/SIZE_VARS/`echo $is | jq -r '[([.[0][] | split("")] | add | unique | sort)[] | "size_idx_"+.+" = 16"] | join(", ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/NUM_INPUTS/`echo $is | jq -r '[.[0][] | split("")] | add | unique | sort | length'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/READ_ARGS/`echo $is | jq -r '[([.[0][] | split("")] | add | unique | sort)[] | "size_idx_"+.+" = atoi(argv[++i]);"] | join(" ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/REDUCE_VARS/`echo $is | jq -r '[(def intersection(x;y):      ( (x|unique) + (y|unique) | sort) as $sorted      | reduce range(1; $sorted|length) as $i          ([]; if $sorted[$i] == $sorted[$i-1] then . + [$sorted[$i]] else . end) ; intersection(.[0][1]|split("");.[0][2]|split(""))-(.[0][0]|split("")))[] | tostring | "size_idx_"+.] | join(" * ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/T3_VARS/`echo $is | jq -r '[.[0][0] | split("")[] | tostring | "size_idx_"+.] | join(" * ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/T2_VARS/`echo $is | jq -r '[.[0][1] | split("")[] | tostring | "size_idx_"+.] | join(" * ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/V2_VARS/`echo $is | jq -r '[.[0][2] | split("")[] | tostring | "size_idx_"+.] | join(" * ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  sed -i "s/ALL_VARS/`echo $is | jq -r '[([.[0][] | split("")] | add | unique | sort)[] | tostring | "size_idx_"+.] | join(", ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
  ./bench.sh &&
  cat cogent_fb_results.txt | head -n 1 | tail -n 1 | grep -oP "time\(ms\): ', '\K[0-9\.]+" > $ARTIFACT_ROOT/results/overall/gpu/COGENT/ccsdt_`echo $input_size | jq -r '[.[] | tostring] | join("x")'`_runtime || rm $ARTIFACT_ROOT/results/overall/gpu/COGENT/ccsdt_`echo $input_size | jq -r '[.[] | tostring] | join("x")'`_runtime
) |& tee "$ARTIFACT_ROOT/results/overall/gpu/COGENT/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`".log"