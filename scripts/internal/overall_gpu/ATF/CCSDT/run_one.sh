#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

platform_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."GPU OpenCL platform id"'` && \
device_id=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."GPU OpenCL device id"'` && \
input_size=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."CCSD(T)"."input size"'` && \
minutes=`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."overall"."tuning time (minutes)"'` && \
cd $BUILD_FOLDER/evaluation/overall/ATF && \
mkdir -p $ARTIFACT_ROOT/results/overall/gpu/$platform_id/$device_id/ATF && \
(FILES_SUFFIX="_$1"; ./overall_atf_ccsdt --mode tune --platform-id $platform_id --device-id $device_id --input-size `echo $input_size | jq -r '[.[] | tostring] | join(" ")'` --tuning-time `echo "scale=4; $minutes/60" | bc` && ./overall_atf_ccsdt --mode bench --platform-id $platform_id --device-id $device_id --input-size `echo $input_size | jq -r '[.[] | tostring] | join(" ")'`) |& tee "$ARTIFACT_ROOT/results/overall/gpu/$platform_id/$device_id/ATF/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log"
grep -aoP "generation time: \K[0-9]+" "$ARTIFACT_ROOT/results/overall/gpu/$platform_id/$device_id/ATF/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log" > "$ARTIFACT_ROOT/results/overall/gpu/$platform_id/$device_id/ATF/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1_generation_time"
grep -aoP "number of evaluated configs: \K[0-9]+" "$ARTIFACT_ROOT/results/overall/gpu/$platform_id/$device_id/ATF/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1.log" > "$ARTIFACT_ROOT/results/overall/gpu/$platform_id/$device_id/ATF/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1_num_valid"
printf "0" > "$ARTIFACT_ROOT/results/overall/gpu/$platform_id/$device_id/ATF/ccsdt_"`echo $input_size | jq -r '[.[] | tostring] | join("x")'`"_$1_num_invalid"