#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

$ARTIFACT_ROOT/scripts/internal/generation/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/storage/run_all.sh && \
$ARTIFACT_ROOT/scripts/internal/exploration/run_all.sh && \
if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."overall"."run experiments on"[] | test("^CPU$")) as $item (false; . or $item)'`" = "true" ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_cpu/run_all.sh
fi && \
if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."overall"."run experiments on"[] | test("^GPU$")) as $item (false; . or $item)'`" = "true" ]]; then
  $ARTIFACT_ROOT/scripts/internal/overall_gpu/run_all.sh
fi && \
if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."siamese"."run experiments on"[] | test("^CPU$")) as $item (false; . or $item)'`" = "true" ]]; then
  $ARTIFACT_ROOT/scripts/internal/siamese_cpu/run_all.sh
fi && \
if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."siamese"."run experiments on"[] | test("^GPU$")) as $item (false; . or $item)'`" = "true" ]]; then
  $ARTIFACT_ROOT/scripts/internal/siamese_gpu/run_all.sh
fi

echo ""
echo ""
echo ""
echo "Done executing experiments"
echo ""