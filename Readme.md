# Efficient Auto-Tuning of Parallel Programs with Interdependent Tuning Parameters Via Auto-Tuning Framework (ATF)

This artifact contains the workflow to reproduce the experiments shown in the paper *Efficient Auto-Tuning of Parallel Programs with Interdependent Tuning Parameters Via Auto-Tuning Framework (ATF)* under review at [ACM Transactions on Architecture and Code Optimization (TACO)](https://dl.acm.org/journal/taco). The user is invited to perform the steps described below.

All experiments in our paper haven been conducted on Intel Xeon Gold 6140 18-core CPU, tacted at 2.3GHz with 192 GB main memory and hyper-threading enabled, and an NVIDIA Tesla V100-SXM2-16GB GPU.

In case of **any problems**, please feel free to **open an issue** to get in touch with the authors.

## Software Requirements

- an OpenCL 1.2 driver and runtime environment
- CMake 3.8 or higher
- a compiler supporting C++14 or higher
- OpenTuner 0.8.0
- Java 8 SDK
- Intel MKL
- ATLAS
- NVIDIA cuBLAS
- NVIDIA cuDNN
- Tensor Comprehensions
- jq

Instructions on how to install the requirements are provided below.

## Workflow

The workflow of this artifact is divided into three main steps: **1) Installing the artifact**, **2) Running the experiments**, and **3) Plotting the results**. Note that running the experiments may take a long time (~1976h), because for each routine and input size both MDH and some of the references are auto-tuned for several hours. The user may edit the file `experiments.json` to decrease the overall auto-tuning time, e.g., by reducing the number of input sizes to auto-tune for or the tuning time per framework. Note that this may prevent the user from reproducing the results presented in the paper.

All experiments are compiled with the `-O3` flag. Additionally, for the Intel MKL experiments, we use the following flags, as recommended by the [Intel Math Kernel Library Link Line Advisor](https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor):

`-Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl`

### Step 1: Installing the artifact

Before compiling the artifact, the following dependencies have to be installed:

- **OpenCL 1.2 driver and runtime:**
  
  Download and install the OpenCL driver and runtime from the vendor website, e.g. [Intel](https://software.intel.com/content/www/us/en/develop/tools/opencl-sdk.html) or [NVIDIA](https://developer.nvidia.com/opencl), of the utilized hardware.
  
- **CMake 3.8 or higher**:

  Download CMake from the developer website:
  
  `$ wget https://cmake.org/files/v3.13/cmake-3.13.0-rc3-Linux-x86_64.sh`
  
  Install CMake locally. If you are asked to include the subdirectory in the installation path, type `y`:
  
  `$ /bin/bash cmake-3.13.0-rc3-Linux-x86_64.sh`
  
  Make CMake available:
  
  ``$ export PATH=`pwd`/cmake-3.13.0-rc3-Linux-x86_64/bin:$PATH``
  
- **A compiler supporting C++14 or higher**:
  
  `$ sudo apt-get install gcc g++`

- **OpenTuner 0.8.0**:

  OpenTuner has dependencies itself. The OpenTuner dependencies are installed by executing:
  
  `$ sudo apt-get install sqlite3 libsqlite3-dev`
  
  Afterwards, install OpenTuner by executing:
  
  `$ sudo pip install opentuner`
  
- **Java 8 SDK**:

  The Oracle Java SDK is installed as follows:
  
  ```
  $ sudo su
  $ echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | $ tee /etc/apt/sources.list.d/webupd8team-java.list
  $ echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
  $ apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
  $ apt-get update
  $ apt-get install oracle-java8-installer
  $ exit
  ```
  
  After installing Java, please restart your terminal session.
  
- **Intel MKL**:

  Intel MKL can be downloaded at [https://software.intel.com/en-us/mkl/choose-download/linux](https://software.intel.com/en-us/mkl/choose-download/linux). After registering, the Intel MKL library can be downloaded and installed using the provided installation scripts.


- **ATLAS**:

  ATLAS' source and detailed installation instructions can be downloaded at [http://math-atlas.sourceforge.net/](http://math-atlas.sourceforge.net/).


- **NVIDIA cuBLAS**:

  NVIDIA cuBLAS is bundled into NVIDIA CUDA Toolkit. The CUDA Toolkit can be downloaded at [https://developer.nvidia.com/cuda-downloads](https://developer.nvidia.com/cuda-downloads). Choose your preferred download type and follow the instructions to install NVIDIA CUDA Toolkit.

- **NVIDIA cuDNN**:

  NVIDIA cuDNN has to be manually installed into an existing NVIDIA CUDA Toolkit installation. The required steps can be found at [https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html).
  
- **Tensor Comprehensions**:

  Tensor Comprehensions can be installed as a conda package. Installation instructions for conda can be found at [https://conda.io/projects/conda/en/latest/user-guide/install/index.html](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). Afterwards, Tensor Comprehensions can be installed as follows:
  
  `$ conda install -y -c pytorch -c tensorcomp tensor_comprehensions`
  
- **jq**:

  `$ sudo apt-get install jq`
  
- **Compiling the artifact**:

  Clone the repository:

  `$ git clone https://gitlab.com/mdh-project/taco2020-atf.git`
  
  Change into the artifact directory:
  
  `$ cd taco2020-atf`
  
  Edit the file `environment.env` to configure the locations of the dependencies, and optionally disable references that you do not wish to evaluate. Note that in order to evaluate COGENT, the `CUDA_ARCH` variable in `environment.env` has to be set appropriately. Afterwards, execute:
  
  `$ source environment.env`

  Compile artifact's source code:

  `$ ./scripts/install.sh`

  All arguments provided to script install.sh will be directly forwarded to CMake when building the binaries. For example, forwarding arguments can be required if some dependencies cannot be found by CMake (e.g., when dependencies are not installed in their default locations).


### Step 2: Running the experiments

The experiments are run by executing:

`$ ./scripts/run.sh`

Note that this step may take a long time (~1976h), because for each routine and input size both MDH and some of the references are tuned for several hours. The user may edit the file `experiments.json` to decrease the overall auto-tuning time, e.g., by reducing the number of input sizes to auto-tune for or the tuning time per framework. Note that this may prevent the user from reproducing the results presented in the paper.

### Step 3: Plotting the results

The results are plotted by executing:
  
  `$ ./scripts/plot.sh`
  
The plots generated from this step can be found in the folder `plots` in the artifact root directory.