#!/usr/bin/env sh
set -e

TOOLS=$BUILD_FOLDER/evaluation/siamese/caffe_cpu/tools

$TOOLS/caffe train --solver=$ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/examples/siamese/mnist_siamese_solver.prototxt $@
