#!/usr/bin/env sh
# This script converts the mnist data into leveldb format.
set -e

EXAMPLES=$BUILD_FOLDER/evaluation/siamese/caffe_cpu/examples/siamese
DATA=$ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/data/mnist

echo "Creating leveldb..."

rm -rf $ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/examples/siamese/mnist_siamese_train_leveldb
rm -rf $ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/examples/siamese/mnist_siamese_test_leveldb

$EXAMPLES/convert_mnist_siamese_data.bin \
    $DATA/train-images-idx3-ubyte \
    $DATA/train-labels-idx1-ubyte \
    $ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/examples/siamese/mnist_siamese_train_leveldb
$EXAMPLES/convert_mnist_siamese_data.bin \
    $DATA/t10k-images-idx3-ubyte \
    $DATA/t10k-labels-idx1-ubyte \
    $ARTIFACT_ROOT/evaluation/siamese/caffe_cpu/examples/siamese/mnist_siamese_test_leveldb

echo "Done."
