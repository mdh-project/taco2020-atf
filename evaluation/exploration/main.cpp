#include <cstdio>

#include <atf.h>
#ifdef PRL
#include <unordered_map>
#endif

int main(int argc, char **argv) {
    int platform_id = std::stoi(argv[1]);
    int device_id = std::stoi(argv[2]);
    int abort_after_minutes = std::stoi(argv[3]);
    int run_num = std::stoi(argv[4]);
#if defined(GAUSSIAN)
    int input_size_1 = -1;
    int input_size_2 = -1;
    if (argc >= 3) {
        try {
            input_size_1 = std::stoi(argv[5]);
            input_size_2 = std::stoi(argv[6]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2);
#elif defined(GEMM)
    int input_size_1 = -1;
    int input_size_2 = -1;
    int input_size_3 = -1;
    if (argc >= 4) {
        try {
            input_size_1 = std::stoi(argv[5]);
            input_size_2 = std::stoi(argv[6]);
            input_size_3 = std::stoi(argv[7]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
            input_size_3 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0 || input_size_3 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2 input_size_3" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2) + "x" + std::to_string(input_size_3);
#elif defined(TENSOR_CONTRACTION)
    int input_size_1 = -1;
    int input_size_2 = -1;
    int input_size_3 = -1;
    int input_size_4 = -1;
    int input_size_5 = -1;
    int input_size_6 = -1;
    int input_size_7 = -1;
    if (argc >= 8) {
        try {
            input_size_1 = std::stoi(argv[5]);
            input_size_2 = std::stoi(argv[6]);
            input_size_3 = std::stoi(argv[7]);
            input_size_4 = std::stoi(argv[8]);
            input_size_5 = std::stoi(argv[9]);
            input_size_6 = std::stoi(argv[10]);
            input_size_7 = std::stoi(argv[11]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
            input_size_3 = -1;
            input_size_4 = -1;
            input_size_5 = -1;
            input_size_6 = -1;
            input_size_7 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0 || input_size_3 < 0 || input_size_4 < 0 || input_size_5 < 0 || input_size_6 < 0 || input_size_7 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2 input_size_3 input_size_4 input_size_5 input_size_6 input_size_7" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2) + "x" + std::to_string(input_size_3) + "x" + std::to_string(input_size_4) + "x" + std::to_string(input_size_5) + "x" + std::to_string(input_size_6) + "x" + std::to_string(input_size_7);
#elif defined(PRL)
    int input_size_1 = -1;
    int input_size_2 = -1;
    if (argc >= 3) {
        try {
            input_size_1 = std::stoi(argv[5]);
            input_size_2 = std::stoi(argv[6]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2);
#endif

    // get device
    atf::cf::device_info device_info(platform_id, device_id);
    device_info.initialize(false);

    // read device specific boundaries for work items
    size_t max_wg_size;
    device_info.device().getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, &max_wg_size);

    // create tuning parameter
#define ATF_RANGE(max) atf::interval<int>(1, max)
#if defined(GAUSSIAN)
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto L_CB_RES_DEST_LEVEL) { return L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL; });
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL; });

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0, 1}, [&](auto OCL_DIM_L_2) { return OCL_DIM_L_2 != OCL_DIM_L_1; });

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {input_size_1 - 4});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       ATF_RANGE(input_size_1 - 4), [&](auto L_CB_SIZE_L_1) { return INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0; });
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       ATF_RANGE(input_size_1 - 4), [&](auto P_CB_SIZE_L_1) { return L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0; });
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          ATF_RANGE(input_size_1 - 4), [&](auto NUM_WG_L_1) { return (INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0; });
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          ATF_RANGE(input_size_1 - 4), [&](auto NUM_WI_L_1) { return ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) && (NUM_WI_L_1 <= ((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1)); });

    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {input_size_2 - 4});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       ATF_RANGE(input_size_2 - 4), [&](auto L_CB_SIZE_L_2) { return INPUT_SIZE_L_2 % L_CB_SIZE_L_2 == 0; });
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       ATF_RANGE(input_size_2 - 4), [&](auto P_CB_SIZE_L_2) { return L_CB_SIZE_L_2 % P_CB_SIZE_L_2 == 0; });
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          ATF_RANGE(input_size_2 - 4), [&](auto NUM_WG_L_2) { return (INPUT_SIZE_L_2 / L_CB_SIZE_L_2) % NUM_WG_L_2 == 0; });
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          ATF_RANGE(input_size_2 - 4), [&](auto NUM_WI_L_2) { return ((L_CB_SIZE_L_2 / P_CB_SIZE_L_2) % NUM_WI_L_2 == 0) && (NUM_WI_L_2 <= ((INPUT_SIZE_L_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2)); });

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {2});

    auto is_valid = [&] (auto &configuration) { return configuration[NUM_WI_L_1.name()].value().int_val() * configuration[NUM_WI_L_2.name()].value().int_val() <= max_wg_size; };

    // prepare kernel for tuning
    std::vector<float> in(input_size_1 * input_size_2); for (int i = 0; i < in.size(); ++i) in[i] = (i % 100) + 1;
    std::vector<float> out((input_size_1 - 4) * (input_size_2 - 4)); for (int i = 0; i < out.size(); ++i) out[i] = 0;
    size_t res_g_l_size = (input_size_1 - 4) * (input_size_2 - 4) * sizeof(float);
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                return size;
            };
    std::vector<float> int_res((input_size_1 - 4) * (input_size_2 - 4)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return false;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::LOCAL,
            "../../extern/ATF/ocl_md_hom_process_wrapper_float",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "../../../evaluation/kernels/gaussian_static_1.cl", "gaussian_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(in)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "../../../evaluation/kernels/gaussian_static_2.cl", "gaussian_2"},
            atf::inputs(atf::buffer(out)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );
#elif defined(GEMM)
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto L_CB_RES_DEST_LEVEL) { return L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL; });
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL; });

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1, 2});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0, 1, 2}, [&](auto OCL_DIM_L_2) { return OCL_DIM_L_2 != OCL_DIM_L_1; });
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1, 2}, [&](auto OCL_DIM_R_1) { return (OCL_DIM_R_1 != OCL_DIM_L_1) && (OCL_DIM_R_1 != OCL_DIM_L_2); });

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {input_size_1});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto L_CB_SIZE_L_1) { return INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0; });
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto P_CB_SIZE_L_1) { return L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0; });
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WG_L_1) { return (INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0; });
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WI_L_1) { return ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) && (NUM_WI_L_1 <= ((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1)); });

    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {input_size_2});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto L_CB_SIZE_L_2) { return INPUT_SIZE_L_2 % L_CB_SIZE_L_2 == 0; });
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto P_CB_SIZE_L_2) { return L_CB_SIZE_L_2 % P_CB_SIZE_L_2 == 0; });
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WG_L_2) { return (INPUT_SIZE_L_2 / L_CB_SIZE_L_2) % NUM_WG_L_2 == 0; });
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WI_L_2) { return ((L_CB_SIZE_L_2 / P_CB_SIZE_L_2) % NUM_WI_L_2 == 0) && (NUM_WI_L_2 <= ((INPUT_SIZE_L_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2)); });

    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {input_size_3});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       ATF_RANGE(input_size_3), [&](auto L_CB_SIZE_R_1) { return INPUT_SIZE_R_1 % L_CB_SIZE_R_1 == 0; });
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       ATF_RANGE(input_size_3), [&](auto P_CB_SIZE_R_1) { return L_CB_SIZE_R_1 % P_CB_SIZE_R_1 == 0; });
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          ATF_RANGE(input_size_3), [&](auto NUM_WG_R_1) { return (INPUT_SIZE_R_1 / L_CB_SIZE_R_1) % NUM_WG_R_1 == 0; });
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          ATF_RANGE(input_size_3), [&](auto NUM_WI_R_1) { return ((L_CB_SIZE_R_1 / P_CB_SIZE_R_1) % NUM_WI_R_1 == 0) && (NUM_WI_R_1 <= ((INPUT_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1)) && (NUM_WG_R_1 == 1 || ((NUM_WG_R_1 % L_CB_SIZE_R_1 == 0))); });

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {2});

    auto is_valid = [&] (auto &configuration) { return configuration[NUM_WI_L_1.name()].value().int_val() * configuration[NUM_WI_L_2.name()].value().int_val() * configuration[NUM_WI_R_1.name()].value().int_val() <= max_wg_size; };

    // prepare kernel for tuning
    std::vector<float> a(input_size_1 * input_size_3); for (int i = 0; i < a.size(); ++i) a[i] = (i % 100) + 1;
    std::vector<float> b(input_size_3 * input_size_2); for (int i = 0; i < b.size(); ++i) b[i] = (i % 100) + 1;
    std::vector<float> c(input_size_1 * input_size_2); for (int i = 0; i < c.size(); ++i) c[i] = 0;
    size_t res_g_l_size = input_size_1 * input_size_2 * sizeof(float);
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 2) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (NUM_WG_R_1) * (NUM_WI_R_1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 2) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (NUM_WI_R_1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0)                * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1)                * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 2) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2)                * (NUM_WI_R_1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 2) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (NUM_WI_R_1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::LOCAL,
            "../../extern/ATF/ocl_md_hom_process_wrapper_float",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "../../../evaluation/kernels/gemm_1.cl", "gemm_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(a), atf::buffer(b)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "../../../evaluation/kernels/gemm_2.cl", "gemm_2"},
            atf::inputs(atf::buffer(c)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );
#elif defined(TENSOR_CONTRACTION)
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto L_CB_RES_DEST_LEVEL) { return L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL; });
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL; });

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1, 2, 3, 4, 5, 6});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_2) { return (OCL_DIM_L_2 != OCL_DIM_L_1); });
    auto OCL_DIM_L_3         = atf::tp("OCL_DIM_L_3",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_3) { return (OCL_DIM_L_3 != OCL_DIM_L_1) && (OCL_DIM_L_3 != OCL_DIM_L_2); });
    auto OCL_DIM_L_4         = atf::tp("OCL_DIM_L_4",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_4) { return (OCL_DIM_L_4 != OCL_DIM_L_1) && (OCL_DIM_L_4 != OCL_DIM_L_2) && (OCL_DIM_L_4 != OCL_DIM_L_3); });
    auto OCL_DIM_L_5         = atf::tp("OCL_DIM_L_5",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_5) { return (OCL_DIM_L_5 != OCL_DIM_L_1) && (OCL_DIM_L_5 != OCL_DIM_L_2) && (OCL_DIM_L_5 != OCL_DIM_L_3) && (OCL_DIM_L_5 != OCL_DIM_L_4); });
    auto OCL_DIM_L_6         = atf::tp("OCL_DIM_L_6",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_6) { return (OCL_DIM_L_6 != OCL_DIM_L_1) && (OCL_DIM_L_6 != OCL_DIM_L_2) && (OCL_DIM_L_6 != OCL_DIM_L_3) && (OCL_DIM_L_6 != OCL_DIM_L_4) && (OCL_DIM_L_6 != OCL_DIM_L_5); });
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_R_1) { return (OCL_DIM_R_1 != OCL_DIM_L_1) && (OCL_DIM_R_1 != OCL_DIM_L_2) && (OCL_DIM_R_1 != OCL_DIM_L_3) && (OCL_DIM_R_1 != OCL_DIM_L_4) && (OCL_DIM_R_1 != OCL_DIM_L_5) && (OCL_DIM_R_1 != OCL_DIM_L_6); });

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {input_size_1});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto L_CB_SIZE_L_1) { return INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0; });
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto P_CB_SIZE_L_1) { return L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0; });
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WG_L_1) { return (INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0; });
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WI_L_1) { return ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) && (NUM_WI_L_1 <= ((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1)); });

    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {input_size_2});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto L_CB_SIZE_L_2) { return INPUT_SIZE_L_2 % L_CB_SIZE_L_2 == 0; });
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto P_CB_SIZE_L_2) { return L_CB_SIZE_L_2 % P_CB_SIZE_L_2 == 0; });
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WG_L_2) { return (INPUT_SIZE_L_2 / L_CB_SIZE_L_2) % NUM_WG_L_2 == 0; });
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WI_L_2) { return ((L_CB_SIZE_L_2 / P_CB_SIZE_L_2) % NUM_WI_L_2 == 0) && (NUM_WI_L_2 <= ((INPUT_SIZE_L_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2)); });

    auto INPUT_SIZE_L_3      = atf::tp("INPUT_SIZE_L_3",      {input_size_3});
    auto L_CB_SIZE_L_3       = atf::tp("L_CB_SIZE_L_3",       ATF_RANGE(input_size_3), [&](auto L_CB_SIZE_L_3) { return INPUT_SIZE_L_3 % L_CB_SIZE_L_3 == 0; });
    auto P_CB_SIZE_L_3       = atf::tp("P_CB_SIZE_L_3",       ATF_RANGE(input_size_3), [&](auto P_CB_SIZE_L_3) { return L_CB_SIZE_L_3 % P_CB_SIZE_L_3 == 0; });
    auto NUM_WG_L_3          = atf::tp("NUM_WG_L_3",          ATF_RANGE(input_size_3), [&](auto NUM_WG_L_3) { return (INPUT_SIZE_L_3 / L_CB_SIZE_L_3) % NUM_WG_L_3 == 0; });
    auto NUM_WI_L_3          = atf::tp("NUM_WI_L_3",          ATF_RANGE(input_size_3), [&](auto NUM_WI_L_3) { return ((L_CB_SIZE_L_3 / P_CB_SIZE_L_3) % NUM_WI_L_3 == 0) && (NUM_WI_L_3 <= ((INPUT_SIZE_L_3 + NUM_WG_L_3 - 1) / NUM_WG_L_3)); });

    auto INPUT_SIZE_L_4      = atf::tp("INPUT_SIZE_L_4",      {input_size_4});
    auto L_CB_SIZE_L_4       = atf::tp("L_CB_SIZE_L_4",       ATF_RANGE(input_size_4), [&](auto L_CB_SIZE_L_4) { return INPUT_SIZE_L_4 % L_CB_SIZE_L_4 == 0; });
    auto P_CB_SIZE_L_4       = atf::tp("P_CB_SIZE_L_4",       ATF_RANGE(input_size_4), [&](auto P_CB_SIZE_L_4) { return L_CB_SIZE_L_4 % P_CB_SIZE_L_4 == 0; });
    auto NUM_WG_L_4          = atf::tp("NUM_WG_L_4",          ATF_RANGE(input_size_4), [&](auto NUM_WG_L_4) { return (INPUT_SIZE_L_4 / L_CB_SIZE_L_4) % NUM_WG_L_4 == 0; });
    auto NUM_WI_L_4          = atf::tp("NUM_WI_L_4",          ATF_RANGE(input_size_4), [&](auto NUM_WI_L_4) { return ((L_CB_SIZE_L_4 / P_CB_SIZE_L_4) % NUM_WI_L_4 == 0) && (NUM_WI_L_4 <= ((INPUT_SIZE_L_4 + NUM_WG_L_4 - 1) / NUM_WG_L_4)); });

    auto INPUT_SIZE_L_5      = atf::tp("INPUT_SIZE_L_5",      {input_size_5});
    auto L_CB_SIZE_L_5       = atf::tp("L_CB_SIZE_L_5",       ATF_RANGE(input_size_5), [&](auto L_CB_SIZE_L_5) { return INPUT_SIZE_L_5 % L_CB_SIZE_L_5 == 0; });
    auto P_CB_SIZE_L_5       = atf::tp("P_CB_SIZE_L_5",       ATF_RANGE(input_size_5), [&](auto P_CB_SIZE_L_5) { return L_CB_SIZE_L_5 % P_CB_SIZE_L_5 == 0; });
    auto NUM_WG_L_5          = atf::tp("NUM_WG_L_5",          ATF_RANGE(input_size_5), [&](auto NUM_WG_L_5) { return (INPUT_SIZE_L_5 / L_CB_SIZE_L_5) % NUM_WG_L_5 == 0; });
    auto NUM_WI_L_5          = atf::tp("NUM_WI_L_5",          ATF_RANGE(input_size_5), [&](auto NUM_WI_L_5) { return ((L_CB_SIZE_L_5 / P_CB_SIZE_L_5) % NUM_WI_L_5 == 0) && (NUM_WI_L_5 <= ((INPUT_SIZE_L_5 + NUM_WG_L_5 - 1) / NUM_WG_L_5)); });

    auto INPUT_SIZE_L_6      = atf::tp("INPUT_SIZE_L_6",      {input_size_6});
    auto L_CB_SIZE_L_6       = atf::tp("L_CB_SIZE_L_6",       ATF_RANGE(input_size_6), [&](auto L_CB_SIZE_L_6) { return INPUT_SIZE_L_6 % L_CB_SIZE_L_6 == 0; });
    auto P_CB_SIZE_L_6       = atf::tp("P_CB_SIZE_L_6",       ATF_RANGE(input_size_6), [&](auto P_CB_SIZE_L_6) { return L_CB_SIZE_L_6 % P_CB_SIZE_L_6 == 0; });
    auto NUM_WG_L_6          = atf::tp("NUM_WG_L_6",          ATF_RANGE(input_size_6), [&](auto NUM_WG_L_6) { return (INPUT_SIZE_L_6 / L_CB_SIZE_L_6) % NUM_WG_L_6 == 0; });
    auto NUM_WI_L_6          = atf::tp("NUM_WI_L_6",          ATF_RANGE(input_size_6), [&](auto NUM_WI_L_6) { return ((L_CB_SIZE_L_6 / P_CB_SIZE_L_6) % NUM_WI_L_6 == 0) && (NUM_WI_L_6 <= ((INPUT_SIZE_L_6 + NUM_WG_L_6 - 1) / NUM_WG_L_6)); });

    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {input_size_7});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       ATF_RANGE(input_size_7), [&](auto L_CB_SIZE_R_1) { return INPUT_SIZE_R_1 % L_CB_SIZE_R_1 == 0; });
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       ATF_RANGE(input_size_7), [&](auto P_CB_SIZE_R_1) { return L_CB_SIZE_R_1 % P_CB_SIZE_R_1 == 0; });
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          ATF_RANGE(input_size_7), [&](auto NUM_WG_R_1) { return (INPUT_SIZE_R_1 / L_CB_SIZE_R_1) % NUM_WG_R_1 == 0; });
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          ATF_RANGE(input_size_7), [&](auto NUM_WI_R_1) { return ((L_CB_SIZE_R_1 / P_CB_SIZE_R_1) % NUM_WI_R_1 == 0) && (NUM_WI_R_1 <= ((INPUT_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1)) && (NUM_WG_R_1 == 1 || ((NUM_WG_R_1 % L_CB_SIZE_R_1 == 0))); });

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {6});

    auto is_valid = [&] (auto &configuration) { return configuration[NUM_WI_L_1.name()].value().int_val() * configuration[NUM_WI_L_2.name()].value().int_val() * configuration[NUM_WI_L_3.name()].value().int_val() * configuration[NUM_WI_L_4.name()].value().int_val() * configuration[NUM_WI_L_5.name()].value().int_val() * configuration[NUM_WI_L_6.name()].value().int_val() * configuration[NUM_WI_R_1.name()].value().int_val() <= max_wg_size; };

    std::vector<float> a(input_size_7 * input_size_5 * input_size_2 * input_size_3); for (int i = 0; i < a.size(); ++i) a[i] = (i % 100) + 1;
    std::vector<float> b(input_size_4 * input_size_6 * input_size_7 * input_size_1); for (int i = 0; i < b.size(); ++i) b[i] = (i % 100) + 1;
    std::vector<float> c(input_size_1 * input_size_2 * input_size_2 * input_size_4 * input_size_5 * input_size_6); for (int i = 0; i < c.size(); ++i) c[i] = 0;
    size_t res_g_l_size = input_size_1 * input_size_2 * input_size_3 * input_size_4 * input_size_5 * input_size_6 * sizeof(float);
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WG_L_5) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WG_L_6) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WG_R_1) * (NUM_WI_R_1), 1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) *                (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) *                (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WG_L_5) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WG_L_6) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) *                (NUM_WI_R_1), 1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::LOCAL,
            "../../extern/ATF/ocl_md_hom_process_wrapper_float",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "../../../evaluation/kernels/tc_abcdef_gebc_dfga_1.cl", "tc_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(a), atf::buffer(b)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "../../../evaluation/kernels/tc_abcdef_gebc_dfga_2.cl", "tc_2"},
            atf::inputs(atf::buffer(c)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );
#elif defined(PRL)
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto L_CB_RES_DEST_LEVEL) { return L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL; });
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL; });

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1}, [&](auto OCL_DIM_R_1) { return OCL_DIM_R_1 != OCL_DIM_L_1; });

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {input_size_1});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto L_CB_SIZE_L_1) { return INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0; });
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto P_CB_SIZE_L_1) { return L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0; });
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WG_L_1) { return (INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0; });
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WI_L_1) { return ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) && (NUM_WI_L_1 <= ((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1)); });

    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {input_size_2});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       ATF_RANGE(input_size_2), [&](auto L_CB_SIZE_R_1) { return INPUT_SIZE_R_1 % L_CB_SIZE_R_1 == 0; });
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       ATF_RANGE(input_size_2), [&](auto P_CB_SIZE_R_1) { return L_CB_SIZE_R_1 % P_CB_SIZE_R_1 == 0; });
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          ATF_RANGE(input_size_2), [&](auto NUM_WG_R_1) { return (INPUT_SIZE_R_1 / L_CB_SIZE_R_1) % NUM_WG_R_1 == 0; });
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          ATF_RANGE(input_size_2), [&](auto NUM_WI_R_1) { return ((L_CB_SIZE_R_1 / P_CB_SIZE_R_1) % NUM_WI_R_1 == 0) && (NUM_WI_R_1 <= ((INPUT_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1)) && (NUM_WG_R_1 == 1 || ((NUM_WG_R_1 % L_CB_SIZE_R_1 == 0))); });

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {1});

    auto is_valid = [&] (auto &configuration) { return configuration[NUM_WI_L_1.name()].value().int_val() * configuration[NUM_WI_R_1.name()].value().int_val() <= max_wg_size; };

    // prepare inputs
    const dbl8 probM =
            {{
                     0.99439781, // lastname
                     0.98247962, // firstname
                     0.98772576, // birthname
                     0.99949391, // birthday
                     0.9995371,  // birthmonth
                     0.99971322, // birthyear
                     0.99999304, // gender
                     0.9915867   // cid
             }};

    std::vector<long>      n_id(input_size_1);
    std::vector<str46>     n_lastname1(input_size_1);
    std::vector<str46>     n_lastname2(input_size_1);
    std::vector<str46>     n_lastname3(input_size_1);
    std::vector<str46>     n_firstname1(input_size_1);
    std::vector<str46>     n_firstname2(input_size_1);
    std::vector<str46>     n_firstname3(input_size_1);
    std::vector<long>      n_firstname_group1(input_size_1);
    std::vector<long>      n_firstname_group2(input_size_1);
    std::vector<long>      n_firstname_group3(input_size_1);
    std::vector<str46>     n_birthname1(input_size_1);
    std::vector<str46>     n_birthname2(input_size_1);
    std::vector<str46>     n_birthname3(input_size_1);
    std::vector<str46>     n_birthday(input_size_1);
    std::vector<chr2>      n_gender(input_size_1);
    std::vector<int>       n_birthmonth(input_size_1);
    std::vector<int>       n_birthyear(input_size_1);
    std::vector<int>       n_cin(input_size_1);
    std::vector<double>    n_prob_lastname1(input_size_1);
    std::vector<double>    n_prob_lastname2(input_size_1);
    std::vector<double>    n_prob_lastname3(input_size_1);
    std::vector<double>    n_prob_firstname1(input_size_1);
    std::vector<double>    n_prob_firstname2(input_size_1);
    std::vector<double>    n_prob_firstname3(input_size_1);
    std::vector<double>    n_prob_birthname1(input_size_1);
    std::vector<double>    n_prob_birthname2(input_size_1);
    std::vector<double>    n_prob_birthname3(input_size_1);
    std::vector<double>    n_prob_birthday(input_size_1);
    std::vector<double>    n_prob_gender(input_size_1);
    std::vector<double>    n_prob_birthmonth(input_size_1);
    std::vector<double>    n_prob_birthyear(input_size_1);
    std::vector<double>    n_prob_cin(input_size_1);

    std::string n_file_name = std::getenv("ARTIFACT_ROOT");
    n_file_name.append("/evaluation/data/n_" + std::to_string(input_size_1) + ".csv");
    std::cout << "loading new reports from " << n_file_name << std::endl;
    std::unordered_map<std::string, long> firstname_groups;
    std::ifstream n_csv_file(n_file_name);
    std::string line;
    int li = 0;
    while (std::getline(n_csv_file, line)) {
        std::istringstream s(line);
        std::string field;
        int fi = 0;
        while (std::getline(s, field, ';')) {
            std::stringstream ss(field);
            switch (fi) {
                case 0:
                    if (field.empty() || !(ss >> n_id[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    if (!field.empty() && !memcpy(n_lastname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2:
                    if (!field.empty() && !(ss >> n_prob_lastname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 3:
                    if (!field.empty() && !memcpy(n_lastname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 4:
                    if (!field.empty() && !(ss >> n_prob_lastname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 5:
                    if (!field.empty() && !memcpy(n_lastname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 6:
                    if (!field.empty() && !(ss >> n_prob_lastname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 7:
                    if (!field.empty() && !memcpy(n_firstname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 8:
                    if (!field.empty() && !(ss >> n_prob_firstname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 9:
                    if (!field.empty() && !memcpy(n_firstname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 10:
                    if (!field.empty() && !(ss >> n_prob_firstname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 11:
                    if (!field.empty() && !memcpy(n_firstname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 12:
                    if (!field.empty() && !(ss >> n_prob_firstname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 13:
                    if (!field.empty() && !memcpy(n_birthname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 14:
                    if (!field.empty() && !(ss >> n_prob_birthname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 15:
                    if (!field.empty() && !memcpy(n_birthname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 16:
                    if (!field.empty() && !(ss >> n_prob_birthname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 17:
                    if (!field.empty() && !memcpy(n_birthname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 18:
                    if (!field.empty() && !(ss >> n_prob_birthname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 19:
                case 20:
                    // pcfields are not used
                    break;
                case 21:
                    if (!field.empty() && !memcpy(n_birthday[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 22:
                    if (!field.empty() && !(ss >> n_prob_birthday[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 23:
                    if (!field.empty() && !(ss >> n_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 24:
                    if (!field.empty() && !(ss >> n_prob_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 25:
                    if (!field.empty() && !(ss >> n_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 26:
                    if (!field.empty() && !(ss >> n_prob_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 27:
                    if (!field.empty() && !memcpy(n_gender[li].values, field.c_str(), 2)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 28:
                    if (!field.empty() && !(ss >> n_prob_gender[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 29:
                    if (!field.empty() && !(ss >> n_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 30:
                    if (!field.empty() && !(ss >> n_prob_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    std::cerr << "unexpected values" << std::endl;
                    exit(EXIT_FAILURE);
            }
            ++fi;
        }
        if (n_prob_firstname1[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname1[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname1[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group1[li] = firstname_groups[std::string(n_firstname1[li].str, 46)];
        }
        if (n_prob_firstname2[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname2[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname2[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group2[li] = firstname_groups[std::string(n_firstname2[li].str, 46)];
        }
        if (n_prob_firstname3[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname3[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname3[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group3[li] = firstname_groups[std::string(n_firstname3[li].str, 46)];
        }
        n_prob_birthyear[li] = 1.0E-7;
        n_prob_gender[li] = 1.0E-7;
        n_prob_cin[li] = 1.0E-7;
        ++li;
    }
    assert(li == input_size_1);
    n_csv_file.close();

    std::vector<long>      i_id(input_size_2);
    std::vector<str46>     i_lastname1(input_size_2);
    std::vector<str46>     i_lastname2(input_size_2);
    std::vector<str46>     i_lastname3(input_size_2);
    std::vector<str46>     i_firstname1(input_size_2);
    std::vector<str46>     i_firstname2(input_size_2);
    std::vector<str46>     i_firstname3(input_size_2);
    std::vector<long>      i_firstname_group1(input_size_2);
    std::vector<long>      i_firstname_group2(input_size_2);
    std::vector<long>      i_firstname_group3(input_size_2);
    std::vector<str46>     i_birthname1(input_size_2);
    std::vector<str46>     i_birthname2(input_size_2);
    std::vector<str46>     i_birthname3(input_size_2);
    std::vector<str46>     i_birthday(input_size_2);
    std::vector<chr2>      i_gender(input_size_2);
    std::vector<int>       i_birthmonth(input_size_2);
    std::vector<int>       i_birthyear(input_size_2);
    std::vector<int>       i_cin(input_size_2);
    std::vector<double>    i_prob_lastname1(input_size_2);
    std::vector<double>    i_prob_lastname2(input_size_2);
    std::vector<double>    i_prob_lastname3(input_size_2);
    std::vector<double>    i_prob_firstname1(input_size_2);
    std::vector<double>    i_prob_firstname2(input_size_2);
    std::vector<double>    i_prob_firstname3(input_size_2);
    std::vector<double>    i_prob_birthname1(input_size_2);
    std::vector<double>    i_prob_birthname2(input_size_2);
    std::vector<double>    i_prob_birthname3(input_size_2);
    std::vector<double>    i_prob_birthday(input_size_2);
    std::vector<double>    i_prob_gender(input_size_2);
    std::vector<double>    i_prob_birthmonth(input_size_2);
    std::vector<double>    i_prob_birthyear(input_size_2);
    std::vector<double>    i_prob_cin(input_size_2);

    std::string i_file_name = std::getenv("ARTIFACT_ROOT");
    i_file_name.append("/evaluation/data/i_" + std::to_string(input_size_2) + ".csv");
    std::cout << "loading inventory reports from " << i_file_name << std::endl;
    std::ifstream i_csv_file(i_file_name);
    li = 0;
    while (std::getline(i_csv_file, line)) {
        std::istringstream s(line);
        std::string field;
        int fi = 0;
        while (std::getline(s, field, ';')) {
            std::stringstream ss(field);
            switch (fi) {
                case 0:
                    if (field.empty() || !(ss >> i_id[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    if (!field.empty() && !memcpy(i_lastname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2:
                    if (!field.empty() && !(ss >> i_prob_lastname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 3:
                    if (!field.empty() && !memcpy(i_lastname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 4:
                    if (!field.empty() && !(ss >> i_prob_lastname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 5:
                    if (!field.empty() && !memcpy(i_lastname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 6:
                    if (!field.empty() && !(ss >> i_prob_lastname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 7:
                    if (!field.empty() && !memcpy(i_firstname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 8:
                    if (!field.empty() && !(ss >> i_prob_firstname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 9:
                    if (!field.empty() && !memcpy(i_firstname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 10:
                    if (!field.empty() && !(ss >> i_prob_firstname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 11:
                    if (!field.empty() && !memcpy(i_firstname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 12:
                    if (!field.empty() && !(ss >> i_prob_firstname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 13:
                    if (!field.empty() && !memcpy(i_birthname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 14:
                    if (!field.empty() && !(ss >> i_prob_birthname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 15:
                    if (!field.empty() && !memcpy(i_birthname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 16:
                    if (!field.empty() && !(ss >> i_prob_birthname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 17:
                    if (!field.empty() && !memcpy(i_birthname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 18:
                    if (!field.empty() && !(ss >> i_prob_birthname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 19:
                case 20:
                    // pcfields are not used
                    break;
                case 21:
                    if (!field.empty() && !memcpy(i_birthday[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 22:
                    if (!field.empty() && !(ss >> i_prob_birthday[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 23:
                    if (!field.empty() && !(ss >> i_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 24:
                    if (!field.empty() && !(ss >> i_prob_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 25:
                    if (!field.empty() && !(ss >> i_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 26:
                    if (!field.empty() && !(ss >> i_prob_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 27:
                    if (!field.empty() && !memcpy(i_gender[li].values, field.c_str(), 2)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 28:
                    if (!field.empty() && !(ss >> i_prob_gender[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 29:
                    if (!field.empty() && !(ss >> i_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 30:
                    if (!field.empty() && !(ss >> i_prob_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    std::cerr << "unexpected values" << std::endl;
                    exit(EXIT_FAILURE);
            }
            ++fi;
        }
        if (i_prob_firstname1[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname1[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname1[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group1[li] = firstname_groups[std::string(i_firstname1[li].str, 46)];
        }
        if (i_prob_firstname2[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname2[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname2[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group2[li] = firstname_groups[std::string(i_firstname2[li].str, 46)];
        }
        if (i_prob_firstname3[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname3[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname3[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group3[li] = firstname_groups[std::string(i_firstname3[li].str, 46)];
        }
        i_prob_birthyear[li] = 1.0E-7;
        i_prob_gender[li] = 1.0E-7;
        i_prob_cin[li] = 1.0E-7;
        ++li;
    }
    assert(li == input_size_2);
    i_csv_file.close();

    std::vector<char> int_res(input_size_1);
    std::vector<long> res_match_id(input_size_1);
    std::vector<double> res_match_weight(input_size_1);
    std::vector<int> res_id_measure(input_size_1);
    auto res_g_size = [&](unsigned int kernel, atf::configuration &config) -> size_t {
        size_t size = input_size_1 * sizeof(char);
        if (kernel == 1) {
            if (config[G_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WG_R_1.name()].value().size_t_val();
            }
            if (config[L_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WI_R_1.name()].value().size_t_val();
            }
        } else {
            if (config[L_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WI_R_1.name()].value().size_t_val();
            }
        }
        if (config[P_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
            size *= 1;
        }
        return size;
    };
    auto int_res_size = [&](atf::configuration &config) -> size_t {
        size_t size = input_size_1 * config[NUM_WG_R_1.name()].value().size_t_val() * sizeof(char);
        return size;
    };
    auto needs_second_kernel = [&](atf::configuration &config) -> bool {
        return config[NUM_WG_R_1.name()].value().size_t_val() > 1;
    };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0)                * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1)                * (NUM_WI_R_1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::LOCAL,
            "../../extern/ATF_RL/ocl_md_hom_process_wrapper_float_rl [flags]",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "../../../evaluation/kernels/rl_1.cl", "rl_1", ""},
            atf::inputs(atf::scalar<dbl8>(probM),
                        atf::buffer(n_id),
                        atf::buffer(n_lastname1),
                        atf::buffer(n_lastname2),
                        atf::buffer(n_lastname3),
                        atf::buffer(n_firstname1),
                        atf::buffer(n_firstname2),
                        atf::buffer(n_firstname3),
                        atf::buffer(n_firstname_group1),
                        atf::buffer(n_firstname_group2),
                        atf::buffer(n_firstname_group3),
                        atf::buffer(n_birthname1),
                        atf::buffer(n_birthname2),
                        atf::buffer(n_birthname3),
                        atf::buffer(n_birthday),
                        atf::buffer(n_gender),
                        atf::buffer(n_birthmonth),
                        atf::buffer(n_birthyear),
                        atf::buffer(n_cin),
                        atf::buffer(n_prob_lastname1),
                        atf::buffer(n_prob_lastname2),
                        atf::buffer(n_prob_lastname3),
                        atf::buffer(n_prob_firstname1),
                        atf::buffer(n_prob_firstname2),
                        atf::buffer(n_prob_firstname3),
                        atf::buffer(n_prob_birthname1),
                        atf::buffer(n_prob_birthname2),
                        atf::buffer(n_prob_birthname3),
                        atf::buffer(n_prob_birthday),
                        atf::buffer(n_prob_gender),
                        atf::buffer(n_prob_birthmonth),
                        atf::buffer(n_prob_birthyear),
                        atf::buffer(n_prob_cin),

                        atf::buffer(i_id),
                        atf::buffer(i_lastname1),
                        atf::buffer(i_lastname2),
                        atf::buffer(i_lastname3),
                        atf::buffer(i_firstname1),
                        atf::buffer(i_firstname2),
                        atf::buffer(i_firstname3),
                        atf::buffer(i_firstname_group1),
                        atf::buffer(i_firstname_group2),
                        atf::buffer(i_firstname_group3),
                        atf::buffer(i_birthname1),
                        atf::buffer(i_birthname2),
                        atf::buffer(i_birthname3),
                        atf::buffer(i_birthday),
                        atf::buffer(i_gender),
                        atf::buffer(i_birthmonth),
                        atf::buffer(i_birthyear),
                        atf::buffer(i_cin),
                        atf::buffer(i_prob_lastname1),
                        atf::buffer(i_prob_lastname2),
                        atf::buffer(i_prob_lastname3),
                        atf::buffer(i_prob_firstname1),
                        atf::buffer(i_prob_firstname2),
                        atf::buffer(i_prob_firstname3),
                        atf::buffer(i_prob_birthname1),
                        atf::buffer(i_prob_birthname2),
                        atf::buffer(i_prob_birthname3),
                        atf::buffer(i_prob_birthday),
                        atf::buffer(i_prob_gender),
                        atf::buffer(i_prob_birthmonth),
                        atf::buffer(i_prob_birthyear),
                        atf::buffer(i_prob_cin)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "../../../evaluation/kernels/rl_2.cl", "rl_2", ""},
            atf::inputs(atf::buffer(res_match_id), atf::buffer(res_match_weight), atf::buffer(res_id_measure)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );
#endif

    // create tuner
#if defined(ANNEALING_FLAT)
//    auto tuner = atf::annealing(new atf::cond::duration<std::chrono::minutes>(abort_after_minutes));
    // annealing is only able to test a single configuration in 4h
    // -> do not attempt to test a second one to enable the tuner to exit gracefully
    auto tuner = atf::random_search(new atf::cond::valid_evaluations(1));
#elif defined(ANNEALING_TREE)
    auto tuner = atf::annealing_tree(new atf::cond::duration<std::chrono::minutes>(abort_after_minutes));
#elif defined(OPENTUNER_FLAT)
    auto tuner = atf::open_tuner_flat(new atf::cond::duration<std::chrono::minutes>(abort_after_minutes));
    tuner.set_path_to_database(std::tmpnam(nullptr));
#elif defined(OPENTUNER_TREE)
    auto tuner = atf::open_tuner(new atf::cond::duration<std::chrono::minutes>(abort_after_minutes));
    tuner.set_path_to_database(std::tmpnam(nullptr));
#endif

    // add parameters to tuner
#if defined(GAUSSIAN)
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);
#elif defined(GEMM)
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);
#elif defined(TENSOR_CONTRACTION)
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_L_3, OCL_DIM_L_4, OCL_DIM_L_5, OCL_DIM_L_6, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (INPUT_SIZE_L_3, L_CB_SIZE_L_3, P_CB_SIZE_L_3, NUM_WG_L_3, NUM_WI_L_3)
            (INPUT_SIZE_L_4, L_CB_SIZE_L_4, P_CB_SIZE_L_4, NUM_WG_L_4, NUM_WI_L_4)
            (INPUT_SIZE_L_5, L_CB_SIZE_L_5, P_CB_SIZE_L_5, NUM_WG_L_5, NUM_WI_L_5)
            (INPUT_SIZE_L_6, L_CB_SIZE_L_6, P_CB_SIZE_L_6, NUM_WG_L_6, NUM_WI_L_6)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);
#elif defined(PRL)
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);
#endif

    // start tuning
    auto best_config = tuner(kernel);

    // measure best found configuration
    kernel.warm_ups(10);
    kernel.evaluations(200);
    auto best_result = kernel(best_config);

    // write results to file
    std::string results_dir = std::getenv("ARTIFACT_ROOT");
    results_dir.append("/results/exploration/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id));
#if defined(ANNEALING_FLAT)
    results_dir.append("/SA_1D/");
#elif defined(ANNEALING_TREE)
    results_dir.append("/SA_MD/");
#elif defined(OPENTUNER_FLAT)
    results_dir.append("/AUC_1D/");
#elif defined(OPENTUNER_TREE)
    results_dir.append("/AUC_MD/");
#endif
#if defined(GAUSSIAN)
    results_dir.append("conv_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2));
#elif defined(GEMM)
    results_dir.append("gemm_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2))
            .append("x").append(std::to_string(input_size_3));
#elif defined(TENSOR_CONTRACTION)
    results_dir.append("ccsdt_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2))
            .append("x").append(std::to_string(input_size_3))
            .append("x").append(std::to_string(input_size_4))
            .append("x").append(std::to_string(input_size_5))
            .append("x").append(std::to_string(input_size_6))
            .append("x").append(std::to_string(input_size_7));
#elif defined(PRL)
    results_dir.append("prl_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2));
#endif
    results_dir.append("_").append(std::to_string(run_num));
    std::ofstream runtime_file(results_dir, std::ios::out | std::ios::trunc);
    runtime_file << (best_result / 1000000.0f);
    runtime_file.close();
}
