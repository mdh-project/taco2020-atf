cmake_minimum_required(VERSION 2.8.11)

project(storage)

add_executable(storage_conv libraries/atf/src/abort_conditions.cpp
		libraries/atf/src/ocl_wrapper.cpp
		libraries/atf/src/op_wrapper.cpp
		libraries/atf/src/tp_value.cpp
		libraries/atf/src/tp_value_node.cpp
		libraries/atf/src/tuner_with_constraints.cpp
		libraries/atf/src/tuner_without_constraints.cpp
		libraries/atf/src/value_type.cpp
		main.cpp)
target_include_directories(storage_conv PUBLIC ${PYTHON_INCLUDE_DIR} ${OpenCL_INCLUDE_DIR})
target_link_libraries(storage_conv ${PYTHON_LIBRARY} ${OpenCL_LIBRARY} pthread)
target_compile_definitions(storage_conv PUBLIC GAUSSIAN)

add_executable(storage_gemm libraries/atf/src/abort_conditions.cpp
		libraries/atf/src/ocl_wrapper.cpp
		libraries/atf/src/op_wrapper.cpp
		libraries/atf/src/tp_value.cpp
		libraries/atf/src/tp_value_node.cpp
		libraries/atf/src/tuner_with_constraints.cpp
		libraries/atf/src/tuner_without_constraints.cpp
		libraries/atf/src/value_type.cpp
		main.cpp)
target_include_directories(storage_gemm PUBLIC ${PYTHON_INCLUDE_DIR} ${OpenCL_INCLUDE_DIR})
target_link_libraries(storage_gemm ${PYTHON_LIBRARY} ${OpenCL_LIBRARY} pthread)
target_compile_definitions(storage_gemm PUBLIC GEMM)

add_executable(storage_ccsdt libraries/atf/src/abort_conditions.cpp
		libraries/atf/src/ocl_wrapper.cpp
		libraries/atf/src/op_wrapper.cpp
		libraries/atf/src/tp_value.cpp
		libraries/atf/src/tp_value_node.cpp
		libraries/atf/src/tuner_with_constraints.cpp
		libraries/atf/src/tuner_without_constraints.cpp
		libraries/atf/src/value_type.cpp
		main.cpp)
target_include_directories(storage_ccsdt PUBLIC ${PYTHON_INCLUDE_DIR} ${OpenCL_INCLUDE_DIR})
target_link_libraries(storage_ccsdt ${PYTHON_LIBRARY} ${OpenCL_LIBRARY} pthread)
target_compile_definitions(storage_ccsdt PUBLIC TENSOR_CONTRACTION)

add_executable(storage_prl libraries/atf/src/abort_conditions.cpp
		libraries/atf/src/ocl_wrapper.cpp
		libraries/atf/src/op_wrapper.cpp
		libraries/atf/src/tp_value.cpp
		libraries/atf/src/tp_value_node.cpp
		libraries/atf/src/tuner_with_constraints.cpp
		libraries/atf/src/tuner_without_constraints.cpp
		libraries/atf/src/value_type.cpp
		main.cpp)
target_include_directories(storage_prl PUBLIC ${PYTHON_INCLUDE_DIR} ${OpenCL_INCLUDE_DIR})
target_link_libraries(storage_prl ${PYTHON_LIBRARY} ${OpenCL_LIBRARY} pthread)
target_compile_definitions(storage_prl PUBLIC PRL)
