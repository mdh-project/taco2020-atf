#include "libraries/atf/atf.h"
#include "libraries/atf/include/InfInt.h"

void check_error(cl_int err) {
    if (err != CL_SUCCESS) {
        printf("OpenCL error with errorcode: %d\n", err);
        throw std::exception();
    }
}

int main(int argc, char **argv) {
    int platform_id = std::stoi(argv[1]);
    int device_id = std::stoi(argv[2]);
#if defined(GAUSSIAN)
    int input_size_1 = -1;
    int input_size_2 = -1;
    if (argc >= 3) {
        try {
            input_size_1 = std::stoi(argv[3]);
            input_size_2 = std::stoi(argv[4]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2);
#elif defined(GEMM)
    int input_size_1 = -1;
    int input_size_2 = -1;
    int input_size_3 = -1;
    if (argc >= 4) {
        try {
            input_size_1 = std::stoi(argv[3]);
            input_size_2 = std::stoi(argv[4]);
            input_size_3 = std::stoi(argv[5]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
            input_size_3 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0 || input_size_3 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2 input_size_3" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2) + "x" + std::to_string(input_size_3);
#elif defined(TENSOR_CONTRACTION)
    int input_size_1 = -1;
    int input_size_2 = -1;
    int input_size_3 = -1;
    int input_size_4 = -1;
    int input_size_5 = -1;
    int input_size_6 = -1;
    int input_size_7 = -1;
    if (argc >= 8) {
        try {
            input_size_1 = std::stoi(argv[3]);
            input_size_2 = std::stoi(argv[4]);
            input_size_3 = std::stoi(argv[5]);
            input_size_4 = std::stoi(argv[6]);
            input_size_5 = std::stoi(argv[7]);
            input_size_6 = std::stoi(argv[8]);
            input_size_7 = std::stoi(argv[9]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
            input_size_3 = -1;
            input_size_4 = -1;
            input_size_5 = -1;
            input_size_6 = -1;
            input_size_7 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0 || input_size_3 < 0 || input_size_4 < 0 || input_size_5 < 0 || input_size_6 < 0 || input_size_7 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2 input_size_3 input_size_4 input_size_5 input_size_6 input_size_7" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2) + "x" + std::to_string(input_size_3) + "x" + std::to_string(input_size_4) + "x" + std::to_string(input_size_5) + "x" + std::to_string(input_size_6) + "x" + std::to_string(input_size_7);
#elif defined(PRL)
    int input_size_1 = -1;
    int input_size_2 = -1;
    if (argc >= 3) {
        try {
            input_size_1 = std::stoi(argv[3]);
            input_size_2 = std::stoi(argv[4]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2);
#endif

    // get platform
    std::vector<cl::Platform> platforms;
    auto error = cl::Platform::get(&platforms); check_error(error);
    if (platform_id >= platforms.size()) {
        std::cout << "No platform with id " << platform_id << std::endl;
        exit(1);
    }
    cl::Platform platform = platforms[platform_id];
    std::string platform_name;
    platform.getInfo(CL_PLATFORM_VENDOR, &platform_name);
    std::cout << "Platform with name " << platform_name << " found." << std::endl;

    // get device
    std::vector<cl::Device> devices;
    error = platform.getDevices(CL_DEVICE_TYPE_ALL, &devices); check_error(error);
    if (device_id >= devices.size()) {
        std::cout << "No device with id " << device_id << " for platform with id " << platform_id << std::endl;
        exit(1);
    }
    cl::Device device = devices[device_id];
    std::string device_name;
    device.getInfo(CL_DEVICE_NAME, &device_name);
    std::cout << "Device with name " << device_name << " found." << std::endl;

    // create tuner
    auto tuner = atf::exhaustive(atf::cond::evaluations(1));

    // create tuning parameter
#define ATF_RANGE(max) atf::interval<int>(1, max)
#if defined(GAUSSIAN)
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto L_CB_RES_DEST_LEVEL) { return L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL; });
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL; });

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0, 1}, [&](auto OCL_DIM_L_2) { return OCL_DIM_L_2 != OCL_DIM_L_1; });

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {input_size_1});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto L_CB_SIZE_L_1) { return INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0; });
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto P_CB_SIZE_L_1) { return L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0; });
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WG_L_1) { return (INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0; });
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WI_L_1) { return ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) && (NUM_WI_L_1 <= ((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1)); });

    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {input_size_2});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto L_CB_SIZE_L_2) { return INPUT_SIZE_L_2 % L_CB_SIZE_L_2 == 0; });
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto P_CB_SIZE_L_2) { return L_CB_SIZE_L_2 % P_CB_SIZE_L_2 == 0; });
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WG_L_2) { return (INPUT_SIZE_L_2 / L_CB_SIZE_L_2) % NUM_WG_L_2 == 0; });
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WI_L_2) { return ((L_CB_SIZE_L_2 / P_CB_SIZE_L_2) % NUM_WI_L_2 == 0) && (NUM_WI_L_2 <= ((INPUT_SIZE_L_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2)); });

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {2});
#elif defined(GEMM)
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto L_CB_RES_DEST_LEVEL) { return L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL; });
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL; });

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1, 2});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0, 1, 2}, [&](auto OCL_DIM_L_2) { return OCL_DIM_L_2 != OCL_DIM_L_1; });
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1, 2}, [&](auto OCL_DIM_R_1) { return (OCL_DIM_R_1 != OCL_DIM_L_1) && (OCL_DIM_R_1 != OCL_DIM_L_2); });

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {input_size_1});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto L_CB_SIZE_L_1) { return INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0; });
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto P_CB_SIZE_L_1) { return L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0; });
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WG_L_1) { return (INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0; });
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WI_L_1) { return ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) && (NUM_WI_L_1 <= ((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1)); });

    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {input_size_2});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto L_CB_SIZE_L_2) { return INPUT_SIZE_L_2 % L_CB_SIZE_L_2 == 0; });
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto P_CB_SIZE_L_2) { return L_CB_SIZE_L_2 % P_CB_SIZE_L_2 == 0; });
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WG_L_2) { return (INPUT_SIZE_L_2 / L_CB_SIZE_L_2) % NUM_WG_L_2 == 0; });
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WI_L_2) { return ((L_CB_SIZE_L_2 / P_CB_SIZE_L_2) % NUM_WI_L_2 == 0) && (NUM_WI_L_2 <= ((INPUT_SIZE_L_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2)); });

    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {input_size_3});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       ATF_RANGE(input_size_3), [&](auto L_CB_SIZE_R_1) { return INPUT_SIZE_R_1 % L_CB_SIZE_R_1 == 0; });
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       ATF_RANGE(input_size_3), [&](auto P_CB_SIZE_R_1) { return L_CB_SIZE_R_1 % P_CB_SIZE_R_1 == 0; });
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          ATF_RANGE(input_size_3), [&](auto NUM_WG_R_1) { return (INPUT_SIZE_R_1 / L_CB_SIZE_R_1) % NUM_WG_R_1 == 0; });
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          ATF_RANGE(input_size_3), [&](auto NUM_WI_R_1) { return ((L_CB_SIZE_R_1 / P_CB_SIZE_R_1) % NUM_WI_R_1 == 0) && (NUM_WI_R_1 <= ((INPUT_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1)) && (NUM_WG_R_1 == 1 || ((NUM_WG_R_1 % L_CB_SIZE_R_1 == 0))); });

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {2});
#elif defined(TENSOR_CONTRACTION)
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto L_CB_RES_DEST_LEVEL) { return L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL; });
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL; });

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1, 2, 3, 4, 5, 6});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_2) { return (OCL_DIM_L_2 != OCL_DIM_L_1); });
    auto OCL_DIM_L_3         = atf::tp("OCL_DIM_L_3",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_3) { return (OCL_DIM_L_3 != OCL_DIM_L_1) && (OCL_DIM_L_3 != OCL_DIM_L_2); });
    auto OCL_DIM_L_4         = atf::tp("OCL_DIM_L_4",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_4) { return (OCL_DIM_L_4 != OCL_DIM_L_1) && (OCL_DIM_L_4 != OCL_DIM_L_2) && (OCL_DIM_L_4 != OCL_DIM_L_3); });
    auto OCL_DIM_L_5         = atf::tp("OCL_DIM_L_5",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_5) { return (OCL_DIM_L_5 != OCL_DIM_L_1) && (OCL_DIM_L_5 != OCL_DIM_L_2) && (OCL_DIM_L_5 != OCL_DIM_L_3) && (OCL_DIM_L_5 != OCL_DIM_L_4); });
    auto OCL_DIM_L_6         = atf::tp("OCL_DIM_L_6",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_L_6) { return (OCL_DIM_L_6 != OCL_DIM_L_1) && (OCL_DIM_L_6 != OCL_DIM_L_2) && (OCL_DIM_L_6 != OCL_DIM_L_3) && (OCL_DIM_L_6 != OCL_DIM_L_4) && (OCL_DIM_L_6 != OCL_DIM_L_5); });
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1, 2, 3, 4, 5, 6}, [&](auto OCL_DIM_R_1) { return (OCL_DIM_R_1 != OCL_DIM_L_1) && (OCL_DIM_R_1 != OCL_DIM_L_2) && (OCL_DIM_R_1 != OCL_DIM_L_3) && (OCL_DIM_R_1 != OCL_DIM_L_4) && (OCL_DIM_R_1 != OCL_DIM_L_5) && (OCL_DIM_R_1 != OCL_DIM_L_6); });

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {input_size_1});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto L_CB_SIZE_L_1) { return INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0; });
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto P_CB_SIZE_L_1) { return L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0; });
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WG_L_1) { return (INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0; });
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WI_L_1) { return ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) && (NUM_WI_L_1 <= ((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1)); });

    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {input_size_2});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto L_CB_SIZE_L_2) { return INPUT_SIZE_L_2 % L_CB_SIZE_L_2 == 0; });
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       ATF_RANGE(input_size_2), [&](auto P_CB_SIZE_L_2) { return L_CB_SIZE_L_2 % P_CB_SIZE_L_2 == 0; });
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WG_L_2) { return (INPUT_SIZE_L_2 / L_CB_SIZE_L_2) % NUM_WG_L_2 == 0; });
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          ATF_RANGE(input_size_2), [&](auto NUM_WI_L_2) { return ((L_CB_SIZE_L_2 / P_CB_SIZE_L_2) % NUM_WI_L_2 == 0) && (NUM_WI_L_2 <= ((INPUT_SIZE_L_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2)); });

    auto INPUT_SIZE_L_3      = atf::tp("INPUT_SIZE_L_3",      {input_size_3});
    auto L_CB_SIZE_L_3       = atf::tp("L_CB_SIZE_L_3",       ATF_RANGE(input_size_3), [&](auto L_CB_SIZE_L_3) { return INPUT_SIZE_L_3 % L_CB_SIZE_L_3 == 0; });
    auto P_CB_SIZE_L_3       = atf::tp("P_CB_SIZE_L_3",       ATF_RANGE(input_size_3), [&](auto P_CB_SIZE_L_3) { return L_CB_SIZE_L_3 % P_CB_SIZE_L_3 == 0; });
    auto NUM_WG_L_3          = atf::tp("NUM_WG_L_3",          ATF_RANGE(input_size_3), [&](auto NUM_WG_L_3) { return (INPUT_SIZE_L_3 / L_CB_SIZE_L_3) % NUM_WG_L_3 == 0; });
    auto NUM_WI_L_3          = atf::tp("NUM_WI_L_3",          ATF_RANGE(input_size_3), [&](auto NUM_WI_L_3) { return ((L_CB_SIZE_L_3 / P_CB_SIZE_L_3) % NUM_WI_L_3 == 0) && (NUM_WI_L_3 <= ((INPUT_SIZE_L_3 + NUM_WG_L_3 - 1) / NUM_WG_L_3)); });

    auto INPUT_SIZE_L_4      = atf::tp("INPUT_SIZE_L_4",      {input_size_4});
    auto L_CB_SIZE_L_4       = atf::tp("L_CB_SIZE_L_4",       ATF_RANGE(input_size_4), [&](auto L_CB_SIZE_L_4) { return INPUT_SIZE_L_4 % L_CB_SIZE_L_4 == 0; });
    auto P_CB_SIZE_L_4       = atf::tp("P_CB_SIZE_L_4",       ATF_RANGE(input_size_4), [&](auto P_CB_SIZE_L_4) { return L_CB_SIZE_L_4 % P_CB_SIZE_L_4 == 0; });
    auto NUM_WG_L_4          = atf::tp("NUM_WG_L_4",          ATF_RANGE(input_size_4), [&](auto NUM_WG_L_4) { return (INPUT_SIZE_L_4 / L_CB_SIZE_L_4) % NUM_WG_L_4 == 0; });
    auto NUM_WI_L_4          = atf::tp("NUM_WI_L_4",          ATF_RANGE(input_size_4), [&](auto NUM_WI_L_4) { return ((L_CB_SIZE_L_4 / P_CB_SIZE_L_4) % NUM_WI_L_4 == 0) && (NUM_WI_L_4 <= ((INPUT_SIZE_L_4 + NUM_WG_L_4 - 1) / NUM_WG_L_4)); });

    auto INPUT_SIZE_L_5      = atf::tp("INPUT_SIZE_L_5",      {input_size_5});
    auto L_CB_SIZE_L_5       = atf::tp("L_CB_SIZE_L_5",       ATF_RANGE(input_size_5), [&](auto L_CB_SIZE_L_5) { return INPUT_SIZE_L_5 % L_CB_SIZE_L_5 == 0; });
    auto P_CB_SIZE_L_5       = atf::tp("P_CB_SIZE_L_5",       ATF_RANGE(input_size_5), [&](auto P_CB_SIZE_L_5) { return L_CB_SIZE_L_5 % P_CB_SIZE_L_5 == 0; });
    auto NUM_WG_L_5          = atf::tp("NUM_WG_L_5",          ATF_RANGE(input_size_5), [&](auto NUM_WG_L_5) { return (INPUT_SIZE_L_5 / L_CB_SIZE_L_5) % NUM_WG_L_5 == 0; });
    auto NUM_WI_L_5          = atf::tp("NUM_WI_L_5",          ATF_RANGE(input_size_5), [&](auto NUM_WI_L_5) { return ((L_CB_SIZE_L_5 / P_CB_SIZE_L_5) % NUM_WI_L_5 == 0) && (NUM_WI_L_5 <= ((INPUT_SIZE_L_5 + NUM_WG_L_5 - 1) / NUM_WG_L_5)); });

    auto INPUT_SIZE_L_6      = atf::tp("INPUT_SIZE_L_6",      {input_size_6});
    auto L_CB_SIZE_L_6       = atf::tp("L_CB_SIZE_L_6",       ATF_RANGE(input_size_6), [&](auto L_CB_SIZE_L_6) { return INPUT_SIZE_L_6 % L_CB_SIZE_L_6 == 0; });
    auto P_CB_SIZE_L_6       = atf::tp("P_CB_SIZE_L_6",       ATF_RANGE(input_size_6), [&](auto P_CB_SIZE_L_6) { return L_CB_SIZE_L_6 % P_CB_SIZE_L_6 == 0; });
    auto NUM_WG_L_6          = atf::tp("NUM_WG_L_6",          ATF_RANGE(input_size_6), [&](auto NUM_WG_L_6) { return (INPUT_SIZE_L_6 / L_CB_SIZE_L_6) % NUM_WG_L_6 == 0; });
    auto NUM_WI_L_6          = atf::tp("NUM_WI_L_6",          ATF_RANGE(input_size_6), [&](auto NUM_WI_L_6) { return ((L_CB_SIZE_L_6 / P_CB_SIZE_L_6) % NUM_WI_L_6 == 0) && (NUM_WI_L_6 <= ((INPUT_SIZE_L_6 + NUM_WG_L_6 - 1) / NUM_WG_L_6)); });

    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {input_size_7});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       ATF_RANGE(input_size_7), [&](auto L_CB_SIZE_R_1) { return INPUT_SIZE_R_1 % L_CB_SIZE_R_1 == 0; });
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       ATF_RANGE(input_size_7), [&](auto P_CB_SIZE_R_1) { return L_CB_SIZE_R_1 % P_CB_SIZE_R_1 == 0; });
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          ATF_RANGE(input_size_7), [&](auto NUM_WG_R_1) { return (INPUT_SIZE_R_1 / L_CB_SIZE_R_1) % NUM_WG_R_1 == 0; });
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          ATF_RANGE(input_size_7), [&](auto NUM_WI_R_1) { return ((L_CB_SIZE_R_1 / P_CB_SIZE_R_1) % NUM_WI_R_1 == 0) && (NUM_WI_R_1 <= ((INPUT_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1)) && (NUM_WG_R_1 == 1 || ((NUM_WG_R_1 % L_CB_SIZE_R_1 == 0))); });

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {6});
#elif defined(PRL)
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto L_CB_RES_DEST_LEVEL) { return L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL; });
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, [&](auto P_CB_RES_DEST_LEVEL) { return P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL; });

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1}, [&](auto OCL_DIM_R_1) { return OCL_DIM_R_1 != OCL_DIM_L_1; });

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {input_size_1});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto L_CB_SIZE_L_1) { return INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0; });
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       ATF_RANGE(input_size_1), [&](auto P_CB_SIZE_L_1) { return L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0; });
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WG_L_1) { return (INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0; });
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          ATF_RANGE(input_size_1), [&](auto NUM_WI_L_1) { return ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) && (NUM_WI_L_1 <= ((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1)); });

    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {input_size_2});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       ATF_RANGE(input_size_2), [&](auto L_CB_SIZE_R_1) { return INPUT_SIZE_R_1 % L_CB_SIZE_R_1 == 0; });
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       ATF_RANGE(input_size_2), [&](auto P_CB_SIZE_R_1) { return L_CB_SIZE_R_1 % P_CB_SIZE_R_1 == 0; });
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          ATF_RANGE(input_size_2), [&](auto NUM_WG_R_1) { return (INPUT_SIZE_R_1 / L_CB_SIZE_R_1) % NUM_WG_R_1 == 0; });
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          ATF_RANGE(input_size_2), [&](auto NUM_WI_R_1) { return ((L_CB_SIZE_R_1 / P_CB_SIZE_R_1) % NUM_WI_R_1 == 0) && (NUM_WI_R_1 <= ((INPUT_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1)) && (NUM_WG_R_1 == 1 || ((NUM_WG_R_1 % L_CB_SIZE_R_1 == 0))); });

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {1});
#endif

    // generate search space to calculate memory usage
#if defined(GAUSSIAN)
    constexpr int dims = 2;
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);
#elif defined(GEMM)
    constexpr int dims = 3;
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);
#elif defined(TENSOR_CONTRACTION)
    constexpr int dims = 7;
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_L_3, OCL_DIM_L_4, OCL_DIM_L_5, OCL_DIM_L_6, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (INPUT_SIZE_L_3, L_CB_SIZE_L_3, P_CB_SIZE_L_3, NUM_WG_L_3, NUM_WI_L_3)
            (INPUT_SIZE_L_4, L_CB_SIZE_L_4, P_CB_SIZE_L_4, NUM_WG_L_4, NUM_WI_L_4)
            (INPUT_SIZE_L_5, L_CB_SIZE_L_5, P_CB_SIZE_L_5, NUM_WG_L_5, NUM_WI_L_5)
            (INPUT_SIZE_L_6, L_CB_SIZE_L_6, P_CB_SIZE_L_6, NUM_WG_L_6, NUM_WI_L_6)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);
#elif defined(PRL)
    constexpr int dims = 2;
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);
#endif
    tuner([] (auto configuration) { return 0; }); // trigger search space generation
    const InfInt num_tree_nodes = atf::tp_value_node::number_of_nodes();
    const InfInt search_space_size = tuner.search_space_size();
    InfInt unconstrained_search_space_size = 1;
#if defined(GAUSSIAN)
    unconstrained_search_space_size *= 2 * 2;
    unconstrained_search_space_size *= 3 * 3;
    unconstrained_search_space_size *= 2 * 2;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
#elif defined(GEMM)
    unconstrained_search_space_size *= 2 * 2;
    unconstrained_search_space_size *= 3 * 3;
    unconstrained_search_space_size *= 3 * 3 * 3;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_3;
    unconstrained_search_space_size *= input_size_3;
    unconstrained_search_space_size *= input_size_3;
    unconstrained_search_space_size *= input_size_3;
#elif defined(TENSOR_CONTRACTION)
    unconstrained_search_space_size *= 2 * 2;
    unconstrained_search_space_size *= 3 * 3;
    unconstrained_search_space_size *= 7 * 7 * 7 * 7 * 7 * 7 * 7;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_3;
    unconstrained_search_space_size *= input_size_3;
    unconstrained_search_space_size *= input_size_3;
    unconstrained_search_space_size *= input_size_3;
    unconstrained_search_space_size *= input_size_4;
    unconstrained_search_space_size *= input_size_4;
    unconstrained_search_space_size *= input_size_4;
    unconstrained_search_space_size *= input_size_4;
    unconstrained_search_space_size *= input_size_5;
    unconstrained_search_space_size *= input_size_5;
    unconstrained_search_space_size *= input_size_5;
    unconstrained_search_space_size *= input_size_5;
    unconstrained_search_space_size *= input_size_6;
    unconstrained_search_space_size *= input_size_6;
    unconstrained_search_space_size *= input_size_6;
    unconstrained_search_space_size *= input_size_6;
    unconstrained_search_space_size *= input_size_7;
    unconstrained_search_space_size *= input_size_7;
    unconstrained_search_space_size *= input_size_7;
    unconstrained_search_space_size *= input_size_7;
#elif defined(PRL)
    unconstrained_search_space_size *= 2 * 2;
    unconstrained_search_space_size *= 3 * 3;
    unconstrained_search_space_size *= 2 * 2;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_1;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
    unconstrained_search_space_size *= input_size_2;
#endif
    const InfInt num_tree_edges = num_tree_nodes + (5 + dims);
    InfInt atf_bytes = num_tree_nodes * 1 + num_tree_edges * 8;
    InfInt cltune_bytes = search_space_size * (8 + dims * 6) * 1 / 8;

    // write byte count to files
    std::string results_dir = std::getenv("ARTIFACT_ROOT");
    results_dir.append("/results/storage/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id));
    std::string atf_file = results_dir;
    std::string cltune_file = results_dir;
    atf_file.append("/MD/");
    cltune_file.append("/1D/");
#if defined(GAUSSIAN)
    atf_file.append("conv_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2));
    cltune_file.append("conv_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2));
#elif defined(GEMM)
    atf_file.append("gemm_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2))
            .append("x").append(std::to_string(input_size_3));
    cltune_file.append("gemm_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2))
            .append("x").append(std::to_string(input_size_3));
#elif defined(TENSOR_CONTRACTION)
    atf_file.append("ccsdt_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2))
            .append("x").append(std::to_string(input_size_3))
            .append("x").append(std::to_string(input_size_4))
            .append("x").append(std::to_string(input_size_5))
            .append("x").append(std::to_string(input_size_6))
            .append("x").append(std::to_string(input_size_7));
    cltune_file.append("ccsdt_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2))
            .append("x").append(std::to_string(input_size_3))
            .append("x").append(std::to_string(input_size_4))
            .append("x").append(std::to_string(input_size_5))
            .append("x").append(std::to_string(input_size_6))
            .append("x").append(std::to_string(input_size_7));
#elif defined(PRL)
    atf_file.append("prl_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2));
    cltune_file.append("prl_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2));
#endif

    std::ofstream result_file(atf_file, std::ios::out | std::ios::trunc);
    result_file << atf_bytes;
    result_file.close();

    result_file = std::ofstream(cltune_file, std::ios::out | std::ios::trunc);
    result_file << cltune_bytes;
    result_file.close();
}
