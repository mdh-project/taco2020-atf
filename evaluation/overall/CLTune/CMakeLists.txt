cmake_minimum_required(VERSION 2.8.11)
project(overall_cltune)

add_executable(overall_cltune_gemm gemm.cpp)
target_include_directories(overall_cltune_gemm PUBLIC ${ATF_INCLUDE_DIRS})
target_link_libraries(overall_cltune_gemm atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(overall_cltune_conv gaussian.cpp)
target_include_directories(overall_cltune_conv PUBLIC ${ATF_INCLUDE_DIRS})
target_link_libraries(overall_cltune_conv atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(overall_cltune_prl rl.cpp)
target_include_directories(overall_cltune_prl PUBLIC ${ATF_RL_INCLUDE_DIRS})
target_link_libraries(overall_cltune_prl atf_rl ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(overall_cltune_ccsdt tc.cpp)
target_include_directories(overall_cltune_ccsdt PUBLIC ${ATF_INCLUDE_DIRS})
target_link_libraries(overall_cltune_ccsdt atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)