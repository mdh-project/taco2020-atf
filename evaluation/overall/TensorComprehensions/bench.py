import time
import tensor_comprehensions as tc
import torch
import os
import sys

device_id = 0

sizes = dict()
O_dims = sys.argv[1]
for dim in O_dims:
	sizes[dim] = 0
L_dims = sys.argv[2]
for dim in L_dims:
	sizes[dim] = 0
R_dims = sys.argv[3]
for dim in R_dims:
	sizes[dim] = 0

sizes_sorted = []
i = 0
for k in sorted(sizes.keys()):
	sizes[k] = int(sys.argv[4+i])
	sizes_sorted.append(str(sizes[k]))
	i += 1
L_sizes = []
for dim in L_dims:
	L_sizes.append(sizes[dim])
R_sizes = []
for dim in R_dims:
	R_sizes.append(sizes[dim])

torch.cuda.device(device_id)
if (not os.path.isfile("{0}/results/overall/gpu/tensor_comprehensions/tc_{1}{2}.cuda".format(os.environ["ARTIFACT_ROOT"], "x".join(sizes_sorted), os.environ["FILES_SUFFIX"]))) or \
		(not os.path.isfile("{0}/overall/results/gpu/tensor_comprehensions/tc_{1}{2}.options".format(os.environ["ARTIFACT_ROOT"], "x".join(sizes_sorted), os.environ["FILES_SUFFIX"]))):
	print("Unable to benchmark Tensor Comprehensions. Has it not yet been tuned for this input size?")
	sys.exit(1)

lang = """
def tccg(float({0}) L, float({1}) R) -> (O) {{
	O({2}) +=! L({3}) * R({4})
}}
"""
lang.format(", ".join([x.upper() for x in L_dims]), ", ".join([x.upper() for x in R_dims]), ", ".join(O_dims), ", ".join(L_dims), ", ".join(R_dims))

matmul = tc.define(lang, cache="{0}/results/overall/gpu/tensor_comprehensions/tc_{1}{2}".format(os.environ["ARTIFACT_ROOT"], "x".join(sizes_sorted), os.environ["FILES_SUFFIX"]), name="tccg")
mat1, mat2 = torch.randn(sizes=L_sizes).cuda(), torch.randn(sizes=R_sizes).cuda()
out = matmul(mat1, mat2)
torch.cuda.synchronize()
# warm ups
for i in range(0, 10):
	out = matmul(mat1, mat2)
#evaluations
times = []
for i in range(0, 200):
	start = time.process_time()
	out = matmul(mat1, mat2)
	torch.cuda.synchronize()
	end = time.process_time()
	times.append(int((end-start) * 1000000000))
torch.cuda.synchronize()

print ("time: ", min(times))
with open("{0}/results/overall/gpu/tensor_comprehensions/tc_{1}{2}_runtime".format(os.environ["ARTIFACT_ROOT"], "x".join(sizes_sorted), os.environ["FILES_SUFFIX"]), 'w') as file:
	file.write(str(min(times) / 1000000.0))