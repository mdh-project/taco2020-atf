from constraint import *
import sys
import timeit

M = int(sys.argv[1])
N = int(sys.argv[2])
K = int(sys.argv[3])

problem = Problem()

problem.addVariable('CACHE_L_CB',          [0, 1])
problem.addVariable('CACHE_P_CB',          [0, 1])
problem.addVariable('G_CB_RES_DEST_LEVEL', [2])
problem.addVariable('L_CB_RES_DEST_LEVEL', [2, 1, 0])
problem.addVariable('P_CB_RES_DEST_LEVEL', [2, 1, 0])

problem.addVariable('INPUT_SIZE_L_1',      [M])
problem.addVariable('L_CB_SIZE_L_1',       range(1, M + 1))
problem.addVariable('P_CB_SIZE_L_1',       range(1, M + 1))
problem.addVariable('OCL_DIM_L_1',         [0, 1, 2])
problem.addVariable('NUM_WG_L_1',          range(1, M + 1))
problem.addVariable('NUM_WI_L_1',          range(1, M + 1))

problem.addVariable('INPUT_SIZE_L_2',      [N])
problem.addVariable('L_CB_SIZE_L_2',       range(1, N + 1))
problem.addVariable('P_CB_SIZE_L_2',       range(1, N + 1))
problem.addVariable('OCL_DIM_L_2',         [0, 1, 2])
problem.addVariable('NUM_WG_L_2',          range(1, N + 1))
problem.addVariable('NUM_WI_L_2',          range(1, N + 1))

problem.addVariable('INPUT_SIZE_R_1',      [K])
problem.addVariable('L_CB_SIZE_R_1',       range(1, K + 1))
problem.addVariable('P_CB_SIZE_R_1',       range(1, K + 1))
problem.addVariable('OCL_DIM_R_1',         [0, 1, 2])
problem.addVariable('NUM_WG_R_1',          range(1, K + 1))
problem.addVariable('NUM_WI_R_1',          range(1, K + 1))

problem.addVariable('L_REDUCTION',         [1])
problem.addVariable('P_WRITE_BACK',        [0])
problem.addVariable('L_WRITE_BACK',        [2])

problem.addConstraint(lambda CACHE_L_CB, CACHE_P_CB, G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL,
                             INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, OCL_DIM_L_1, NUM_WG_L_1, NUM_WI_L_1,
                             INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, OCL_DIM_L_2, NUM_WG_L_2, NUM_WI_L_2,
                             INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, OCL_DIM_R_1, NUM_WG_R_1, NUM_WI_R_1:
                      (L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL) and
                      (P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL) and
                      (INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0) and
                      (L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0) and
                      ((INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0) and
                      ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) and
                      (INPUT_SIZE_L_2 % L_CB_SIZE_L_2 == 0) and
                      (L_CB_SIZE_L_2 % P_CB_SIZE_L_2 == 0) and
                      (OCL_DIM_L_2 != OCL_DIM_L_1) and
                      ((INPUT_SIZE_L_2 / L_CB_SIZE_L_2) % NUM_WG_L_2 == 0) and
                      ((L_CB_SIZE_L_2 / P_CB_SIZE_L_2) % NUM_WI_L_2 == 0) and
                      (INPUT_SIZE_R_1 % L_CB_SIZE_R_1 == 0) and
                      (L_CB_SIZE_R_1 % P_CB_SIZE_R_1 == 0) and
                      (OCL_DIM_R_1 != OCL_DIM_L_1) and
                      (OCL_DIM_R_1 != OCL_DIM_L_2) and
                      ((INPUT_SIZE_R_1 / L_CB_SIZE_R_1) % NUM_WG_R_1 == 0) and
                      ((L_CB_SIZE_R_1 / P_CB_SIZE_R_1) % NUM_WI_R_1 == 0) and
                      (NUM_WG_R_1 == 1 or (NUM_WG_R_1 % L_CB_SIZE_R_1 == 0)),
                      ('CACHE_L_CB', 'CACHE_P_CB', 'G_CB_RES_DEST_LEVEL', 'L_CB_RES_DEST_LEVEL', 'P_CB_RES_DEST_LEVEL',
                       'INPUT_SIZE_L_1', 'L_CB_SIZE_L_1', 'P_CB_SIZE_L_1', 'OCL_DIM_L_1', 'NUM_WG_L_1', 'NUM_WI_L_1',
                       'INPUT_SIZE_L_2', 'L_CB_SIZE_L_2', 'P_CB_SIZE_L_2', 'OCL_DIM_L_2', 'NUM_WG_L_2', 'NUM_WI_L_2',
                       'INPUT_SIZE_R_1', 'L_CB_SIZE_R_1', 'P_CB_SIZE_R_1', 'OCL_DIM_R_1', 'NUM_WG_R_1', 'NUM_WI_R_1'))

milliseconds=timeit.timeit(stmt=problem.getSolutions, number=1) * 1000
print(milliseconds)