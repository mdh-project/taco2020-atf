from constraint import *
import sys
import timeit

A = int(sys.argv[1])
B = int(sys.argv[2])
C = int(sys.argv[3])
D = int(sys.argv[4])
E = int(sys.argv[5])
F = int(sys.argv[6])
G = int(sys.argv[7])

problem = Problem()

problem.addVariable('CACHE_L_CB',          [0, 1])
problem.addVariable('CACHE_P_CB',          [0, 1])
problem.addVariable('G_CB_RES_DEST_LEVEL', [2])
problem.addVariable('L_CB_RES_DEST_LEVEL', [2, 1, 0])
problem.addVariable('P_CB_RES_DEST_LEVEL', [2, 1, 0])

problem.addVariable('INPUT_SIZE_L_1',      [A])
problem.addVariable('L_CB_SIZE_L_1',       range(1, A + 1))
problem.addVariable('P_CB_SIZE_L_1',       range(1, A + 1))
problem.addVariable('OCL_DIM_L_1',         [0, 1, 2, 3, 4, 5, 6])
problem.addVariable('NUM_WG_L_1',          range(1, A + 1))
problem.addVariable('NUM_WI_L_1',          range(1, A + 1))

problem.addVariable('INPUT_SIZE_L_2',      [B])
problem.addVariable('L_CB_SIZE_L_2',       range(1, B + 1))
problem.addVariable('P_CB_SIZE_L_2',       range(1, B + 1))
problem.addVariable('OCL_DIM_L_2',         [0, 1, 2, 3, 4, 5, 6])
problem.addVariable('NUM_WG_L_2',          range(1, B + 1))
problem.addVariable('NUM_WI_L_2',          range(1, B + 1))

problem.addVariable('INPUT_SIZE_L_3',      [C])
problem.addVariable('L_CB_SIZE_L_3',       range(1, C + 1))
problem.addVariable('P_CB_SIZE_L_3',       range(1, C + 1))
problem.addVariable('OCL_DIM_L_3',         [0, 1, 2, 3, 4, 5, 6])
problem.addVariable('NUM_WG_L_3',          range(1, C + 1))
problem.addVariable('NUM_WI_L_3',          range(1, C + 1))

problem.addVariable('INPUT_SIZE_L_4',      [D])
problem.addVariable('L_CB_SIZE_L_4',       range(1, D + 1))
problem.addVariable('P_CB_SIZE_L_4',       range(1, D + 1))
problem.addVariable('OCL_DIM_L_4',         [0, 1, 2, 3, 4, 5, 6])
problem.addVariable('NUM_WG_L_4',          range(1, D + 1))
problem.addVariable('NUM_WI_L_4',          range(1, D + 1))

problem.addVariable('INPUT_SIZE_L_5',      [E])
problem.addVariable('L_CB_SIZE_L_5',       range(1, E + 1))
problem.addVariable('P_CB_SIZE_L_5',       range(1, E + 1))
problem.addVariable('OCL_DIM_L_5',         [0, 1, 2, 3, 4, 5, 6])
problem.addVariable('NUM_WG_L_5',          range(1, E + 1))
problem.addVariable('NUM_WI_L_5',          range(1, E + 1))

problem.addVariable('INPUT_SIZE_L_6',      [F])
problem.addVariable('L_CB_SIZE_L_6',       range(1, F + 1))
problem.addVariable('P_CB_SIZE_L_6',       range(1, F + 1))
problem.addVariable('OCL_DIM_L_6',         [0, 1, 2, 3, 4, 5, 6])
problem.addVariable('NUM_WG_L_6',          range(1, F + 1))
problem.addVariable('NUM_WI_L_6',          range(1, F + 1))

problem.addVariable('INPUT_SIZE_R_1',      [G])
problem.addVariable('L_CB_SIZE_R_1',       range(1, G + 1))
problem.addVariable('P_CB_SIZE_R_1',       range(1, G + 1))
problem.addVariable('OCL_DIM_R_1',         [0, 1, 2, 3, 4, 5, 6])
problem.addVariable('NUM_WG_R_1',          range(1, G + 1))
problem.addVariable('NUM_WI_R_1',          range(1, G + 1))

problem.addVariable('L_REDUCTION',         [1])
problem.addVariable('P_WRITE_BACK',        [0])
problem.addVariable('L_WRITE_BACK',        [6])

problem.addConstraint(lambda CACHE_L_CB, CACHE_P_CB, G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL,
                             INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, OCL_DIM_L_1, NUM_WG_L_1, NUM_WI_L_1,
                             INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, OCL_DIM_L_2, NUM_WG_L_2, NUM_WI_L_2,
                             INPUT_SIZE_L_3, L_CB_SIZE_L_3, P_CB_SIZE_L_3, OCL_DIM_L_3, NUM_WG_L_3, NUM_WI_L_3,
                             INPUT_SIZE_L_4, L_CB_SIZE_L_4, P_CB_SIZE_L_4, OCL_DIM_L_4, NUM_WG_L_4, NUM_WI_L_4,
                             INPUT_SIZE_L_5, L_CB_SIZE_L_5, P_CB_SIZE_L_5, OCL_DIM_L_5, NUM_WG_L_5, NUM_WI_L_5,
                             INPUT_SIZE_L_6, L_CB_SIZE_L_6, P_CB_SIZE_L_6, OCL_DIM_L_6, NUM_WG_L_6, NUM_WI_L_6,
                             INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, OCL_DIM_R_1, NUM_WG_R_1, NUM_WI_R_1:
                      (L_CB_RES_DEST_LEVEL <= G_CB_RES_DEST_LEVEL) and
                      (P_CB_RES_DEST_LEVEL <= L_CB_RES_DEST_LEVEL) and
                      (INPUT_SIZE_L_1 % L_CB_SIZE_L_1 == 0) and
                      (L_CB_SIZE_L_1 % P_CB_SIZE_L_1 == 0) and
                      ((INPUT_SIZE_L_1 / L_CB_SIZE_L_1) % NUM_WG_L_1 == 0) and
                      ((L_CB_SIZE_L_1 / P_CB_SIZE_L_1) % NUM_WI_L_1 == 0) and
                      (INPUT_SIZE_L_2 % L_CB_SIZE_L_2 == 0) and
                      (L_CB_SIZE_L_2 % P_CB_SIZE_L_2 == 0) and
                      (OCL_DIM_L_2 != OCL_DIM_L_1) and
                      ((INPUT_SIZE_L_2 / L_CB_SIZE_L_2) % NUM_WG_L_2 == 0) and
                      ((L_CB_SIZE_L_2 / P_CB_SIZE_L_2) % NUM_WI_L_2 == 0) and
                      (INPUT_SIZE_L_3 % L_CB_SIZE_L_3 == 0) and
                      (L_CB_SIZE_L_3 % P_CB_SIZE_L_3 == 0) and
                      (OCL_DIM_L_3 != OCL_DIM_L_1) and
                      (OCL_DIM_L_3 != OCL_DIM_L_2) and
                      ((INPUT_SIZE_L_3 / L_CB_SIZE_L_3) % NUM_WG_L_3 == 0) and
                      ((L_CB_SIZE_L_3 / P_CB_SIZE_L_3) % NUM_WI_L_3 == 0) and
                      (INPUT_SIZE_L_4 % L_CB_SIZE_L_4 == 0) and
                      (L_CB_SIZE_L_4 % P_CB_SIZE_L_4 == 0) and
                      (OCL_DIM_L_4 != OCL_DIM_L_1) and
                      (OCL_DIM_L_4 != OCL_DIM_L_2) and
                      (OCL_DIM_L_4 != OCL_DIM_L_3) and
                      ((INPUT_SIZE_L_4 / L_CB_SIZE_L_4) % NUM_WG_L_4 == 0) and
                      ((L_CB_SIZE_L_4 / P_CB_SIZE_L_4) % NUM_WI_L_4 == 0) and
                      (INPUT_SIZE_L_5 % L_CB_SIZE_L_5 == 0) and
                      (L_CB_SIZE_L_5 % P_CB_SIZE_L_5 == 0) and
                      (OCL_DIM_L_5 != OCL_DIM_L_1) and
                      (OCL_DIM_L_5 != OCL_DIM_L_2) and
                      (OCL_DIM_L_5 != OCL_DIM_L_3) and
                      (OCL_DIM_L_5 != OCL_DIM_L_4) and
                      ((INPUT_SIZE_L_5 / L_CB_SIZE_L_5) % NUM_WG_L_5 == 0) and
                      ((L_CB_SIZE_L_5 / P_CB_SIZE_L_5) % NUM_WI_L_5 == 0) and
                      (INPUT_SIZE_L_6 % L_CB_SIZE_L_6 == 0) and
                      (L_CB_SIZE_L_6 % P_CB_SIZE_L_6 == 0) and
                      (OCL_DIM_L_6 != OCL_DIM_L_1) and
                      (OCL_DIM_L_6 != OCL_DIM_L_2) and
                      (OCL_DIM_L_6 != OCL_DIM_L_3) and
                      (OCL_DIM_L_6 != OCL_DIM_L_4) and
                      (OCL_DIM_L_6 != OCL_DIM_L_5) and
                      ((INPUT_SIZE_L_6 / L_CB_SIZE_L_6) % NUM_WG_L_6 == 0) and
                      ((L_CB_SIZE_L_6 / P_CB_SIZE_L_6) % NUM_WI_L_6 == 0) and
                      (INPUT_SIZE_R_1 % L_CB_SIZE_R_1 == 0) and
                      (L_CB_SIZE_R_1 % P_CB_SIZE_R_1 == 0) and
                      (OCL_DIM_R_1 != OCL_DIM_L_1) and
                      (OCL_DIM_R_1 != OCL_DIM_L_2) and
                      (OCL_DIM_R_1 != OCL_DIM_L_3) and
                      (OCL_DIM_R_1 != OCL_DIM_L_4) and
                      (OCL_DIM_R_1 != OCL_DIM_L_5) and
                      (OCL_DIM_R_1 != OCL_DIM_L_6) and
                      ((INPUT_SIZE_R_1 / L_CB_SIZE_R_1) % NUM_WG_R_1 == 0) and
                      ((L_CB_SIZE_R_1 / P_CB_SIZE_R_1) % NUM_WI_R_1 == 0) and
                      (NUM_WG_R_1 == 1 or (NUM_WG_R_1 % L_CB_SIZE_R_1 == 0)),
                      ('CACHE_L_CB', 'CACHE_P_CB', 'G_CB_RES_DEST_LEVEL', 'L_CB_RES_DEST_LEVEL', 'P_CB_RES_DEST_LEVEL',
                       'INPUT_SIZE_L_1', 'L_CB_SIZE_L_1', 'P_CB_SIZE_L_1', 'OCL_DIM_L_1', 'NUM_WG_L_1', 'NUM_WI_L_1',
                       'INPUT_SIZE_L_2', 'L_CB_SIZE_L_2', 'P_CB_SIZE_L_2', 'OCL_DIM_L_2', 'NUM_WG_L_2', 'NUM_WI_L_2',
                       'INPUT_SIZE_L_3', 'L_CB_SIZE_L_3', 'P_CB_SIZE_L_3', 'OCL_DIM_L_3', 'NUM_WG_L_3', 'NUM_WI_L_3',
                       'INPUT_SIZE_L_4', 'L_CB_SIZE_L_4', 'P_CB_SIZE_L_4', 'OCL_DIM_L_4', 'NUM_WG_L_4', 'NUM_WI_L_4',
                       'INPUT_SIZE_L_5', 'L_CB_SIZE_L_5', 'P_CB_SIZE_L_5', 'OCL_DIM_L_5', 'NUM_WG_L_5', 'NUM_WI_L_5',
                       'INPUT_SIZE_L_6', 'L_CB_SIZE_L_6', 'P_CB_SIZE_L_6', 'OCL_DIM_L_6', 'NUM_WG_L_6', 'NUM_WI_L_6',
                       'INPUT_SIZE_R_1', 'L_CB_SIZE_R_1', 'P_CB_SIZE_R_1', 'OCL_DIM_R_1', 'NUM_WG_R_1', 'NUM_WI_R_1'))

milliseconds=timeit.timeit(stmt=problem.getSolutions, number=1) * 1000
print(milliseconds)