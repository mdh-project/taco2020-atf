#include <fstream>
#include <iostream>
#include <cmath>
#include <chrono>

#include <CL/cl.hpp>

#include "cltune.h"

void check_error(cl_int err) {
    if (err != CL_SUCCESS) {
        printf("OpenCL error with errorcode: %d\n", err);
        throw std::exception();
    }
}

std::vector<size_t> cltune_range(size_t max) {
    std::vector<size_t> values(max);
    for (int i = 1; i <= max; ++i) {
        values[i - 1] = i;
    }
    return values;
}

int main(int argc, char **argv) {
    int platform_id = std::stoi(argv[1]);
    int device_id = std::stoi(argv[2]);
#if defined(GAUSSIAN)
    int input_size_1 = -1;
    int input_size_2 = -1;
    if (argc >= 3) {
        try {
            input_size_1 = std::stoi(argv[3]);
            input_size_2 = std::stoi(argv[4]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2);
#elif defined(GEMM)
    int input_size_1 = -1;
    int input_size_2 = -1;
    int input_size_3 = -1;
    if (argc >= 4) {
        try {
            input_size_1 = std::stoi(argv[3]);
            input_size_2 = std::stoi(argv[4]);
            input_size_3 = std::stoi(argv[5]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
            input_size_3 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0 || input_size_3 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2 input_size_3" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2) + "x" + std::to_string(input_size_3);
#elif defined(TENSOR_CONTRACTION)
    int input_size_1 = -1;
    int input_size_2 = -1;
    int input_size_3 = -1;
    int input_size_4 = -1;
    int input_size_5 = -1;
    int input_size_6 = -1;
    int input_size_7 = -1;
    if (argc >= 8) {
        try {
            input_size_1 = std::stoi(argv[3]);
            input_size_2 = std::stoi(argv[4]);
            input_size_3 = std::stoi(argv[5]);
            input_size_4 = std::stoi(argv[6]);
            input_size_5 = std::stoi(argv[7]);
            input_size_6 = std::stoi(argv[8]);
            input_size_7 = std::stoi(argv[9]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
            input_size_3 = -1;
            input_size_4 = -1;
            input_size_5 = -1;
            input_size_6 = -1;
            input_size_7 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0 || input_size_3 < 0 || input_size_4 < 0 || input_size_5 < 0 || input_size_6 < 0 || input_size_7 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2 input_size_3 input_size_4 input_size_5 input_size_6 input_size_7" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2) + "x" + std::to_string(input_size_3) + "x" + std::to_string(input_size_4) + "x" + std::to_string(input_size_5) + "x" + std::to_string(input_size_6) + "x" + std::to_string(input_size_7);
#elif defined(PRL)
    int input_size_1 = -1;
    int input_size_2 = -1;
    if (argc >= 3) {
        try {
            input_size_1 = std::stoi(argv[3]);
            input_size_2 = std::stoi(argv[4]);
        } catch(std::invalid_argument &e) {
            input_size_1 = -1;
            input_size_2 = -1;
        }
    }
    if (input_size_1 < 0 || input_size_2 < 0) {
        std::cout << "usage: " << argv[0] << " platform_id device_id input_size_1 input_size_2" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string input_size_str = std::to_string(input_size_1) + "x" + std::to_string(input_size_2);
#endif

    // get platform
    std::vector<cl::Platform> platforms;
    auto error = cl::Platform::get(&platforms); check_error(error);
    if (platform_id >= platforms.size()) {
        std::cout << "No platform with id " << platform_id << std::endl;
        exit(1);
    }
    cl::Platform platform = platforms[platform_id];
    std::string platform_name;
    platform.getInfo(CL_PLATFORM_VENDOR, &platform_name);
    std::cout << "Platform with name " << platform_name << " found." << std::endl;

    // get device
    std::vector<cl::Device> devices;
    error = platform.getDevices(CL_DEVICE_TYPE_ALL, &devices); check_error(error);
    if (device_id >= devices.size()) {
        std::cout << "No device with id " << device_id << " for platform with id " << platform_id << std::endl;
        exit(1);
    }
    cl::Device device = devices[device_id];
    std::string device_name;
    device.getInfo(CL_DEVICE_NAME, &device_name);
    std::cout << "Device with name " << device_name << " found." << std::endl;

    // read device specific boundaries for work items
    size_t max_wi_sizes[3];
    device.getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, &max_wi_sizes);

    // create tuner
    cltune::Tuner tuner(platform_id, device_id);

    // create tuning parameter
    auto cltune_descending = [] (std::vector<size_t> v) {
        bool valid = true;
        for (int i = 1; i < v.size(); ++i) {
            valid = valid && (v[i - 1] >= v[i]);
        }
        return valid;
    };
    auto cltune_unequal = [] (std::vector<size_t> v) {
        if (v.size() < 2) return true;
        bool valid = true;
        for (int i = 1; i < v.size(); ++i) {
            valid = valid && (v[i - 1] != v[i]);
        }
        valid = valid && (v[v.size() - 1] != v[0]);
        return valid;
    };
    auto cltune_less_than_or_equal = [] (std::vector<size_t> v) { return v[0] <= v[1]; };
    auto cltune_less_than_or_equal_ceil_div = [] (std::vector<size_t> v) { return v[0] <= (v[1] + v[2] - 1) / v[2]; };
    auto cltune_divides = [] (std::vector<size_t> v) { return v[1] % v[0] == 0; };
    auto cltune_divides_div = [] (std::vector<size_t> v) { return (v[1] / v[2]) % v[0] == 0; };
    auto cltune_no_post_in_second_kernel = [] (std::vector<size_t> v) { return v[0] == 1 || (v[0] % v[1] == 0); };
#if defined(GAUSSIAN)
    auto id = tuner.AddKernel({"../../../../evaluation/kernels/gaussian_static_1.cl"}, "gaussian_1", {1, 1}, {1, 1});

    tuner.AddParameter(id, "CACHE_L_CB", {0, 1});
    tuner.AddParameter(id, "CACHE_P_CB", {0, 1});
    tuner.AddParameter(id, "G_CB_RES_DEST_LEVEL", {2});
    tuner.AddParameter(id, "L_CB_RES_DEST_LEVEL", {2, 1, 0});
    tuner.AddParameter(id, "P_CB_RES_DEST_LEVEL", {2, 1, 0});

    tuner.AddParameter(id, "OCL_DIM_L_1", {0, 1});
    tuner.AddParameter(id, "OCL_DIM_L_2", {0, 1});

    tuner.AddParameter(id, "INPUT_SIZE_L_1", {input_size_1});
    tuner.AddParameter(id, "L_CB_SIZE_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "P_CB_SIZE_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "NUM_WG_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "NUM_WI_L_1", cltune_range(input_size_1));

    tuner.AddParameter(id, "INPUT_SIZE_L_2", {input_size_2});
    tuner.AddParameter(id, "L_CB_SIZE_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "P_CB_SIZE_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "NUM_WG_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "NUM_WI_L_2", cltune_range(input_size_2));

    tuner.AddParameter(id, "L_REDUCTION", {1});
    tuner.AddParameter(id, "P_WRITE_BACK", {0});
    tuner.AddParameter(id, "L_WRITE_BACK", {2});

    tuner.AddConstraint(id, cltune_descending, {"G_CB_RES_DEST_LEVEL", "L_CB_RES_DEST_LEVEL", "P_CB_RES_DEST_LEVEL"});
    tuner.AddConstraint(id, cltune_unequal, {"OCL_DIM_L_1", "OCL_DIM_L_2"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_1", "INPUT_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_1", "L_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_1", "INPUT_SIZE_L_1", "L_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_1", "L_CB_SIZE_L_1", "P_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_1", "INPUT_SIZE_L_1", "NUM_WG_L_1"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_2", "INPUT_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_2", "L_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_2", "INPUT_SIZE_L_2", "L_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_2", "L_CB_SIZE_L_2", "P_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_2", "INPUT_SIZE_L_2", "NUM_WG_L_2"});
#elif defined(GEMM)
    auto id = tuner.AddKernel({"../../../../evaluation/kernels/gemm_1.cl"}, "gemm_1", {1, 1, 1}, {1, 1, 1});

    tuner.AddParameter(id, "CACHE_L_CB", {0, 1});
    tuner.AddParameter(id, "CACHE_P_CB", {0, 1});
    tuner.AddParameter(id, "G_CB_RES_DEST_LEVEL", {2});
    tuner.AddParameter(id, "L_CB_RES_DEST_LEVEL", {2, 1, 0});
    tuner.AddParameter(id, "P_CB_RES_DEST_LEVEL", {2, 1, 0});

    tuner.AddParameter(id, "OCL_DIM_L_1", {0, 1, 2});
    tuner.AddParameter(id, "OCL_DIM_L_2", {0, 1, 2});
    tuner.AddParameter(id, "OCL_DIM_R_1", {0, 1, 2});

    tuner.AddParameter(id, "INPUT_SIZE_L_1", {input_size_1});
    tuner.AddParameter(id, "L_CB_SIZE_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "P_CB_SIZE_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "NUM_WG_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "NUM_WI_L_1", cltune_range(input_size_1));

    tuner.AddParameter(id, "INPUT_SIZE_L_2", {input_size_2});
    tuner.AddParameter(id, "L_CB_SIZE_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "P_CB_SIZE_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "NUM_WG_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "NUM_WI_L_2", cltune_range(input_size_2));

    tuner.AddParameter(id, "INPUT_SIZE_R_1", {input_size_3});
    tuner.AddParameter(id, "L_CB_SIZE_R_1", cltune_range(input_size_3));
    tuner.AddParameter(id, "P_CB_SIZE_R_1", cltune_range(input_size_3));
    tuner.AddParameter(id, "NUM_WG_R_1", cltune_range(input_size_3));
    tuner.AddParameter(id, "NUM_WI_R_1", cltune_range(input_size_3));

    tuner.AddParameter(id, "L_REDUCTION", {1});
    tuner.AddParameter(id, "P_WRITE_BACK", {0});
    tuner.AddParameter(id, "L_WRITE_BACK", {2});

    tuner.AddConstraint(id, cltune_descending, {"G_CB_RES_DEST_LEVEL", "L_CB_RES_DEST_LEVEL", "P_CB_RES_DEST_LEVEL"});
    tuner.AddConstraint(id, cltune_unequal, {"OCL_DIM_L_1", "OCL_DIM_L_2"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_1", "INPUT_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_1", "L_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_1", "INPUT_SIZE_L_1", "L_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_1", "L_CB_SIZE_L_1", "P_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_1", "INPUT_SIZE_L_1", "NUM_WG_L_1"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_2", "INPUT_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_2", "L_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_2", "INPUT_SIZE_L_2", "L_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_2", "L_CB_SIZE_L_2", "P_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_2", "INPUT_SIZE_L_2", "NUM_WG_L_2"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_R_1", "INPUT_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_R_1", "L_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_R_1", "INPUT_SIZE_R_1", "L_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_R_1", "L_CB_SIZE_R_1", "P_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_R_1", "INPUT_SIZE_R_1", "NUM_WG_R_1"});
    tuner.AddConstraint(id, cltune_no_post_in_second_kernel, {"NUM_WG_R_1", "L_CB_SIZE_R_1"});
#elif defined(TENSOR_CONTRACTION)
    auto id = tuner.AddKernel({"../../../../evaluation/kernels/tc_abcdef_gebc_dfga_1.cl"}, "tc_1", {1, 1, 1}, {1, 1, 1});

    tuner.AddParameter(id, "CACHE_L_CB", {0, 1});
    tuner.AddParameter(id, "CACHE_P_CB", {0, 1});
    tuner.AddParameter(id, "G_CB_RES_DEST_LEVEL", {2});
    tuner.AddParameter(id, "L_CB_RES_DEST_LEVEL", {2, 1, 0});
    tuner.AddParameter(id, "P_CB_RES_DEST_LEVEL", {2, 1, 0});

    tuner.AddParameter(id, "OCL_DIM_L_1", {0, 1, 2, 3, 4, 5, 6});
    tuner.AddParameter(id, "OCL_DIM_L_2", {0, 1, 2, 3, 4, 5, 6});
    tuner.AddParameter(id, "OCL_DIM_L_3", {0, 1, 2, 3, 4, 5, 6});
    tuner.AddParameter(id, "OCL_DIM_L_4", {0, 1, 2, 3, 4, 5, 6});
    tuner.AddParameter(id, "OCL_DIM_L_5", {0, 1, 2, 3, 4, 5, 6});
    tuner.AddParameter(id, "OCL_DIM_L_6", {0, 1, 2, 3, 4, 5, 6});
    tuner.AddParameter(id, "OCL_DIM_R_1", {0, 1, 2, 3, 4, 5, 6});

    tuner.AddParameter(id, "INPUT_SIZE_L_1", {input_size_1});
    tuner.AddParameter(id, "L_CB_SIZE_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "P_CB_SIZE_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "NUM_WG_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "NUM_WI_L_1", cltune_range(input_size_1));

    tuner.AddParameter(id, "INPUT_SIZE_L_2", {input_size_2});
    tuner.AddParameter(id, "L_CB_SIZE_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "P_CB_SIZE_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "NUM_WG_L_2", cltune_range(input_size_2));
    tuner.AddParameter(id, "NUM_WI_L_2", cltune_range(input_size_2));

    tuner.AddParameter(id, "INPUT_SIZE_L_3", {input_size_3});
    tuner.AddParameter(id, "L_CB_SIZE_L_3", cltune_range(input_size_3));
    tuner.AddParameter(id, "P_CB_SIZE_L_3", cltune_range(input_size_3));
    tuner.AddParameter(id, "NUM_WG_L_3", cltune_range(input_size_3));
    tuner.AddParameter(id, "NUM_WI_L_3", cltune_range(input_size_3));

    tuner.AddParameter(id, "INPUT_SIZE_L_4", {input_size_4});
    tuner.AddParameter(id, "L_CB_SIZE_L_4", cltune_range(input_size_4));
    tuner.AddParameter(id, "P_CB_SIZE_L_4", cltune_range(input_size_4));
    tuner.AddParameter(id, "NUM_WG_L_4", cltune_range(input_size_4));
    tuner.AddParameter(id, "NUM_WI_L_4", cltune_range(input_size_4));

    tuner.AddParameter(id, "INPUT_SIZE_L_5", {input_size_5});
    tuner.AddParameter(id, "L_CB_SIZE_L_5", cltune_range(input_size_5));
    tuner.AddParameter(id, "P_CB_SIZE_L_5", cltune_range(input_size_5));
    tuner.AddParameter(id, "NUM_WG_L_5", cltune_range(input_size_5));
    tuner.AddParameter(id, "NUM_WI_L_5", cltune_range(input_size_5));

    tuner.AddParameter(id, "INPUT_SIZE_L_6", {input_size_6});
    tuner.AddParameter(id, "L_CB_SIZE_L_6", cltune_range(input_size_6));
    tuner.AddParameter(id, "P_CB_SIZE_L_6", cltune_range(input_size_6));
    tuner.AddParameter(id, "NUM_WG_L_6", cltune_range(input_size_6));
    tuner.AddParameter(id, "NUM_WI_L_6", cltune_range(input_size_6));

    tuner.AddParameter(id, "INPUT_SIZE_R_1", {input_size_7});
    tuner.AddParameter(id, "L_CB_SIZE_R_1", cltune_range(input_size_7));
    tuner.AddParameter(id, "P_CB_SIZE_R_1", cltune_range(input_size_7));
    tuner.AddParameter(id, "NUM_WG_R_1", cltune_range(input_size_7));
    tuner.AddParameter(id, "NUM_WI_R_1", cltune_range(input_size_7));

    tuner.AddParameter(id, "L_REDUCTION", {1});
    tuner.AddParameter(id, "P_WRITE_BACK", {0});
    tuner.AddParameter(id, "L_WRITE_BACK", {6});

    tuner.AddConstraint(id, cltune_descending, {"G_CB_RES_DEST_LEVEL", "L_CB_RES_DEST_LEVEL", "P_CB_RES_DEST_LEVEL"});
    tuner.AddConstraint(id, cltune_unequal, {"OCL_DIM_L_1", "OCL_DIM_L_2"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_1", "INPUT_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_1", "L_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_1", "INPUT_SIZE_L_1", "L_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_1", "L_CB_SIZE_L_1", "P_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_1", "INPUT_SIZE_L_1", "NUM_WG_L_1"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_2", "INPUT_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_2", "L_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_2", "INPUT_SIZE_L_2", "L_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_2", "L_CB_SIZE_L_2", "P_CB_SIZE_L_2"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_2", "INPUT_SIZE_L_2", "NUM_WG_L_2"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_3", "INPUT_SIZE_L_3"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_3", "L_CB_SIZE_L_3"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_3", "INPUT_SIZE_L_3", "L_CB_SIZE_L_3"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_3", "L_CB_SIZE_L_3", "P_CB_SIZE_L_3"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_3", "INPUT_SIZE_L_3", "NUM_WG_L_3"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_4", "INPUT_SIZE_L_4"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_4", "L_CB_SIZE_L_4"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_4", "INPUT_SIZE_L_4", "L_CB_SIZE_L_4"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_4", "L_CB_SIZE_L_4", "P_CB_SIZE_L_4"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_4", "INPUT_SIZE_L_4", "NUM_WG_L_4"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_5", "INPUT_SIZE_L_5"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_5", "L_CB_SIZE_L_5"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_5", "INPUT_SIZE_L_5", "L_CB_SIZE_L_5"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_5", "L_CB_SIZE_L_5", "P_CB_SIZE_L_5"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_5", "INPUT_SIZE_L_5", "NUM_WG_L_5"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_6", "INPUT_SIZE_L_6"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_6", "L_CB_SIZE_L_6"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_6", "INPUT_SIZE_L_6", "L_CB_SIZE_L_6"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_6", "L_CB_SIZE_L_6", "P_CB_SIZE_L_6"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_6", "INPUT_SIZE_L_6", "NUM_WG_L_6"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_R_1", "INPUT_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_R_1", "L_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_R_1", "INPUT_SIZE_R_1", "L_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_R_1", "L_CB_SIZE_R_1", "P_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_R_1", "INPUT_SIZE_R_1", "NUM_WG_R_1"});
    tuner.AddConstraint(id, cltune_no_post_in_second_kernel, {"NUM_WG_R_1", "L_CB_SIZE_R_1"});
#elif defined(PRL)
    auto id = tuner.AddKernel({"../../../../evaluation/kernels/rl_1.cl"}, "rl_1", {1, 1}, {1, 1});

    tuner.AddParameter(id, "CACHE_L_CB", {0, 1});
    tuner.AddParameter(id, "CACHE_P_CB", {0, 1});
    tuner.AddParameter(id, "G_CB_RES_DEST_LEVEL", {2});
    tuner.AddParameter(id, "L_CB_RES_DEST_LEVEL", {2, 1, 0});
    tuner.AddParameter(id, "P_CB_RES_DEST_LEVEL", {2, 1, 0});

    tuner.AddParameter(id, "OCL_DIM_L_1", {0, 1});
    tuner.AddParameter(id, "OCL_DIM_R_1", {0, 1});

    tuner.AddParameter(id, "INPUT_SIZE_L_1", {input_size_1});
    tuner.AddParameter(id, "L_CB_SIZE_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "P_CB_SIZE_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "NUM_WG_L_1", cltune_range(input_size_1));
    tuner.AddParameter(id, "NUM_WI_L_1", cltune_range(input_size_1));

    tuner.AddParameter(id, "INPUT_SIZE_R_1", {input_size_2});
    tuner.AddParameter(id, "L_CB_SIZE_R_1", cltune_range(input_size_2));
    tuner.AddParameter(id, "P_CB_SIZE_R_1", cltune_range(input_size_2));
    tuner.AddParameter(id, "NUM_WG_R_1", cltune_range(input_size_2));
    tuner.AddParameter(id, "NUM_WI_R_1", cltune_range(input_size_2));

    tuner.AddParameter(id, "L_REDUCTION", {1});
    tuner.AddParameter(id, "P_WRITE_BACK", {0});
    tuner.AddParameter(id, "L_WRITE_BACK", {1});

    tuner.AddConstraint(id, cltune_descending, {"G_CB_RES_DEST_LEVEL", "L_CB_RES_DEST_LEVEL", "P_CB_RES_DEST_LEVEL"});
    tuner.AddConstraint(id, cltune_unequal, {"OCL_DIM_L_1", "OCL_DIM_R_1"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_L_1", "INPUT_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_L_1", "L_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_L_1", "INPUT_SIZE_L_1", "L_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_L_1", "L_CB_SIZE_L_1", "P_CB_SIZE_L_1"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_L_1", "INPUT_SIZE_L_1", "NUM_WG_L_1"});

    tuner.AddConstraint(id, cltune_divides, {"L_CB_SIZE_R_1", "INPUT_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides, {"P_CB_SIZE_R_1", "L_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WG_R_1", "INPUT_SIZE_R_1", "L_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_divides_div, {"NUM_WI_R_1", "L_CB_SIZE_R_1", "P_CB_SIZE_R_1"});
    tuner.AddConstraint(id, cltune_less_than_or_equal_ceil_div, {"NUM_WI_R_1", "INPUT_SIZE_R_1", "NUM_WG_R_1"});
    tuner.AddConstraint(id, cltune_no_post_in_second_kernel, {"NUM_WG_R_1", "L_CB_SIZE_R_1"});
#endif

    // measure runtime for search space generation
    size_t millis = 0;
    tuner.Tune(&millis);

    std::string results_dir = std::getenv("ARTIFACT_ROOT");
    results_dir.append("/results/generation/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id));
    results_dir.append("/CLTune/");
#if defined(GAUSSIAN)
    results_dir.append("/CLTune/conv_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2));
#elif defined(GEMM)
    results_dir.append("/CLTune/gemm_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2))
            .append("x").append(std::to_string(input_size_3));
#elif defined(TENSOR_CONTRACTION)
    results_dir.append("/CLTune/ccsdt_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2))
            .append("x").append(std::to_string(input_size_3))
            .append("x").append(std::to_string(input_size_4))
            .append("x").append(std::to_string(input_size_5))
            .append("x").append(std::to_string(input_size_6))
            .append("x").append(std::to_string(input_size_7));
#elif defined(PRL)
    results_dir.append("/CLTune/prl_").append(std::to_string(input_size_1))
            .append("x").append(std::to_string(input_size_2));
#endif
    std::ofstream result_file(results_dir, std::ios::out | std::ios::trunc);
    result_file << millis;
    result_file.close();
}
