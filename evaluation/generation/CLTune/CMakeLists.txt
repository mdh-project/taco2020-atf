cmake_minimum_required(VERSION 2.8.11)

project(generation_cltune)

add_executable(generation_cltune_conv main.cpp)
target_include_directories(generation_cltune_conv PUBLIC ../../../extern/CLTune/include ${OpenCL_INCLUDE_DIR})
target_link_libraries(generation_cltune_conv cltune)
target_compile_definitions(generation_cltune_conv PUBLIC GAUSSIAN)

add_executable(generation_cltune_gemm main.cpp)
target_include_directories(generation_cltune_gemm PUBLIC ../../../extern/CLTune/include ${OpenCL_INCLUDE_DIR})
target_link_libraries(generation_cltune_gemm cltune)
target_compile_definitions(generation_cltune_gemm PUBLIC GEMM)

add_executable(generation_cltune_ccsdt main.cpp)
target_include_directories(generation_cltune_ccsdt PUBLIC ../../../extern/CLTune/include ${OpenCL_INCLUDE_DIR})
target_link_libraries(generation_cltune_ccsdt cltune)
target_compile_definitions(generation_cltune_ccsdt PUBLIC TENSOR_CONTRACTION)

add_executable(generation_cltune_prl main.cpp)
target_include_directories(generation_cltune_prl PUBLIC ../../../extern/CLTune/include ${OpenCL_INCLUDE_DIR})
target_link_libraries(generation_cltune_prl cltune)
target_compile_definitions(generation_cltune_prl PUBLIC PRL)
